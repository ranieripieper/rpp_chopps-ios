//
//  EditPaymentMethodsViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 11/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class EditPaymentMethodsViewController: UIViewController {
  
  @IBOutlet weak var paymentMethodCollectionViewDelegate: PaymentMethodCollectionViewDelegate!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var paymentMethodFlagLabel: UILabel!
  @IBOutlet weak var paymentMethodNameLabel: UILabel!
  @IBOutlet weak var paymentMethodNumberLabel: UILabel!
  @IBOutlet weak var paymentMethodExpireLabel: UILabel!
  
  var selectedPaymentMethod: PaymentMethod? {
    didSet {
      paymentMethodFlagLabel.text = "Cartão \(selectedPaymentMethod!.flag!)"
      paymentMethodNameLabel.text = selectedPaymentMethod!.name
      paymentMethodNumberLabel.text = "Número do cartão: \(selectedPaymentMethod!.number)"
      paymentMethodExpireLabel.text = "Validalde: \(selectedPaymentMethod!.expire)"
    }
  }
  var selectedItem: Int?
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.registerNib(UINib(nibName: PaymentMethodCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: PaymentMethodCell.defaultIdentifier())
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let addPaymentViewController = segue.destinationViewController as? AddPaymentMethodViewController {
      addPaymentViewController.completionHandler = ({
        self.navigationController!.popViewControllerAnimated(true)
        self.paymentMethodCollectionViewDelegate.paymentMethods = PaymentMethod.paymentMethods()
        if self.paymentMethodCollectionViewDelegate.paymentMethods.count > 0 {
          self.collectionView.insertItemsAtIndexPaths([NSIndexPath(forItem: 0, inSection: 0)])
        }
      })
    }
  }
  
  @IBAction func deletePaymentMethod(sender: UIButton) {
    if selectedPaymentMethod != nil && selectedItem != nil {
      if selectedPaymentMethod!.delete() {
        paymentMethodCollectionViewDelegate.paymentMethods = PaymentMethod.paymentMethods()
        collectionView.deleteItemsAtIndexPaths([NSIndexPath(forItem: selectedItem!, inSection: 0)])
        paymentMethodFlagLabel.text = ""
        paymentMethodNameLabel.text = ""
        paymentMethodNumberLabel.text = ""
        paymentMethodExpireLabel.text = ""
      }
    }
  }
}
