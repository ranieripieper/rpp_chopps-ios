//
//  BeersAvailableViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/24/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BeerSelectionViewController: UIViewController {
  
  @IBOutlet weak var packageTypelabel: UILabel!
  @IBOutlet weak var drink1Button: UIButton!
  @IBOutlet weak var drink2Button: UIButton!
  @IBOutlet weak var drink3Button: UIButton!
  @IBOutlet weak var drink1NameLabel: UILabel!
  @IBOutlet weak var drink1VolumeLabel: UILabel!
  @IBOutlet weak var drink1AmountLabel: UILabel!
  @IBOutlet weak var drink2NameLabel: UILabel!
  @IBOutlet weak var drink2VolumeLabel: UILabel!
  @IBOutlet weak var drink2AmountLabel: UILabel!
  @IBOutlet weak var drink3NameLabel: UILabel!
  @IBOutlet weak var drink3VolumeLabel: UILabel!
  @IBOutlet weak var drink3AmountLabel: UILabel!
  
  var package: Package?
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configure()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = false
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let verificationCodeViewController = segue.destinationViewController as? VerificationCodeViewController {
      verificationCodeViewController.package = package
      if let drink = sender as? Drink {
        verificationCodeViewController.drink = drink
      }
      navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  @IBAction func drink1Tapped(sender: UIButton) {
    performSegueWithIdentifier("SegueCode", sender: package!.drinks[0])
  }
  
  @IBAction func drink2Tapped(sender: UIButton) {
    performSegueWithIdentifier("SegueCode", sender: package!.drinks[1])
  }
  
  @IBAction func drink3Tapped(sender: UIButton) {
    performSegueWithIdentifier("SegueCode", sender: package!.drinks[2])
  }
  
  func configure() {
    switch package!.type {
    case .Chopp:
      packageTypelabel.text = "QUAL CHOPP VOCÊ QUER?"
      drink1Button.setImage(UIImage(named: "img_choose_chopp1"), forState: .Normal)
      drink2Button.setImage(UIImage(named: "img_choose_chopp2"), forState: .Normal)
      drink3Button.setImage(UIImage(named: "img_choose_chopp3"), forState: .Normal)
    case .Beer:
      packageTypelabel.text = "QUAL CERVEJA VOCÊ QUER?"
      drink1Button.setImage(UIImage(named: "img_choose_beer1"), forState: .Normal)
      drink2Button.setImage(UIImage(named: "img_choose_beer2"), forState: .Normal)
      drink3Button.setImage(UIImage(named: "img_choose_beer3"), forState: .Normal)
    }
    if package!.drinks.count > 0 {
      drink1NameLabel.text = package!.drinks[0].brand
      drink1AmountLabel.text = "\(package!.drinks[0].volume)ml"
      drink1VolumeLabel.text = "\(package!.drinks[0].amount) " + (package!.drinks[0].amount > 1 ? "unidades" : "unidade")
    }
    if package!.drinks.count > 1 {
      drink2NameLabel.text = package!.drinks[1].brand
      drink2AmountLabel.text = "\(package!.drinks[1].volume)ml"
      drink2VolumeLabel.text = "\(package!.drinks[1].amount) " + (package!.drinks[1].amount > 1 ? "unidades" : "unidade")
    } else {
      drink2NameLabel.text = ""
      drink2AmountLabel.text = ""
      drink2VolumeLabel.text = ""
      drink2Button.hidden = true
    }
    if package!.drinks.count > 2 {
      drink3NameLabel.text = package!.drinks[2].brand
      drink3AmountLabel.text = "\(package!.drinks[2].volume)ml"
      drink3VolumeLabel.text = "\(package!.drinks[2].amount) " + (package!.drinks[2].amount > 1 ? "unidades" : "unidade")
    } else {
      drink3NameLabel.text = ""
      drink3AmountLabel.text = ""
      drink3VolumeLabel.text = ""
      drink3Button.hidden = true
    }
  }
}
