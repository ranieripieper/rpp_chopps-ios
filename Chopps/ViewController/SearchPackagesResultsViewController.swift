//
//  SearchPackagesResultsViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 2/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchPackagesResultsViewController: UIViewController {
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchPackagesResultsTableViewDelegate: SearchPackagesResultsTableViewDelegate!
  @IBOutlet weak var countLabel: UILabel!
  
  var value: Float = 0
  var type: Int = 0
  var isLoading = false
  var shouldReload = true
  var currentPage = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    search(1)
    tableView.registerNib(UINib(nibName: "SearchPackagesResultsHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "SearchPackagesResultsHeader")
    tableView.setContentOffset(CGPoint(x: 0, y: 100), animated: false)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let packageDetailsViewController = segue.destinationViewController as? PackageDetailViewController {
      if let package = sender as? Package {
        packageDetailsViewController.package = package
      }
      packageDetailsViewController.delegate = self
    } else if let paymentMethodViewController = segue.destinationViewController as? PaymentMethodViewController {
      paymentMethodViewController.package = sender as Package!
    } else if let addPaymentMethodViewController = segue.destinationViewController as? AddPaymentMethodViewController {
      addPaymentMethodViewController.completionHandler = ({
        self.navigationController?.popViewControllerAnimated(false)
        self.performSegueWithIdentifier("SeguePaymentMethod", sender: sender)
      })
    }
  }
  
  func search(page: Int?) {
    if isLoading || !shouldReload { return }
    isLoading = true
    Package.searchWithValue(value, type: type, page: page ?? currentPage + 1) { bars in
      self.shouldReload = bars.count > 0
      self.isLoading = false
      self.currentPage = page ?? self.currentPage + 1
      if page == 1 {
        self.searchPackagesResultsTableViewDelegate.datasource.removeAll(keepCapacity: false)
      }
      self.searchPackagesResultsTableViewDelegate.datasource.addElementsOf(bars)
      var count: String {
        if self.searchPackagesResultsTableViewDelegate.datasource.count == 1 {
          return "1 resultado"
        } else {
          return "\(self.searchPackagesResultsTableViewDelegate.datasource.count) resultados"
        }
      }
      let string = "Encontramos \(count) para sua busca"
      let attr = NSMutableAttributedString(string: string, attributes: [NSFontAttributeName: UIFont(name: "GillSans", size: 16)!])
      attr.setAttributes([NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 18)!], range: (string as NSString).rangeOfString("\(self.searchPackagesResultsTableViewDelegate.datasource.count)"))
      self.countLabel.attributedText = attr
      self.countLabel.hidden = false
      self.tableView.reloadData()
    }
  }
  
  func showDetailsOfPackage(package: Package) {
    performSegueWithIdentifier("SeguePackageDetails", sender: package)
  }
}

extension SearchPackagesResultsViewController: PackageDetailViewControllerDelegate {
  func goToPaymentForPackage(package: Package) {
    if PaymentMethod.hasPaymentMethods() {
      performSegueWithIdentifier("SeguePaymentMethod", sender: package)
    } else {
      performSegueWithIdentifier("SegueAddPaymentMethod", sender: package)
    }
  }
  
  func goToBeerSelection(package: Package) {
    performSegueWithIdentifier("SegueBeerSelection", sender: package)
  }
  
  func goToVerificationCode(package: Package, drink: Drink) {
    performSegueWithIdentifier("SegueCode", sender: ["package": package, "drink": drink])
  }
}
