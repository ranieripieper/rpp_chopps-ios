//
//  FirstScreenViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import Social

class FirstScreenViewController: UIViewController {
  
  @IBOutlet weak var fingertipButton: UIButton!
  @IBOutlet weak var fingertipButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var fingertipButtonTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var bottomPaddingConstraint: NSLayoutConstraint!
  @IBOutlet weak var loadingView: LoadingView!
  @IBOutlet weak var loadingViewHeightConstraint: NSLayoutConstraint!
  
  var locationManager = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager
  var alertView: AlertController?
  lazy var accountStore = ACAccountStore()
  
  deinit {
    println("\(self) is being deinitialized")
    loadingView.stop()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if LocationManager.isAuthorized() {
      locationManager.startUpdatingLocation()
    }
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    if IOS_8() && TouchId.canAuthenticateWithTouchId() && KeychainWrapper.stringForKey("userId") != nil {
      fingertipButton.hidden = false
      fingertipButtonHeightConstraint.constant = 46
      fingertipButtonTopConstraint.constant = 20
      bottomPaddingConstraint.constant = -(5 * 46 + 8 + 3 * 20)
    } else {
      fingertipButton.hidden = true
      fingertipButtonHeightConstraint.constant = 0
      fingertipButtonTopConstraint.constant = 0
      bottomPaddingConstraint.constant = -(4 * 46 + 8 + 2 * 20)
    }
    self.view.layoutIfNeeded()
    bottomPaddingConstraint.constant = 14
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  // MARK: Segue
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let registerViewController = segue.destinationViewController as? RegisterViewController {
      registerViewController.delegate = self
    } else if let loginViewController = segue.destinationViewController as? LoginViewController {
      loginViewController.delegate = self
    } else if let termsViewController = segue.destinationViewController as? TermsViewController {
      termsViewController.delegate = self
    }
  }
  
  // MARK: IBActions
  @IBAction func facebookLoginTapped () {
    let facebookAccountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierFacebook)
    let options = [ACFacebookAppIdKey: "802983189736935", ACFacebookAudienceKey: ACFacebookAudienceOnlyMe, ACFacebookPermissionsKey: ["email"]]
    accountStore.requestAccessToAccountsWithType(facebookAccountType, options: options) { (granted, error) in
      if granted {
        if let facebookAccount = self.accountStore.accountsWithAccountType(facebookAccountType).last as? ACAccount {
          if let properties = facebookAccount.valueForKey("properties") as? NSDictionary {
            if let uid = properties["uid"] as? Int {
              if let auth_token = facebookAccount.credential.oauthToken {
                let request = SLRequest(forServiceType: SLServiceTypeFacebook, requestMethod: .GET, URL: NSURL(string: "https://graph.facebook.com/me"), parameters: nil)
                request.account = facebookAccount
                request.performRequestWithHandler() { data, response, error in
                  if error == nil && response.statusCode == 200 {
                    if let JSON = NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments, error: nil) as? [String: AnyObject] {
                      if let birthday = JSON["birthday"] as? String {
                        if let email = JSON["email"] as? String {
                          if let name = JSON["name"] as? String {
//                            Person.registerUserWithName(name, email: email, birthDate: birthday, password: "", facebookToken: auth_token, facebookID: String(uid), latitude: self.locationManager.userLocation != nil ? Float(self.locationManager.userLocation!.coordinate.latitude) : nil, longitude: self.locationManager.userLocation != nil ? Float(self.locationManager.userLocation!.coordinate.longitude) : nil, pictureData: nil, completionHandler: { success, closestBar in
//                              println(closestBar)
//                            })
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
//    Facebook.loginWithCompletionHandler { (success) -> () in
//      if success {
//        let accessToken = Facebook.getAccessToken()
//        println("send fb to API with accessToken: \(accessToken)")
//      }
//    }
  }
  
  @IBAction func touchIdButtonTapped () {
    TouchId.authenticateWithTouchId { (success, error) -> () in
      if success {
        self.startLoading()
        let email = KeychainWrapper.stringForKey("email")
        let password = KeychainWrapper.stringForKey("password")
        if email != nil && password != nil {
          var location: CLLocation?
          if self.locationManager.userLocation != nil {
            location = self.locationManager.location!
          }
          Person.loginUserWithEmail(email!, password: password!, location: location, completionHandler: { (success, closestBar) -> () in
            if success {
              self.stopLoading()
              self.goHome(closestBar)
            } else {
              self.alertView = AlertController(title: "Chopps App", message: "Usuário e/ou senha inválidos", buttons: nil, cancelButton: ("Ok", {
                self.alertView = nil
              }), style: .Alert)
              self.alertView!.alertInViewController(self)
            }
          })
        } else {
          self.alertView = AlertController(title: "Chopps App", message: "Não há usuário cadastrado neste aparelho", buttons: nil, cancelButton: ("Ok", {
            self.alertView = nil
          }), style: .Alert)
          self.alertView!.alertInViewController(self)
        }
      } else {
        println("no")
      }
    }
  }
  
  @IBAction func signupTapped() {
    performSegueWithIdentifier("SegueTerms", sender: nil)
  }
  
  // MARK: Instance Methods
  func goHome(closestBar: Bar?) {
    self.bottomPaddingConstraint.constant = -234 - fingertipButtonHeightConstraint.constant - fingertipButtonTopConstraint.constant
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
      }, completion: { (finished: Bool) -> Void in
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        if closestBar != nil {
          appDelegate.loadMainApplicationFlow(["hasInitialInfo": true, "closestBar": closestBar!])
        } else {
          appDelegate.loadMainApplicationFlow(["hasInitialInfo": true])
        }
    })
  }
  
  func startLoading() {
    loadingView.start()
    loadingViewHeightConstraint.constant = 4
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func stopLoading() {
    loadingView.stop()
    loadingViewHeightConstraint.constant = 0
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
}

// MARK: RegisterViewControllerDelegate
extension FirstScreenViewController: RegisterViewControllerDelegate {
  
  func registerViewControllerDidRequest(viewController: RegisterViewController) {
    startLoading()
  }
  
  func registerViewControllerDidFinish(viewController: RegisterViewController, success: Bool, closestBar: Bar?) {
    stopLoading()
    dismissViewControllerAnimated(true, completion: nil)
    if success {
      goHome(closestBar)
    }
  }
}

// MARK: LoginViewControllerDelegate
extension FirstScreenViewController: LoginViewControllerDelegate {
  
  func loginViewControllerDidRequest(viewController: LoginViewController) {
    startLoading()
  }
  
  func loginViewControllerDidFinish(viewController: LoginViewController, success: Bool, closestBar: Bar?) {
    stopLoading()
    dismissViewControllerAnimated(true, completion: nil)
    if success {
      goHome(closestBar)
    }
  }
}

// MARK: TermsViewControllerDelegate
extension FirstScreenViewController: TermsViewControllerDelegate {
  
  func termsViewControllerDidFinish(viewController: TermsViewController, success: Bool) {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      if success {
        self.performSegueWithIdentifier("SegueSignUp", sender: nil)
      }
    })
  }
}
