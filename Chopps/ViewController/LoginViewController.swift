//
//  LoginViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol LoginViewControllerDelegate {
  optional func loginViewControllerDidRequest(viewController: LoginViewController)
  optional func loginViewControllerDidFinish(viewController: LoginViewController, success: Bool, closestBar: Bar?)
}

class LoginViewController: UIViewController {

  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var bottomPaddingConstraint: NSLayoutConstraint!
  
  weak var delegate: LoginViewControllerDelegate?
  var locationManager = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager
  var alertView: AlertController?
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.gestureRecognizers = [tap]
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardUp:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardDown:", name: UIKeyboardWillHideNotification, object: nil)
    emailTextField.setPaddings()
    passwordTextField.setPaddings()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
  }
  
  @IBAction func tapped(tap: UITapGestureRecognizer) {
    delegate?.loginViewControllerDidFinish?(self, success: false, closestBar: nil)
  }
  
  @IBAction func authenticate() {
    delegate?.loginViewControllerDidRequest?(self)
    var location: CLLocation?
    if locationManager.userLocation != nil {
      location = locationManager.userLocation!
    }
    Person.loginUserWithEmail(emailTextField.text, password: passwordTextField.text, location: location) { (success, closestBar) -> () in
      if success {
      self.delegate?.loginViewControllerDidFinish!(self, success: true, closestBar: closestBar)
      } else {
        self.alertView = AlertController(title: "Chopps App", message: "Usuário e/ou senha inválidos", buttons: nil, cancelButton: ("Ok", {
          self.alertView = nil
        }), style: .Alert)
        self.alertView!.alertInViewController(self)
      }
    }
  }
  
  @IBAction func forgotPassword() {
    if countElements(emailTextField.text) == 0 {
      self.alertView = AlertController(title: "Chopps App", message: "Por favor, digite seu email", buttons: nil, cancelButton: ("Ok", {
        self.alertView = nil
      }), style: .Alert)
      self.alertView!.alertInViewController(self)
    } else {
      delegate?.loginViewControllerDidRequest?(self)
//      Person.forgotPassword(emailTextField.text, completionHandler: { (success) -> () in
//        self.alertView = AlertController(title: "Chopps App", message: "Um email será enviado com instruções para trocar sua senha", buttons: nil, cancelButton: ("Ok", {
//          self.alertView = nil
//        }), style: .Alert)
//        self.alertView!.alertInViewController(self)
//      })
    }
  }

  func handleKeyboardUp(notification: NSNotification) {
    var notificationData = notification.userInfo
    var frame = notificationData![UIKeyboardFrameBeginUserInfoKey]! as NSValue
    let height = frame.CGRectValue().size.height
    bottomPaddingConstraint.constant = height - 170
    UIView.animateWithDuration(0.25, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func handleKeyboardDown(notification: NSNotification) {
    bottomPaddingConstraint.constant = 20
    UIView.animateWithDuration(0.25, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
}

extension LoginViewController: UIGestureRecognizerDelegate {
  
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
    return !touch.view.isKindOfClass(UITextField.classForCoder()) && !emailTextField.resignFirstResponder() && !passwordTextField.resignFirstResponder() && gestureRecognizer == tap && touch.view == view
  }
}

extension LoginViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == emailTextField {
      passwordTextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
      authenticate()
    }
    return true
  }
}
