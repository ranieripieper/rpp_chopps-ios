//
//  HomeViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  
  @IBOutlet weak var headerHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var headerLogoImageView1: UIImageView!
  @IBOutlet weak var headerLogoImageView2: UIImageView!
  @IBOutlet weak var headerLogoImageView3: UIImageView!
  @IBOutlet weak var headerView: UIView!
  @IBOutlet weak var loadingView: LoadingView!
  
  var firstLaunch = true
  var didAnimatePackagesCell = false
  var didAnimateHistoryCell = false
  var numberOfBars: Int?
  var person = Person.persistedUser()
  var locationManager = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager
  var closestBar: Bar?
  var hasMyPackagesCell = false
  var hasMyHistoryCell = false
  var hasInitialInfo = false
  
  deinit {
    println("\(self) is being deinitialized")
    loadingView.stop()
    NSNotificationCenter.defaultCenter().removeObserver(self, name: "HomeViewControllerRefreshHome", object: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    registerTableViewNibs()
    if !hasInitialInfo {
      getHomeInfo()
    } else {
      numberOfBars = Bar.getNumberOfBars()
      loadingView.start()
      loadingView.stop()
    }
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "getHomeInfo", name: "HomeViewControllerRefreshHome", object: nil)
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = true
    locationManager.locationManagerDelegate = self
    if firstLaunch {
      firstLaunch = false
      animateOpen { (finished) in }
    }
  }

  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
    if let searchBarViewController = segue.destinationViewController as? SearchBarViewController {
      searchBarViewController.locationManager = locationManager
      searchBarViewController.person = person
    } else if let settingsViewController = segue.destinationViewController as? SettingsViewController {
      settingsViewController.homeViewController = self
//    } else if let beerSelectionViewController = segue.destinationViewController as? BeerSelectionViewController {
//      beerSelectionViewController.bar = closestBar
    } else if let barDetailViewController = segue.destinationViewController as? BarDetailViewController {
      barDetailViewController.locationManager = locationManager
      barDetailViewController.bar = closestBar
      barDetailViewController.person = person
    } else if let searchPackageViewController = segue.destinationViewController as? SearchPackageViewController {
//      searchPackageViewController.locationManager = locationManager
    } else if let myPackagesViewController = segue.destinationViewController as? MyPackagesViewController {
      myPackagesViewController.locationManager = locationManager
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  func animateOpen(completionHandler: (Bool) -> ()) {
    tableView.contentInset = UIEdgeInsetsMake (view.bounds.size.height, 0, 35, 0)
    tableView.setContentOffset(CGPoint (x: 0, y: -view.bounds.size.height + 35), animated: false)
    headerHeightConstraint.constant = view.bounds.size.height - 35
    view.layoutIfNeeded()
    headerHeightConstraint.constant = 300
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
      self.tableView.contentInset = UIEdgeInsetsMake (300, 0, 35, 0)
    }, completion: completionHandler)
  }
  
  func animateClose(completionHandler: (Bool) -> ()) {
    headerHeightConstraint.constant = view.bounds.size.height - 35
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.view.layoutIfNeeded()
    }, completion: completionHandler)
  }
  
  func registerTableViewNibs() {
    tableView.registerNib (UINib (nibName: NearestBarCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: NearestBarCell.defaultIdentifier())
    tableView.registerNib (UINib (nibName: BarCounterCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: BarCounterCell.defaultIdentifier())
    tableView.registerNib (UINib (nibName: SearchBarCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: SearchBarCell.defaultIdentifier())
    tableView.registerNib (UINib (nibName: ProfileCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: ProfileCell.defaultIdentifier())
    tableView.registerNib (UINib (nibName: MyPackagesCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: MyPackagesCell.defaultIdentifier())
    tableView.registerNib (UINib (nibName: MyHistoryCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: MyHistoryCell.defaultIdentifier())
    tableView.registerNib (UINib (nibName: SettingsCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: SettingsCell.defaultIdentifier())
  }
  
  func updateClosestBar(userLocation: CLLocation) {
    getHomeInfo()
  }
  
  func getHomeInfo() {
    loadingView.start()
    person?.getUserInfo(locationManager.userLocation) { (closestBar) in
      self.loadingView.stop()
      if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? NearestBarCell {
        self.closestBar = closestBar
        cell.configureWithBarName(self.closestBar?.name ?? "")
      }
      self.numberOfBars = Bar.getNumberOfBars()
      if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 1, inSection: 0)) as? BarCounterCell {
        cell.configureWithBarCount(self.numberOfBars!)
      }
      if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 3, inSection: 0)) as? ProfileCell {
        cell.configureWithAvatar(self.person!.pictureURL, name: self.person!.name, barName: self.person!.lastPackageConsumed?.barName ?? "", lastChoppTakenDate: self.person!.lastPackageConsumed?.date ?? "", brand: self.person!.lastPackageConsumed?.brand ?? "", type: self.person!.lastPackageConsumed?.style ?? "")
      }
      if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 4, inSection: 0)) as? MyPackagesCell {
        cell.configureWithPackages(self.person!.packages)
      } else if !self.hasMyPackagesCell && self.person!.packages.count > 0 {
        self.tableView.insertRowsAtIndexPaths([NSIndexPath(forRow: 4, inSection: 0)], withRowAnimation: .Automatic)
      }
      if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 4, inSection: 0)) as? MyHistoryCell {
        if self.person != nil && self.person!.history != nil {
          cell.configureWithChoppTypes(self.person!.history!)
        }
      } else if let cell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 5, inSection: 0)) as? MyHistoryCell {
        if self.person != nil && self.person!.history != nil {
          cell.configureWithChoppTypes(self.person!.history!)
        }
      } else if !self.hasMyHistoryCell && self.person != nil && self.person!.history != nil {
        var indexPath: NSIndexPath
        if self.person!.packages.count > 0 {
          indexPath = NSIndexPath(forRow: 5, inSection: 0)
        } else {
          indexPath = NSIndexPath(forRow: 4, inSection: 0)
        }
        self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
      }
    }
  }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5 + (person?.packages.count > 0 ? 1 : 0) + (person?.history != nil ? 1 : 0)
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    switch indexPath.row {
    case 0:
      return 140
    case 1:
      return 140
    case 2:
      return 156
    case 3:
      return 218
    case 4:
      if person?.packages.count > 0 || person?.history != nil {
        hasMyPackagesCell = person?.packages.count > 0
        hasMyHistoryCell = !(person?.packages.count > 0)
        return 269
      } else {
        return 250
      }
    case 5:
      if person?.packages.count > 0 && person?.history != nil {
        hasMyHistoryCell = true
        return 269
      } else {
        return 250
      }
    case 6:
      return 250
    default:
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    var cell: UITableViewCell?
    switch indexPath.row {
    case 0:
      let nearestBarCell = tableView.dequeueReusableCellWithIdentifier(NearestBarCell.defaultIdentifier(), forIndexPath: indexPath) as NearestBarCell
      if closestBar != nil {
        nearestBarCell.configureWithBarName(closestBar!.name)
      } else {
        nearestBarCell.configureWithBarName("")
      }
      nearestBarCell.delegate = self
      cell = nearestBarCell
    case 1:
      let barCounterCell = tableView.dequeueReusableCellWithIdentifier(BarCounterCell.defaultIdentifier(), forIndexPath: indexPath) as BarCounterCell
      barCounterCell.configureWithBarCount(numberOfBars ?? 0)
      cell = barCounterCell
    case 2:
      let searchBarCell = tableView.dequeueReusableCellWithIdentifier(SearchBarCell.defaultIdentifier(), forIndexPath: indexPath) as SearchBarCell
      searchBarCell.delegate = self
      cell = searchBarCell
    case 3:
      let profileCell = tableView.dequeueReusableCellWithIdentifier(ProfileCell.defaultIdentifier(), forIndexPath: indexPath) as ProfileCell
      profileCell.configureWithAvatar(person!.pictureURL, name: person!.name, barName: person!.lastPackageConsumed?.barName ?? "", lastChoppTakenDate: person!.lastPackageConsumed?.date ?? "", brand: person!.lastPackageConsumed?.brand ?? "", type: person!.lastPackageConsumed?.style ?? "")
      cell = profileCell
    case 4:
      if person?.packages.count > 0 {
        cell = myPackagesCell(indexPath)
      } else if person?.history != nil {
        cell = historyCell(indexPath)
      } else {
        cell = settingsCell(indexPath)
      }
    case 5:
      if person?.packages.count > 0 && person?.history != nil {
        cell = historyCell(indexPath)
      } else {
        cell = settingsCell(indexPath)
      }
    case 6:
      cell = settingsCell(indexPath)
    default:
      cell = nil
      break
    }
    return cell!
  }
  
  func myPackagesCell(indexPath: NSIndexPath) -> MyPackagesCell {
    let myPackagesCell = tableView.dequeueReusableCellWithIdentifier(MyPackagesCell.defaultIdentifier(), forIndexPath: indexPath) as MyPackagesCell
    myPackagesCell.configureWithPackages(person!.packages)
    myPackagesCell.delegate = self
    return myPackagesCell
  }
  
  func historyCell(indexPath: NSIndexPath) -> MyHistoryCell {
    let myHistoryCell = tableView.dequeueReusableCellWithIdentifier(MyHistoryCell.defaultIdentifier(), forIndexPath: indexPath) as MyHistoryCell
    if person != nil && person!.history != nil {
      myHistoryCell.configureWithChoppTypes(person!.history!)
    }
    myHistoryCell.delegate = self
    return myHistoryCell
  }
  
  func settingsCell(indexPath: NSIndexPath) -> SettingsCell {
    let settingsCell = tableView.dequeueReusableCellWithIdentifier(SettingsCell.defaultIdentifier(), forIndexPath: indexPath) as SettingsCell
    settingsCell.delegate = self
    return settingsCell
  }
}

extension HomeViewController: UIScrollViewDelegate {
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView != tableView {
      return
    }
    headerHeightConstraint.constant = 300 - min(170, scrollView.contentOffset.y + 300)
    if !didAnimatePackagesCell && person?.packages.count > 0 {
      let indexPath = NSIndexPath (forRow: 4, inSection: 0)
      let myPackagesCell = tableView.cellForRowAtIndexPath(indexPath) as MyPackagesCell?
      if myPackagesCell != nil {
        if tableView.contentOffset.y + view.bounds.height > (tableView.rectForRowAtIndexPath(indexPath).origin.y + myPackagesCell!.bounds.height / 1.25) {
          didAnimatePackagesCell = true
          myPackagesCell!.animateFirstCell()
        }
      }
    }
    if !didAnimateHistoryCell && person?.history != nil {
      let indexPath = NSIndexPath (forRow: 4 + (person?.packages.count > 0 ? 1 : 0), inSection: 0)
      let myHistoryCell = tableView.cellForRowAtIndexPath(indexPath) as MyHistoryCell?
      if myHistoryCell != nil {
        if tableView.contentOffset.y + view.bounds.height > (tableView.rectForRowAtIndexPath(indexPath).origin.y + myHistoryCell!.bounds.height / 1.25) {
          if person != nil && person!.history != nil {
            didAnimateHistoryCell = true
            myHistoryCell!.configureWithChoppTypes(person!.history!)
            myHistoryCell!.animateCell()
          }
        }
      }
    }
  }
}

extension HomeViewController: NearestBarCellDelegate {
  
  func letsGetaBeerButtonTapped(nearesBarCell: NearestBarCell) {
    if closestBar == nil {
      performSegueWithIdentifier("SegueSearchBar", sender: nil)
//    } else if closestBar!.userHasActivePackages {
//      performSegueWithIdentifier("SegueGetABeer", sender: nil)
    } else {
      performSegueWithIdentifier("SegueBarDetail", sender: nil)
    }
  }
}

extension HomeViewController: SearchBarCellDelegate {
  
  func searchBarButtonTapped(searchBarCell: SearchBarCell) {
    performSegueWithIdentifier("SegueSearchBar", sender: nil)
  }
  
  func buyPackageButtonTapped(searchBarCell: SearchBarCell) {
    performSegueWithIdentifier("SegueSearchPackages", sender: nil)
  }
}

extension HomeViewController: MyPackagesCellDelegate {
  
  func myPackagesButtonTapped(myPackagesCell: MyPackagesCell) {
    performSegueWithIdentifier("SegueMyPackages", sender: nil)
  }
}

extension HomeViewController: MyHistoryCellDelegate {
  
  func myHistoryButtonTapped(myHistoryCell: MyHistoryCell) {
    performSegueWithIdentifier("SegueHistory", sender: nil)
  }
}

extension HomeViewController: SettingsCellDelegate {
  
  func settingsButtonTapped(settingsCell: SettingsCell) {
    performSegueWithIdentifier("SegueSettings", sender: nil)
  }
}

extension HomeViewController: LocationManagerDelegate {
  
  func didUpdateLocation(userLocation: CLLocation) {
    updateClosestBar(userLocation)
  }
}

extension UITableViewCell { // Class Name
  
  class func defaultIdentifier() -> String {
    let tokens = split(NSStringFromClass(self.classForCoder()), { $0 == "." })
    return tokens.last!
  }
}
