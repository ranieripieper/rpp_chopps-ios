//
//  TermsViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol TermsViewControllerDelegate {
  optional func termsViewControllerDidFinish(viewController: TermsViewController, success: Bool)
}

class TermsViewController: UIViewController {

  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var textViewBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  weak var delegate: TermsViewControllerDelegate?
  var showAgreeButton = true
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if !showAgreeButton {
      textViewBottomSpaceConstraint.constant = 20
      view.addGestureRecognizer(tap)
    }
    if !IOS_8() {
      modalPresentationStyle = .CurrentContext
      modalPresentationStyle = .FormSheet
    }
  }
  
  @IBAction func agree(sender: UIButton) {
    delegate?.termsViewControllerDidFinish?(self, success: true)
  }
  
  @IBAction func disagree(sender: UIButton) {
    delegate?.termsViewControllerDidFinish?(self, success: false)
  }
  
  @IBAction func dismiss(sender: UITapGestureRecognizer) {
    delegate?.termsViewControllerDidFinish?(self, success: true)
  }
}
