//
//  AddPaymentMethodViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 11/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class AddPaymentMethodViewController: UIViewController {
  
  @IBOutlet weak var nameOnCardTextField: UITextField!
  @IBOutlet weak var numberTextField: BKCardNumberField!
  @IBOutlet weak var expireTextField: BKCardExpiryField!
  @IBOutlet weak var scrollViewBottomConstraint: NSLayoutConstraint!
  
  var completionHandler: (() -> ())?
  var alertView: AlertController?
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardUp:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardDown:", name: UIKeyboardWillHideNotification, object: nil)
    nameOnCardTextField.setPaddings()
    numberTextField.setPaddings()
    expireTextField.setPaddings()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
  }
  
  @IBAction func confirmAddingCard (sender: UIButton) {
    if PaymentMethod.isCreditCardValidWithNumber(numberTextField.text) {
      let paymentMethod = PaymentMethod(name: nameOnCardTextField.text, number: numberTextField.text, expire: expireTextField.text)
      if paymentMethod.save() {
        if completionHandler != nil {
          completionHandler!()
        }
      } else {
        alertView = AlertController(title: "Chopps App", message: "Não foi possível cadastrar este cartão, por favor, tente novamente", buttons: nil, cancelButton: ("Ok", {
          self.alertView = nil
        }), style: .Alert)
        alertView?.alertInViewController(self)
      }
    }
  }
  
  func handleKeyboardUp (notification: NSNotification) {
    var notificationData = notification.userInfo
    var frame = notificationData![UIKeyboardFrameBeginUserInfoKey]! as NSValue
    let durationValue = notificationData![UIKeyboardAnimationDurationUserInfoKey]! as NSValue
    let height = frame.CGRectValue().size.height
    let duration = (durationValue as NSNumber).doubleValue
    scrollViewBottomConstraint.constant = height
    UIView.animateWithDuration(duration, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
  
  func handleKeyboardDown (notification: NSNotification) {
    let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey]! as NSValue
    let duration = (durationValue as NSNumber).doubleValue
    scrollViewBottomConstraint.constant = 0
    UIView.animateWithDuration(duration, animations: { () -> Void in
      self.view.layoutIfNeeded()
    })
  }
}

extension AddPaymentMethodViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == nameOnCardTextField {
      numberTextField.becomeFirstResponder()
    }
    return true
  }
}
