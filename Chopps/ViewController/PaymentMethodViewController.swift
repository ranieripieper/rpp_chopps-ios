//
//  PaymentMethodViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/31/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var paymentMethodCollectionViewDelegate: PaymentMethodCollectionViewDelegate!
  
  var loadingView: LoadingView?
  var package: Package?
  var selectedPaymentMethod: PaymentMethod?
  var alertView: AlertController?
  
  deinit {
    println("\(self) is being deinitialized")
    loadingView?.stop()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    collectionView.registerNib(UINib(nibName: PaymentMethodCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: PaymentMethodCell.defaultIdentifier())
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let addPaymentMethodViewController = segue.destinationViewController as? AddPaymentMethodViewController {
      addPaymentMethodViewController.completionHandler = ({
        self.navigationController!.popViewControllerAnimated(true)
        self.paymentMethodCollectionViewDelegate.paymentMethods = PaymentMethod.paymentMethods()
        self.collectionView.insertItemsAtIndexPaths([NSIndexPath(forItem: 0, inSection: 0)])
      })
//    } else if let beerBoughtViewController = segue.destinationViewController as? BeerConsumedViewController {
//      beerBoughtViewController.package = package
    } else if let purchaseSummaryViewController = segue.destinationViewController as? PurchaseSummaryViewController {
      purchaseSummaryViewController.paymentMethod = self.selectedPaymentMethod
      purchaseSummaryViewController.package = self.package
    }
  }
  
  @IBAction func confirmPaymentMethod (sender: UIButton) {
    if selectedPaymentMethod == nil {
      alertView = AlertController(title: "Chopps App", message: "Por favor, selecione um método de pagamento", buttons: nil, cancelButton: ("Ok", {
        self.alertView = nil
      }), style: .Alert)
      alertView!.alertInViewController(self)
    } else {
      alertView = AlertController(title: "Chopps App", message: "Digite o código de segurança do cartão", buttons: [("Pagar!", {
        let code = self.alertView!.textFieldTextAtIndex(0)
        if code != nil {
          self.selectedPaymentMethod!.securityCode = code
          self.performSegueWithIdentifier("SeguePurchaseSummary", sender: nil)
        }
      })], cancelButton: ("Cancelar", {
        self.alertView = nil
      }), style: .Alert)
      alertView!.addTextFieldWithConfigurationHandler({ (textField) -> Void in
        textField.keyboardType = .NumberPad
      })
      alertView!.alertInViewController(self)
    }
  }
}
