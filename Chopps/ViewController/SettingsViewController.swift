//
//  SettingsViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 11/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
  
  @IBOutlet weak var locationLabel: UILabel!
  @IBOutlet weak var locationSwitch: UISwitch!
  @IBOutlet weak var locationImageViewHeightConstraint: NSLayoutConstraint!
  
  var homeViewController: HomeViewController?
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if !IOS_8() {
      locationImageViewHeightConstraint.constant = 0
      locationLabel.hidden = true
      locationSwitch.hidden = true
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = false
    homeViewController?.locationManager.locationManagerDelegate = self
    locationSwitch.on = CLLocationManager.authorizationStatus() == .Authorized
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  @IBAction func switchValueChanged(sender: UISwitch) {
    UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
  }
  
  @IBAction func editProfile(sender: UIButton) {
    let registerViewController = UIStoryboard(name: "Login", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("RegisterViewController") as RegisterViewController
    registerViewController.isNewProfile = false
    registerViewController.delegate = self
    registerViewController.modalPresentationStyle = .OverFullScreen
    presentViewController(registerViewController, animated: true, completion: nil)
  }
  
  @IBAction func terms(sender: UIButton) {
    let termsViewController = UIStoryboard(name: "Login", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("TermsViewController") as TermsViewController
    termsViewController.delegate = self
    presentViewController(termsViewController, animated: true, completion: nil)
  }
  
  @IBAction func about(sender: UIButton) {
    
  }
  
  @IBAction func logout(sender: UIButton) {
    Person.logout()
    animateClose { (finished) -> () in
      (UIApplication.sharedApplication().delegate as AppDelegate).loadLoginFlow()
    }
  }
  
  func animateClose (completionHandler: (Bool) -> ()) {
    let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 64))
    let headerImageView1 = UIImageView(image: UIImage(named: "img_logo_1"))
    let headerImageView2 = UIImageView(image: UIImage(named: "img_logo_2"))
    let headerImageView3 = UIImageView(image: UIImage(named: "img_logo_3"))
//    headerImageView1.frame = CGRect(x: 0, y: 4, width: 84, height: 36)
//    headerImageView2.frame = CGRect(x: 84, y: 17, width: 16, height: 13)
//    headerImageView3.frame = CGRect(x: 100, y: 7, width: 57, height: 26)
    headerView.setTranslatesAutoresizingMaskIntoConstraints(false)
    headerImageView1.setTranslatesAutoresizingMaskIntoConstraints(false)
    headerImageView2.setTranslatesAutoresizingMaskIntoConstraints(false)
    headerImageView3.setTranslatesAutoresizingMaskIntoConstraints(false)
    headerView.backgroundColor = UIColor(red: 107.0/255, green: 158.0/255, blue: 161.0/255, alpha: 1.0)
    headerImageView1.contentMode = .ScaleAspectFit
    headerImageView2.contentMode = .ScaleAspectFit
    headerImageView3.contentMode = .ScaleAspectFit
    navigationController!.view.addSubview(headerView)
    headerView.addSubview(headerImageView1)
    headerView.addSubview(headerImageView2)
    headerView.addSubview(headerImageView3)
    let headerHeightConstraint = NSLayoutConstraint(item: headerView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 64.0)
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[view]-(0)-|", options: .DirectionMask, metrics: nil, views: ["view": headerView])
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(0)-[view]", options: .DirectionMask, metrics: nil, views: ["view": headerView])
    headerView.addConstraint(headerHeightConstraint)
    navigationController!.view.addConstraints(horizontalConstraints)
    navigationController!.view.addConstraints(verticalConstraints)
    
    let centerX1 = NSLayoutConstraint(item: headerImageView1, attribute: .CenterX, relatedBy: .Equal, toItem: headerView, attribute: .CenterX, multiplier: 1.0, constant: -37.0)
    let centerY1 = NSLayoutConstraint(item: headerImageView1, attribute: .CenterY, relatedBy: .Equal, toItem: headerView, attribute: .CenterY, multiplier: 1.0, constant: 0.0)
    centerY1.priority = 800
    let centerY21 = NSLayoutConstraint(item: headerImageView1, attribute: .CenterY, relatedBy: .Equal, toItem: headerView, attribute: .CenterY, multiplier: 1.0, constant: 24.0)
    centerY21.priority = 900
    let width1 = NSLayoutConstraint(item: headerImageView1, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 84.0)
    let height1 = NSLayoutConstraint(item: headerImageView1, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 36.0)
    height1.priority = 900
    let height21 = NSLayoutConstraint(item: headerImageView1, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 44.0)
    let bottom1 = NSLayoutConstraint(item: headerView, attribute: .Bottom, relatedBy: .GreaterThanOrEqual, toItem: headerImageView1, attribute: .Bottom, multiplier: 1.0, constant: 18.0)
    bottom1.priority = 900
    let bottom21 = NSLayoutConstraint(item: headerView, attribute: .Bottom, relatedBy: .GreaterThanOrEqual, toItem: headerImageView1, attribute: .Bottom, multiplier: 1.0, constant: 0)
    let top1 = NSLayoutConstraint(item: headerImageView1, attribute: .Top, relatedBy: .GreaterThanOrEqual, toItem: headerView, attribute: .Top, multiplier: 1.0, constant: 20.0)
    headerView.addConstraint(centerX1)
    headerView.addConstraint(centerY1)
    headerView.addConstraint(centerY21)
    headerView.addConstraint(width1)
    headerView.addConstraint(height1)
    headerView.addConstraint(height21)
    headerView.addConstraint(bottom1)
    headerView.addConstraint(bottom21)
    headerView.addConstraint(top1)
    
    let centerX2 = NSLayoutConstraint(item: headerImageView2, attribute: .CenterX, relatedBy: .Equal, toItem: headerView, attribute: .CenterX, multiplier: 1.0, constant: 13.0)
    let centerY22 = NSLayoutConstraint(item: headerImageView2, attribute: .CenterY, relatedBy: .Equal, toItem: headerView, attribute: .CenterY, multiplier: 1.0, constant: 11.0)
    centerY22.priority = 900
    let width2 = NSLayoutConstraint(item: headerImageView2, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 13.0)
    let height22 = NSLayoutConstraint(item: headerImageView2, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 10.0)
    headerView.addConstraint(centerX2)
    headerView.addConstraint(centerY22)
    headerView.addConstraint(width2)
    headerView.addConstraint(height22)
    
    let centerX3 = NSLayoutConstraint(item: headerImageView3, attribute: .CenterX, relatedBy: .Equal, toItem: headerView, attribute: .CenterX, multiplier: 1.0, constant: 50.0)
    let centerY23 = NSLayoutConstraint(item: headerImageView3, attribute: .CenterY, relatedBy: .Equal, toItem: headerView, attribute: .CenterY, multiplier: 1.0, constant: 8.0)
    centerY23.priority = 900
    let width3 = NSLayoutConstraint(item: headerImageView3, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 58.0)
    let height23 = NSLayoutConstraint(item: headerImageView3, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 26.0)
    headerView.addConstraint(centerX3)
    headerView.addConstraint(centerY23)
    headerView.addConstraint(width3)
    headerView.addConstraint(height23)
    
    navigationController!.view.layoutIfNeeded()
    
    headerHeightConstraint.constant = view.bounds.size.height - 35
    centerX1.constant = 0.0
    centerY21.constant = 10.0
    width1.constant = 142.0
    height1.constant = 47.0
    
    centerX2.constant = -60.0
    centerY22.constant = 43.0
    width2.constant = 22.0
    height22.constant = 25.0
    
    centerX3.constant = -7.0
    centerY23.constant = 52.0
    width3.constant = 98.0
    height23.constant = 41.0
    UIView.animateWithDuration(0.5, animations: { () -> Void in
      self.navigationController!.view.layoutIfNeeded()
    }, completion: completionHandler)
  }
  
  func goToLogin () {
    animateClose { (finished) -> () in
      if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
        appDelegate.loadLoginFlow()
      }
    }
  }
}

extension SettingsViewController: LocationManagerDelegate {
  
  func didUpdateLocation(userLocation: CLLocation) {
    homeViewController?.updateClosestBar(userLocation)
  }
  
  func didRespondToRequestWithStatus(status: CLAuthorizationStatus) {
    locationSwitch.on = status == .Authorized
  }
}

extension SettingsViewController: RegisterViewControllerDelegate {
  
  func registerViewControllerDidSave(viewController: RegisterViewController, success: Bool) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}

extension SettingsViewController: TermsViewControllerDelegate {
  
  func termsViewControllerDidFinish(viewController: TermsViewController, success: Bool) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
