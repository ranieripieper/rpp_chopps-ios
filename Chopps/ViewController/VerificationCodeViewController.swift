//
//  VerificationCodeViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/24/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class VerificationCodeViewController: UIViewController {
  
  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var verifyButton: UIButton!
  @IBOutlet weak var drinkTypeImageView: UIImageView!
  @IBOutlet weak var drinkNameLabel: UILabel!
  @IBOutlet weak var drinkVolumeLabel: UILabel!
  @IBOutlet weak var stripView: UIView!
  @IBOutlet weak var verifyButtonBottomSpaceMinimumConstraint: NSLayoutConstraint!
  
  var loadingView: LoadingView?
  var package: Package?
  var drink: Drink?
  var alertView: AlertController?
  var keyboardObserver: NotificationObserver?
  
  deinit {
    println("\(self) is being deinitialized")
    loadingView?.stop()
    keyboardObserver = nil
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    textField.setPaddings(8)
    enableKeyboardObserver()
    if UIScreen.mainScreen().bounds.height < 500 {
      stripView.backgroundColor = view.backgroundColor
    }
    configure()
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textField.becomeFirstResponder()
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let beerConsumedViewController = segue.destinationViewController as? BeerConsumedViewController {
      beerConsumedViewController.package = package
      beerConsumedViewController.consumed = true
      beerConsumedViewController.drink = drink
      navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    }
  }
  
  @IBAction func verifyButtonTapped (sender: UIButton) {
    if package != nil {
      if loadingView == nil {
        loadingView = LoadingView(superview: navigationController!.navigationBar)
      }
      loadingView!.start()
      drink?.drinkWithCode(textField.text, userPackageId: package!.userPackageId!, completionHandler: { (success) -> () in
        self.loadingView?.stop()
        if success {
          self.performSegueWithIdentifier("SegueBeerConsumed", sender: nil)
        } else {
          self.alertView = AlertController(title: "Chopps App", message: "Não foi possível completar a chamada", buttons: nil, cancelButton: ("Ok", {
            self.alertView = nil
          }), style: .Alert)
          self.alertView!.alertInViewController(self)
        }
      })
    }
  }
  
  func enableVerifyButton (enable: Bool) {
    verifyButton.enabled = enable
  }
  
  func enableKeyboardObserver() {
    keyboardObserver = NotificationObserver(notification: Notification(name: UIKeyboardWillShowNotification)) { userInfo in
      if let frameValue = userInfo![UIKeyboardFrameEndUserInfoKey] as? NSValue {
        self.verifyButtonBottomSpaceMinimumConstraint.constant = frameValue.CGRectValue().height + 20
        UIView.animateWithDuration(0.3, animations: { () -> Void in
          self.view.layoutIfNeeded()
        })
      }
    }
  }
  
  func configure() {
    drinkTypeImageView.image = UIImage(named: package!.type == .Chopp ? "img_code_chopp" : "img_code_beer")
    drinkNameLabel.text = drink!.brand
    drinkVolumeLabel.text = "\(drink!.volume)ml"
  }
}
