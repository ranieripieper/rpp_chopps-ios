//
//  BarDetailViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/23/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class BarDetailViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var tableViewDelegate: BarDetailTableViewDelegate!
  
  var locationManager = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager
  var loadingView: LoadingView?
  var alertView: AlertController?
  var person: Person?
  var bar: Bar? {
    didSet {
      tableViewDelegate?.bar = bar
      setBarDistance()
      loadingView?.start()
      bar!.getBarInfo {
        self.loadingView?.stop()
        if self.person != nil {
          self.tableViewDelegate.datasource = []
          let bought = self.bar!.packages.filter({ barPackage in
            barPackage.state == Package.PackageState.Owned
          })
          if bought.count > 0 {
            let data: (Package.PackageState, [Package]) = (.Owned, bought)
            self.tableViewDelegate.datasource.append(data)
          }
          let available = self.bar!.packages.filter({ barPackage in
            barPackage.state == Package.PackageState.Available
          })
          if available.count > 0 {
            let data: (Package.PackageState, [Package]) = (.Available, available)
            self.tableViewDelegate.datasource.append(data)
          }
        }
        self.setBarDistance()
        self.tableView.reloadData()
      }
    }
  }
  
  deinit {
    println("\(self) is being deinitialized")
    loadingView?.stop()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    if loadingView == nil && navigationController != nil {
      loadingView = LoadingView(superview: navigationController!.navigationBar)
      loadingView!.start()
    }
    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    tableView.registerNib(UINib (nibName: BarDetailCell.defaultIdentifier(), bundle: nil), forCellReuseIdentifier: BarDetailCell.defaultIdentifier())
    tableView.registerNib(UINib(nibName: BarDetailPackageCell.defaultIdentifier(), bundle: nil), forCellReuseIdentifier: BarDetailPackageCell.defaultIdentifier())
    tableView.registerNib(UINib(nibName: "BarDetailPackagesHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "BarDetailPackagesHeader")
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = false
    locationManager.locationManagerDelegate = self
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let beerSelectionViewController = segue.destinationViewController as? BeerSelectionViewController {
      if let package = sender as? Package {
        beerSelectionViewController.package = package
      }
    } else if let paymentMethodViewController = segue.destinationViewController as? PaymentMethodViewController {
      paymentMethodViewController.package = sender as Package!
    } else if let addPaymentMethodViewController = segue.destinationViewController as? AddPaymentMethodViewController {
      addPaymentMethodViewController.completionHandler = ({
        self.navigationController?.popViewControllerAnimated(false)
        self.performSegueWithIdentifier("SeguePaymentMethod", sender: sender)
      })
    } else if let beerConsumedViewController = segue.destinationViewController as? BeerConsumedViewController {
      if let package = sender as? Package {
        beerConsumedViewController.package = package
      }
    } else if let packageDetailsViewController = segue.destinationViewController as? PackageDetailViewController {
      if let dict = sender as? [String: AnyObject] {
        packageDetailsViewController.package = dict["package"] as? Package
        packageDetailsViewController.bought = dict["bought"] as Bool
      }
      packageDetailsViewController.delegate = self
    } else if let verificationCodeViewController = segue.destinationViewController as? VerificationCodeViewController {
      if let dict = sender as? [String: AnyObject] {
        verificationCodeViewController.package = dict["package"] as? Package
        verificationCodeViewController.drink = dict["drink"] as? Drink
      }
    }
  }
  
  func updateDistanceWithLocation (location: CLLocation) {
    let distance = locationManager.userLocation?.distanceFromLocation(location)
    println("distance is: \(distance)")
  }
  
  func setBarDistance() {
    var barDistance: Double = bar!.location.distanceBetween(locationManager.userLocation) / 1000
    if bar!.location.coordinate.latitude == 0.0 || bar!.location.coordinate.longitude == 0.0 || locationManager.userLocation == nil {
      tableViewDelegate?.barDistance = "--km"
    } else {
      tableViewDelegate?.barDistance = String(format: "%.1fkm", barDistance)
    }
  }
}

extension BarDetailViewController: PackageDetailViewControllerDelegate {
  func goToPaymentForPackage(package: Package) {
    if PaymentMethod.hasPaymentMethods() {
      performSegueWithIdentifier("SeguePaymentMethod", sender: package)
    } else {
      performSegueWithIdentifier("SegueAddPaymentMethod", sender: package)
    }
  }
  
  func goToVerificationCode(package: Package, drink: Drink) {
    performSegueWithIdentifier("SegueCode", sender: ["package": package, "drink": drink])
  }
  
  func goToBeerSelection(package: Package) {
    performSegueWithIdentifier("SegueGetABeer", sender: package)
  }
}

extension BarDetailViewController: BarDetailCellDelegate {
  
  func getaBeerButtonTapped(barDetailCell: BarDetailCell) {
    performSegueWithIdentifier("SegueGetABeer", sender: nil)
  }
  
  func call(barDetailCell: BarDetailCell) {
//    let phone = (bar!.phone as NSString).stringByReplacingOccurrencesOfString(" ", withString: "", options: .CaseInsensitiveSearch, range: NSMakeRange(0, countElements(bar!.phone!)))
//    UIApplication.sharedApplication().openURL(NSURL(string: "tel:\(phone)")!)
  }
  
  func drive(barDetailCell: BarDetailCell) {
    let hasWaze = canOpenAppWithURL("waze://")
    let hasGoogleMaps = canOpenAppWithURL("comgooglemaps://")
    if hasWaze || hasGoogleMaps {
      alertView = AlertController(title: "Escolha um aplicativo", message: nil, buttons: [("Waze", {
        self.openWaze()
        self.alertView = nil
      }), ("Google Maps", {
        self.openGoogleMaps()
        self.alertView = nil
      }), ("Apple Maps", {
        self.openMaps()
        self.alertView = nil
      })], cancelButton: ("Cancelar", {
        self.alertView = nil
      }), style: .ActionSheet)
      alertView!.alertInViewController(self)
    } else {
      openMaps()
    }
  }

  func openWaze() {
    let url = "waze://?ll=\(bar!.location.latitude),\(bar!.location.longitude)&navigate=yes"
    UIApplication.sharedApplication().openURL(NSURL(string: url)!)
  }

  func openGoogleMaps() {
    let url = "comgooglemaps://?center=\(bar!.location.latitude),\(bar!.location.longitude)&zoom=16&views=traffic&q=\(bar!.name)"
    UIApplication.sharedApplication().openURL(NSURL(string: url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!)
  }

  func openMaps() {
    let place = MKPlacemark(coordinate: bar!.location.coordinate, addressDictionary: nil)
    let mapItem = MKMapItem(placemark: place)
    mapItem.name = bar!.name
    mapItem.openInMapsWithLaunchOptions(nil)
  }
  
  private func canOpenAppWithURL(url: String) -> Bool {
    return UIApplication.sharedApplication().canOpenURL(NSURL(string: url)!)
  }
}

extension BarDetailViewController: LocationManagerDelegate {
  
  func didUpdateLocation(userLocation: CLLocation) {
    updateDistanceWithLocation(userLocation)
  }
}

//extension BarDetailViewController: BarDetailPackagesCellDelegate {
//  
//  func buyPackage(package: Package) {
//    if PaymentMethod.hasPaymentMethods() {
//      performSegueWithIdentifier("SeguePaymentMethod", sender: package)
//    } else {
//      performSegueWithIdentifier("SegueAddPaymentMethod", sender: package)
//    }
//  }
//}

extension BarDetailViewController: BarDetailPackageCellDelegate {
  
  func buyPackage(package: Package) {
    goToPaymentForPackage(package)
  }
  
  func consumePackage(package: Package) {
    let notConsumedDrinks = package.drinks.filter() { drink in
      drink.consumed < drink.amount
    }
    if notConsumedDrinks.count == 1 {
      performSegueWithIdentifier("SegueCode", sender: ["package": package, "drink": notConsumedDrinks.first!])
    } else {
      performSegueWithIdentifier("SegueGetABeer", sender: package)
    }
  }
  
  func moreDetailsFromPackage(package: Package, cell: BarDetailPackageCell) {
    let indexPath = tableView.indexPathForCell(cell)
    performSegueWithIdentifier("SeguePackageDetails", sender: ["package": package, "bought": indexPath!.section == 1])
  }
}
