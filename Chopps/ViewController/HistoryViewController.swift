//
//  HistoryViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 11/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var historyTableViewDelegate: HistoryTableViewDelegate!
  @IBOutlet weak var pseudoBackgroundViewHeightConstraint: NSLayoutConstraint!
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.contentInset = UIEdgeInsets(top: 64, left: 0, bottom: 64, right: 0)
    Person.persistedUser()?.getUserHistory() { actives, package, paymentTuple in
      self.historyTableViewDelegate.person = Person.persistedUser()
      self.historyTableViewDelegate.latestChoppTakenPackage = package
      if paymentTuple != nil {
        self.historyTableViewDelegate.latestPurchasedPackage = paymentTuple!.0
        self.historyTableViewDelegate.latestPurchasePaymentMethod = paymentTuple!.1
        self.historyTableViewDelegate.latestPurchaseDateString = paymentTuple!.2
      }
      self.historyTableViewDelegate.actives = actives as? [String: Int] ?? self.historyTableViewDelegate.actives
      self.tableView.reloadData()
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = false
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let myPackagesViewController = segue.destinationViewController as? MyPackagesViewController {
      myPackagesViewController.activePackages = sender!.boolValue
    } else if let packageDetailViewController = segue.destinationViewController as? PackageDetailViewController {
      packageDetailViewController.package = sender as? Package
      packageDetailViewController.bought = true
      packageDetailViewController.delegate = self
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
  
  func handleScrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView == tableView {
      pseudoBackgroundViewHeightConstraint.constant = view.bounds.height - scrollView.contentOffset.y
    }
  }
  
  func showPackageDetails(package: Package) {
    performSegueWithIdentifier("SeguePackageDetails", sender: package)
  }
}

extension HistoryViewController: HistoryPackagesCellDelegate {
  func goToDetail(cell: HistoryPackagesCell) {
    let indexPath = tableView.indexPathForCell(cell)
    var sender: NSNumber
    if indexPath?.row == 3 {
      sender = 1
    } else {
      sender = 0
    }
    performSegueWithIdentifier("SegueMyPackages", sender: sender)
  }
}

extension HistoryViewController: PackageDetailViewControllerDelegate {
  func goToPaymentForPackage(package: Package) {
    if PaymentMethod.hasPaymentMethods() {
      performSegueWithIdentifier("SeguePaymentMethod", sender: package)
    } else {
      performSegueWithIdentifier("SegueAddPaymentMethod", sender: package)
    }
  }
  
  func goToBeerSelection(package: Package) {
    performSegueWithIdentifier("SegueBeerSelection", sender: package)
  }
  
  func goToVerificationCode(package: Package, drink: Drink) {
    performSegueWithIdentifier("SegueCode", sender: [package: drink])
  }
}
