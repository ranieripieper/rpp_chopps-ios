//
//  RegisterViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol RegisterViewControllerDelegate {
  optional func registerViewControllerDidFinish(viewController: RegisterViewController, success: Bool, closestBar: Bar?)
  optional func registerViewControllerDidSave(viewController: RegisterViewController, success: Bool)
  optional func registerViewControllerDidRequest(viewController: RegisterViewController)
}

class RegisterViewController: UIViewController {

  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var avatarButton: UIButton!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var birthDateTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var passwordConfirmationTextField: UITextField!
  @IBOutlet weak var maskView: UIView!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var textViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var registerButton: UIButton!
  @IBOutlet weak var deleteProfileButton: UIButton!
  @IBOutlet weak var deleteProfileButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var deleteProfileButtonTopSpaceConstraint: NSLayoutConstraint!
  
  var avatar: UIImage?
  weak var delegate: RegisterViewControllerDelegate?
  var keyboardHeight: CGFloat?
  var isNewProfile: Bool = true
  var isNewAvatar: Bool = false
  var alertView: AlertController?
  let dateMask = "99/99/9999"
  var locationManager = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.gestureRecognizers = [tap]
    if !IOS_8() {
      modalPresentationStyle = .CurrentContext
      modalPresentationStyle = .FormSheet
    }
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardUp:", name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardDown:", name: UIKeyboardWillHideNotification, object: nil)
    nameTextField.setPaddings()
    emailTextField.setPaddings()
    birthDateTextField.setPaddings()
    passwordTextField.setPaddings()
    passwordConfirmationTextField.setPaddings()
    if isNewProfile {
      deleteProfileButtonHeightConstraint.constant = 0
      deleteProfileButtonTopSpaceConstraint.constant = 0
      registerButton.setTitle("CADASTRAR", forState: .Normal)
    } else {
      prefillProfile()
    }
    deleteProfileButton.hidden = isNewProfile
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    textViewWidthConstraint.constant = view.bounds.size.width - 100
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    maskView.hidden = false;
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    maskView.hidden = true
    view.clipsToBounds = true
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
  }
  
  @IBAction func tapped (tap: UITapGestureRecognizer) {
    if isNewProfile {
      delegate?.registerViewControllerDidFinish?(self, success: false, closestBar: nil)
    } else {
      delegate?.registerViewControllerDidSave?(self, success: false)
    }
  }
  
  @IBAction func register () {
    if isNewProfile {
      if doesPasswordsMatch(passwordTextField.text, passwordConfirmation: passwordConfirmationTextField.text) {
        if verifyAge(birthDateTextField.text) {
          delegate?.registerViewControllerDidRequest?(self)
          Person.registerUserWithName(nameTextField.text, email: emailTextField.text, birthDate: birthDateTextField.text, password: passwordTextField.text, facebookToken: nil, facebookID: nil, location: locationManager.userLocation, pictureData: avatar, completionHandler: { (success: Bool, closestBar: Bar?) -> () in
            if success {
              self.delegate?.registerViewControllerDidFinish?(self, success: true, closestBar: closestBar)
            } else {
              self.alertView = AlertController(title: "Chopps App", message: "Não foi possível completar a chamada", buttons: nil, cancelButton: ("Ok", {
                self.alertView = nil
              }), style: .Alert)
              self.alertView!.alertInViewController(self)
            }
          })
        } else {
          alertView = AlertController(title: "Chopps & Beer", message: "É preciso ter mais de 18 anos para utilizar este aplicativo.", buttons: nil, cancelButton: ("Ok", {
            self.alertView = nil
          }), style: .Alert)
          alertView?.alertInViewController(self)
        }
      }
    } else {
      if (countElements(passwordTextField.text) == 0 && countElements(passwordConfirmationTextField.text) == 0) || doesPasswordsMatch(passwordTextField.text, passwordConfirmation: passwordConfirmationTextField.text) {
        var params = [String: AnyObject]()
        if countElements(nameTextField.text) > 0 {
          params["name"] = nameTextField.text
        }
        if countElements(emailTextField.text) > 0 {
          params["email"] = emailTextField.text
        }
        if countElements(birthDateTextField.text) > 0 {
          params["phone"] = birthDateTextField.text
        }
        if countElements(passwordTextField.text) > 0 {
          params["password"] = passwordTextField.text
        }
        if isNewAvatar {
          params["avatar"] = self.avatar
        }
        let user = Person.persistedUser()
//        user?.updateProfileWithParams(params)
      }
    }
  }
  
  @IBAction func changePhoto () {
    let imagePicker = UIImagePickerController()
    imagePicker.delegate = self
    if UIImagePickerController.isCameraDeviceAvailable(.Rear) || UIImagePickerController.isCameraDeviceAvailable(.Front) {
      alertView = AlertController(title: "Escolha", message: nil, buttons: [("Camera", {
        imagePicker.sourceType = .Camera
        self.presentViewController(imagePicker, animated: true, completion: nil)
        self.alertView = nil
      }), ("Biblioteca", {
        imagePicker.sourceType = .PhotoLibrary
        self.presentViewController(imagePicker, animated: true, completion: nil)
        self.alertView = nil
      })], cancelButton: ("Cancelar", {
        self.alertView = nil
      }), style: .ActionSheet)
      alertView!.alertInViewController(self)
    } else {
      imagePicker.sourceType = .PhotoLibrary
      self.presentViewController(imagePicker, animated: true, completion: nil)
    }
  }
  
  func handleKeyboardUp (notification: NSNotification) {
    var notificationData = notification.userInfo
    var frame = notificationData![UIKeyboardFrameBeginUserInfoKey]! as NSValue
    let height = frame.CGRectValue().size.height
    scrollView.contentInset = UIEdgeInsetsMake(scrollView.contentInset.top, scrollView.contentInset.left, height, scrollView.contentInset.right)
    keyboardHeight = height
  }
  
  func handleKeyboardDown (notification: NSNotification) {
    scrollView.contentInset = UIEdgeInsetsMake(scrollView.contentInset.top, scrollView.contentInset.left, 0, scrollView.contentInset.right)
  }
  
  func doesPasswordsMatch (password: String, passwordConfirmation: String) -> Bool {
    return password == passwordConfirmation
  }
  
  func prefillProfile() {
    let person = Person.persistedUser()
    if countElements(person!.pictureURL) > 0 {
      let imageView = UIImageView(frame: CGRectZero)
      imageView.setImageWithURLRequest(NSURLRequest(URL: NSURL(string: person!.pictureURL)!), placeholderImage: nil, success: { (request, response, image) -> Void in
        self.avatarButton.setImage(image, forState: .Normal)
      }, failure: nil)
    }
    nameTextField.text = person!.name
    emailTextField.text = person!.email
//    birthDateTextField.text = person!.phone
  }
  
  func verifyAge(birthday: String) -> Bool {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    if let birthdate = dateFormatter.dateFromString(birthday) {
      let timeInterval = abs(birthdate.timeIntervalSinceNow)
      let age = timeInterval / 60 / 60 / 24 / 365
      return age >= 18
    }
    return false
  }
}

extension RegisterViewController: UIGestureRecognizerDelegate {
  
  func gestureRecognizer (gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
    return !touch.view.isKindOfClass(UITextField.classForCoder()) && !nameTextField.resignFirstResponder() && !emailTextField.resignFirstResponder() && !birthDateTextField.resignFirstResponder() && !passwordTextField.resignFirstResponder() && !passwordConfirmationTextField.resignFirstResponder() && gestureRecognizer == tap && touch.view == view
  }
}

extension RegisterViewController: UITextFieldDelegate {
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == nameTextField {
      emailTextField.becomeFirstResponder()
    } else if textField == emailTextField {
      birthDateTextField.becomeFirstResponder()
    } else if textField == birthDateTextField {
      passwordTextField.becomeFirstResponder()
    } else if textField == passwordTextField {
      passwordConfirmationTextField.becomeFirstResponder()
    } else {
      textField.resignFirstResponder()
      register()
    }
    return true
  }
  
  func validateDate(dateString: String) -> Bool {
    let regex = "^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$"
    let regexTest = NSPredicate(format: "SELF MATCHES %@", regex)
    return regexTest!.evaluateWithObject(dateString)
  }
  
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    let point = view.convertPoint(textField.frame.origin, fromView: textField.superview)
    if point.y > view.bounds.height - (keyboardHeight ?? 216) - textField.bounds.height + scrollView.contentOffset.y {
      scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y + (keyboardHeight ?? 216)), animated: true)
    }
    return true
  }
  
  func textFieldShouldEndEditing(textField: UITextField) -> Bool {
    if textField == birthDateTextField {
      if !validateDate(textField.text) && countElements(textField.text) > 0 {
        alertView = AlertController(title: "Chopps & Beer", message: "Por favor, digite uma data válida", buttons: nil, cancelButton: ("Ok", nil), style: .Alert)
        alertView?.alertInViewController(self)
        return false
      }
    }
    return true
  }
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    if textField == birthDateTextField {
      return birthDateTextField.shouldChangeCharactersInRange(range, replacementString: string, pattern: "xx/xx/xxxx")
    }
    return true
  }
}

extension RegisterViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
    dismissViewControllerAnimated(true, completion: { () -> Void in
      let image = info[UIImagePickerControllerOriginalImage] as UIImage
      if !self.isNewProfile {
        self.isNewAvatar = true
      }
      self.avatar = image
      self.avatarButton.layer.cornerRadius = self.avatarButton.bounds.size.height / 2
      self.avatarButton.imageView?.contentMode = .ScaleAspectFill
      self.avatarButton.setImage(image, forState: .Normal)
    })
  }
  
  func imagePickerControllerDidCancel(picker: UIImagePickerController) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
