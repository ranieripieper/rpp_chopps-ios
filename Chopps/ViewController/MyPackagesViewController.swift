//
//  MyPackagesViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 11/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MyPackagesViewController: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var myPackagesTableViewDelegate: MyPackagesTableViewDelegate!
  
  var loadingView: LoadingView?
  var locationManager = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager
  var activePackages = true
  
  deinit {
    println("\(self) is being deinitialized")
    loadingView?.stop()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.registerNib(UINib(nibName: MyPackageDetailCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: MyPackageDetailCell.defaultIdentifier())
    tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
    if loadingView == nil {
      loadingView = LoadingView(superview: navigationController!.navigationBar)
    }
    loadingView?.start()
    let person = Person.persistedUser()
    if activePackages {
      person?.getUserPackages(activePackages, completionHandler: { (bars: [Bar]) -> () in
        self.loadingView?.stop()
        self.myPackagesTableViewDelegate.bars = bars
        self.tableView.reloadData()
      })
    } else {
      person?.getUserPackages(activePackages, completionHandler: { (bars: [Bar]) -> () in
        self.loadingView?.stop()
        self.myPackagesTableViewDelegate.bars = bars
        self.tableView.reloadData()
      })
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = false
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let verificationCodeViewController = segue.destinationViewController as? VerificationCodeViewController {
      verificationCodeViewController.package = sender as Package!
    }
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
  }
}

extension MyPackagesViewController: MyPackageDetailCellDelegate {
  
  func getABeerFromPackage(package: Package) {
    performSegueWithIdentifier("SegueCode", sender: package)
  }
}
