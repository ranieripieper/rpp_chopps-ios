//
//  SearchBarViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/15/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class SearchBarViewController: UIViewController {

  @IBOutlet weak var navigationBarLogoImageView: UIImageView!
  @IBOutlet weak var mapView: MapView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchView: SearchView!
  @IBOutlet weak var searchViewBottomSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var tableViewTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var textField: UITextField!
  
  var tableViewContentInsetTop: CGFloat = 0.0
  var bars: [Bar] = []
  var shouldUpdateMap = true
  var locationManager = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager
  var loadingView: LoadingView?
  var person: Person?
  var isKeyboardUp = false
  var isLoading = false
  var currentPage = 1
  var shouldReload = false
  
  deinit {
    println("\(self) is being deinitialized")
    loadingView?.stop()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    println("bounds: \(UIScreen.mainScreen().bounds), tableView: \(tableView.bounds)")
    tableViewContentInsetTop = UIScreen.mainScreen().bounds.height - 224 - 138//(view.bounds.height - 398)
    tableView.registerNib(UINib(nibName: BarCell.defaultIdentifier(), bundle: nil), forCellReuseIdentifier: BarCell.defaultIdentifier())
    tableView.contentInset = UIEdgeInsets(top: tableViewContentInsetTop, left: 0, bottom: 0, right: 0)
    mapView.userInteractionEnabled = false
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = false
    shouldReload = true
    searchBarsWithTerm(nil, page: 1)
    locationManager.locationManagerDelegate = self
    if tableView.indexPathForSelectedRow() != nil {
      tableView.deselectRowAtIndexPath(tableView.indexPathForSelectedRow()!, animated: true)
    }
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleKeyboardUp:", name: UIKeyboardWillShowNotification, object: nil)
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if let barDetailViewController = segue.destinationViewController as? BarDetailViewController {
      barDetailViewController.locationManager = locationManager
      barDetailViewController.person = person
      if let bar = sender as? Bar {
        barDetailViewController.bar = bar
      }
      navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
      closeMap()
    }
  }
  
  @IBAction func toggleMap (sender: UIButton) {
    if self.shouldUpdateMap {
      openMap()
    } else {
      closeMap()
    }
    if isKeyboardUp {
      textField.resignFirstResponder()
    }
  }
  
  @IBAction func centerMapUserLocation(sender: UIButton) {
    searchBarsWithTerm(textField.text, page: 1)
  }
  
  func searchBarsWithTerm(term: String?, page: Int) {
    if isLoading || !shouldReload {
      return
    }
    isLoading = true
    if loadingView == nil {
      loadingView = LoadingView(superview: navigationController!.navigationBar)
    }
    loadingView!.start()
    println("search page \(page)")
    Bar.getBarsWithLocation(locationManager.userLocation, search: term, page: page) { bars in
      self.shouldReload = bars.count > 0
      self.currentPage = page
      self.isLoading = false
      self.loadingView?.stop()
      if page == 1 {
        self.bars.removeAll(keepCapacity: false)
      }
      self.bars.addElementsOf(bars)
      self.addBarsToMap()
      self.tableView.reloadData()
    }
  }
  
  func updateMap () {
    if locationManager.userLocation != nil && shouldUpdateMap {
      let bottomPadding = tableView.bounds.height + min(tableView.contentOffset.y, 0) + 44
      var zoomRect = MKMapRectNull
      let upperPoint = MKMapPointForCoordinate(CLLocationCoordinate2D (latitude: locationManager.userLocation!.coordinate.latitude - 0.002, longitude: locationManager.userLocation!.coordinate.longitude))
      let upperRect = MKMapRect (origin: upperPoint, size: MKMapSize (width: 0.1, height: 0.1))
      zoomRect = MKMapRectUnion(zoomRect, upperRect)
      let bottomPoint = MKMapPointForCoordinate(CLLocationCoordinate2D (latitude: locationManager.userLocation!.coordinate.latitude + 0.002, longitude: locationManager.userLocation!.coordinate.longitude))
      let bottomRect = MKMapRect (origin: bottomPoint, size: MKMapSize (width: 0.1, height: 0.1))
      zoomRect = MKMapRectUnion(zoomRect, bottomRect)
      mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets (top: 0, left: 0, bottom: bottomPadding, right: 0), animated: false)
    }
  }
  
  func addBarsToMap () {
    mapView.addAnnotations(bars)
  }
  
  func openMap () {
    tableView.setContentOffset(CGPoint (x: 0, y: -view.bounds.height + tableViewContentInsetTop + 18), animated: true)
    tableView.userInteractionEnabled = false
    mapView.userInteractionEnabled = true
    delay(0.3, { () -> () in
      self.shouldUpdateMap = false
      self.mapView.removeTapGesture()
      self.searchView.addTapGesture()
    })
  }
  
  func closeMap () {
    tableView.setContentOffset(CGPoint (x: 0, y: -tableViewContentInsetTop), animated: true)
    mapView.userInteractionEnabled = false
    delay(0.3, { () -> () in
      self.shouldUpdateMap = true
      self.mapView.addTapGesture()
      self.searchView.removeTapGesture()
      self.tableView.userInteractionEnabled = true
    })
  }
  
  func handleKeyboardUp(notification: NSNotification) {
    let durationValue = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as NSNumber
    let duration = durationValue.doubleValue
    let frameValue = notification.userInfo![UIKeyboardFrameEndUserInfoKey] as NSValue
    let frame = frameValue.CGRectValue()
    
    enterSearchModeWithKeyboardHeight(frame.height)
  }
  
  func enterSearchModeWithKeyboardHeight(keyboardHeight: CGFloat) {
    isKeyboardUp = true
    searchView.expandTextField()
    searchViewBottomSpaceConstraint.constant = view.frame.height - 20 - 44 - searchView.frame.height
    tableViewTopSpaceConstraint.constant = 20 + 44 + searchView.frame.height
    tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardHeight, right: 0)
    navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: "exitSearchMode")]
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
    }) { (finished) -> Void in
      self.tableView.backgroundColor = UIColor(red: 50.0/255, green: 50.0/255, blue: 50.0/255, alpha: 1.0)
    }
  }
  
  func exitSearchMode() {
    searchView.contractTextField()
    searchView.textField.resignFirstResponder()
    searchViewBottomSpaceConstraint.constant = 138
    tableViewTopSpaceConstraint.constant = 224
    navigationItem.rightBarButtonItems = []
    tableView.contentInset = UIEdgeInsets(top: tableViewContentInsetTop, left: 0, bottom: 0, right: 0)
    tableView.backgroundColor = UIColor.clearColor()
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.view.layoutIfNeeded()
      self.tableView.setContentOffset(CGPoint(x: 0, y: -self.tableViewContentInsetTop), animated: false)
    }) { (finished) -> Void in
      self.isKeyboardUp = false
    }
  }
}

extension SearchBarViewController: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return bars.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let bar = bars[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(BarCell.defaultIdentifier(), forIndexPath: indexPath) as BarCell
    let barDistance = bar.location.distanceBetween(locationManager.userLocation!) / 1000
    let barDistanceString = String(format: "%.1fkm", barDistance)
    cell.configureWithBar(bar, barDistance: barDistanceString, shouldShowPackages: false)
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let bar = bars[indexPath.row]
    performSegueWithIdentifier("SegueBarDetail", sender: bar)
  }
}

extension SearchBarViewController: UIScrollViewDelegate {
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if isKeyboardUp { return }
    
    let value = tableViewContentInsetTop + min(tableView.contentOffset.y, 0.0)
    searchViewBottomSpaceConstraint.constant = 138 + value
    updateMap()
    
    if scrollView.contentOffset.y + scrollView.bounds.height >= scrollView.contentSize.height * 0.8 {
      searchBarsWithTerm(textField.text, page: currentPage + 1)
    }
  }
}

extension SearchBarViewController: MKMapViewDelegate {
  
  func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
    if let bar = annotation as? Bar {
      var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("Bar")
      if annotationView == nil {
        annotationView = MKAnnotationView (annotation: bar, reuseIdentifier: "Bar")
        annotationView.canShowCallout = true
        annotationView.enabled = true
        annotationView.image = UIImage (named: "img_pin")
        let buttonView = UIButton.buttonWithType(.DetailDisclosure) as UIView
        buttonView.tintColor = UIColor.blackColor()
        annotationView.rightCalloutAccessoryView = buttonView
      } else {
        annotationView.annotation = bar
      }
      return annotationView
    }
    return nil
  }
  
  func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
    if let bar = view.annotation as? Bar {
      performSegueWithIdentifier("SegueBarDetail", sender: bar)
    }
  }
}

extension SearchBarViewController: LocationManagerDelegate {
  
  func didUpdateLocation(userLocation: CLLocation) {
    updateMap()
  }
}

extension SearchBarViewController: UITextFieldDelegate {
  
  func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if !shouldUpdateMap {
      closeMap()
      delay(0.3, { () -> () in
        self.tableView.setContentOffset(CGPointZero, animated: true)
      })
    }
    return true
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    exitSearchMode()
    searchBarsWithTerm(textField.text, page: 1)
    return true
  }
}
