//
//  PurchaseSummaryViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 2/27/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class PurchaseSummaryViewController: UIViewController {
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var barCellView: BarCellView!
  @IBOutlet weak var barImageView: UIImageView!
  @IBOutlet weak var packageTypeImageView: UIImageView!
  @IBOutlet weak var packageTypeLabel: UILabel!
  @IBOutlet weak var drink1TypeImageView: UIImageView!
  @IBOutlet weak var drink1BrandLabel: UILabel!
  @IBOutlet weak var drink1VolumeLabel: UILabel!
  @IBOutlet weak var drink2TypeImageView: UIImageView!
  @IBOutlet weak var drink2BrandLabel: UILabel!
  @IBOutlet weak var drink2VolumeLabel: UILabel!
  @IBOutlet weak var drink3TypeImageView: UIImageView!
  @IBOutlet weak var drink3BrandLabel: UILabel!
  @IBOutlet weak var drink3VolumeLabel: UILabel!
  @IBOutlet weak var originalPriceLabel: UILabel!
  @IBOutlet weak var salePriceLabel: UILabel!
  @IBOutlet weak var expirationDateLabel: UILabel!
  @IBOutlet weak var barCellViewWidthConstraint: NSLayoutConstraint!
  
  var loadingView: LoadingView?
  var package: Package?
  var paymentMethod: PaymentMethod?
  var alertView: AlertController?

  override func viewDidLoad() {
    super.viewDidLoad()
    barCellViewWidthConstraint.constant = UIScreen.mainScreen().bounds.width
    configureWithPackage(package!)
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let beerBoughtViewController = segue.destinationViewController as? BeerConsumedViewController {
      beerBoughtViewController.package = package
    }
  }
  
  @IBAction func confirmTapped(sender: UIButton) {
    if self.loadingView == nil {
      self.loadingView = LoadingView(superview: self.navigationController!.navigationBar)
    }
    self.loadingView!.start()
    self.package?.buy(self.paymentMethod!, completionHandler: { (package) -> () in
      self.loadingView?.stop()
      if package != nil {
        self.performSegueWithIdentifier("SegueChoppBought", sender: package)
      } else {
        self.alertView = AlertController(title: "Chopps App", message: "Ocorreu um erro durante a compra, por favor, tente novamente", buttons: nil, cancelButton: ("Ok", {
          self.alertView = nil
        }), style: .Alert)
        self.alertView!.alertInViewController(self)
      }
    })
  }
  
  func configureWithPackage(package: Package) {
    barCellView.name = package.bar?.name
    barCellView.shouldShowDistance = false
    barCellView.setNeedsDisplay()
    packageTypeLabel.text = package.type == .Chopp ? "Pacote de Chopp com" : "Pacote de Cerveja com"
    barImageView.setImageWithURL(NSURL(string: package.bar!.imageURL))
    packageTypeImageView.image = UIImage(named: package.type == .Chopp ? "icn_detail_bar_chopp" : "icn_detail_bar_cerveja")
    let fontDict = [NSFontAttributeName: UIFont(name: "GillSans", size: 16)!]
    let boldFontDict = [NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 20)!]
    if package.drinks.count > 0 {
      drink1TypeImageView.image = UIImage(named: package.type == .Chopp ? "img_code_chopp" : "img_code_beer")
      let string = String(format: "%d %@", package.drinks[0].amount, package.drinks[0].brand)
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(package.drinks[0].amount)))
      drink1BrandLabel.attributedText = attr
      drink1VolumeLabel.text = "(\(package.drinks[0].volume)ml)"
    }
    if package.drinks.count > 1 {
      drink2TypeImageView.image = UIImage(named: package.type == .Chopp ? "img_code_chopp" : "img_code_beer")
      let string = String(format: "%d %@", package.drinks[1].amount, package.drinks[1].brand)
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(package.drinks[1].amount)))
      drink2BrandLabel.attributedText = attr
      drink2VolumeLabel.text = "(\(package.drinks[1].volume)ml)"
    }
    if package.drinks.count > 2 {
      drink3TypeImageView.image = UIImage(named: package.type == .Chopp ? "img_code_chopp" : "img_code_beer")
      let string = String(format: "%d %@", package.drinks[2].amount, package.drinks[2].brand)
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(package.drinks[2].amount)))
      drink3BrandLabel.attributedText = attr
      drink3VolumeLabel.text = "(\(package.drinks[2].volume)ml)"
    }
    let attr = NSAttributedString(string: package.originalPrice, attributes: [NSStrikethroughStyleAttributeName: 1])
    originalPriceLabel.attributedText = attr
    salePriceLabel.text = package.salePrice
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yy"
    expirationDateLabel.text = "Válido até " + dateFormatter.stringFromDate(package.expirationDate ?? package.dateConsumption ?? NSDate(timeIntervalSince1970: 0))
  }
}
