//
//  BeerConsumedViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/24/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BeerConsumedViewController: UIViewController {
  @IBOutlet weak var barPackagesView: BarPackagesView!
  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var packageExpireLabel: UILabel!
  @IBOutlet weak var inviteFriendLabel: UILabel!
  @IBOutlet weak var drinkButton: UIButton!
  @IBOutlet weak var detailsButton: UIButton!
  
  var package: Package?
  var drink: Drink?
  var consumed = false
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if package != nil {
      configureDefault()
    }
    navigationItem.hidesBackButton = true
    navigationItem.leftBarButtonItems = []
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let verificationCodeViewController = segue.destinationViewController as? VerificationCodeViewController {
      verificationCodeViewController.package = package
      verificationCodeViewController.drink = package!.drinks[0]
    } else if let beerSelectionViewController = segue.destinationViewController as? BeerSelectionViewController {
      beerSelectionViewController.package = package
    } else if let packageDetailViewController = segue.destinationViewController as? PackageDetailViewController {
      packageDetailViewController.package = package
      packageDetailViewController.delegate = self
      packageDetailViewController.bought = true
    }
  }
  
  @IBAction func drinkTapped(sender: UIButton) {
    if package!.drinks.count > 1 {
      performSegueWithIdentifier("SegueBeerSelection", sender: nil)
    } else {
      performSegueWithIdentifier("SegueCode", sender: nil)
    }
  }
  
  @IBAction func yesButtonTapped (sender: UIButton) {
    var activityItems = [AnyObject]()
    if self.consumed {
      if let barName = package?.bar?.name {
        if let drinkBrand = drink?.brand {
          activityItems = ["Estou no bar \(barName) tomando uma \(drinkBrand)"]
        }
      }
    } else {
      if let barName = package?.bar?.name {
        let typeString = self.package!.type == .Chopp ? "Chopp's" : "Cerveja"
        activityItems = ["Comprei um pacote de \(typeString) do bar \(self.package?.bar?.name)"]
      }
    }
    let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
    activityViewController.completionHandler = { string, finished in
      self.navigationController?.popToRootViewControllerAnimated(true)
      self.refreshHome()
    }
    presentViewController(activityViewController, animated: true, completion: nil)
  }
  
  @IBAction func noButtonTapped (sender: UIButton) {
    navigationController?.popToRootViewControllerAnimated(true)
    refreshHome()
  }
  
  func configureDefault() {
    if consumed {
      drink!.consumed++
    }
    barPackagesView.package = package
    packageExpireLabel.text = "Válido até \(DateFormatter.dottedStringForDate(package!.expirationDate ?? package!.dateConsumption ?? NSDate(timeIntervalSince1970: 0)))"
    drinkButton.hidden = consumed
    detailsButton.hidden = consumed
    if consumed {
      configureForConsumed()
    } else {
      configureForBought()
    }
  }
  
  func configureForConsumed() {
    headerLabel.text = package!.type == .Chopp ? "UM CHOPP FOI DEBITADO DO TEU PACOTE." : "UMA CERVEJA FOI DEBITADA DO TEU PACOTE."
    let string = "Que tal convidar um amigo para vir pro bar te encontrar?"
    let attributedString = NSMutableAttributedString(string: string, attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
    attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "GillSans", size: 14)!, range: NSMakeRange(0, 7))
    attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "GillSans", size: 14)!, range: NSMakeRange(25, countElements(string) - 25))
    attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "GillSans-Bold", size: 14)!, range: NSMakeRange(8, 17))
    inviteFriendLabel.attributedText = attributedString
  }
  
  func configureForBought() {
    let string = "PRONTO!\nO PACOTE É TEU!"
    let attributedString = NSMutableAttributedString(string: string, attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
    headerLabel.attributedText = attributedString
    
    let string2 = "Que tal convidar um amigo para vir tomar um chopp contigo?"
    let attributedString2 = NSMutableAttributedString(string: string2, attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
    attributedString2.addAttribute(NSFontAttributeName, value: UIFont(name: "GillSans", size: 14)!, range: NSMakeRange(0, 7))
    attributedString2.addAttribute(NSFontAttributeName, value: UIFont(name: "GillSans", size: 14)!, range: NSMakeRange(25, countElements(string2) - 25))
    attributedString2.addAttribute(NSFontAttributeName, value: UIFont(name: "GillSans-Bold", size: 14)!, range: NSMakeRange(8, 17))
    inviteFriendLabel.attributedText = attributedString2
    
    drinkButton.setTitle("TOMAR " + (package!.type == .Chopp ? "CHOPP" : "CERVEJA"), forState: .Normal)
  }
  
  func refreshHome() {
    NSNotificationCenter.defaultCenter().postNotificationName("HomeViewControllerRefreshHome", object: nil)
  }
}

extension BeerConsumedViewController: PackageDetailViewControllerDelegate {
  func goToPaymentForPackage(package: Package) {
    
  }
  
  func goToVerificationCode(package: Package, drink: Drink) {
    performSegueWithIdentifier("SegueCode", sender: ["package": package, "drink": drink])
  }
  
  func goToBeerSelection(package: Package) {
    performSegueWithIdentifier("SegueGetABeer", sender: package)
  }
}
