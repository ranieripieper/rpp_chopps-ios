//
//  SearchPackageViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 10/31/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class SearchPackageViewController: UIViewController {
  
  @IBOutlet weak var searchTextField: UITextField!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  @IBOutlet weak var choppButton: UIButton!
  @IBOutlet weak var beerButton: UIButton!
  @IBOutlet weak var selectedImageView: UIImageView!
  @IBOutlet weak var sliderView: SliderView!
  
  var currentSearchType: Int?
  
  deinit {
    println("\(self) is being deinitialized")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addGestureRecognizer(tap)
  }

  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBarHidden = false
  }
  
  override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
    if identifier == "SegueSearchPackagesResults" {
      return currentSearchType != nil
    }
    return true
  }
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    navigationItem.backBarButtonItem = UIBarButtonItem (title: "", style: .Bordered, target: nil, action: nil)
    if let searchPackagesResultsViewController = segue.destinationViewController as? SearchPackagesResultsViewController {
      searchPackagesResultsViewController.value = sliderView.currentValue
      searchPackagesResultsViewController.type = currentSearchType!
    }
  }
  
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    searchTextField.resignFirstResponder()
  }
  
  @IBAction func selectChopp(sender: UIButton) {
    sender.selected = true
    beerButton.selected = false
    currentSearchType = 1
    moveSelectedToChopp()
  }
  
  @IBAction func selectBeer(sender: UIButton) {
    sender.selected = true
    choppButton.selected = false
    currentSearchType = 2
    moveSelectedToBeer()
  }
  
  @IBAction func searchTapped(sender: UIButton) {
    searchPackages()
  }
  
  func moveSelectedToChopp() {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: nil, animations: { () -> Void in
      self.selectedImageView.transform = CGAffineTransformIdentity
      }) { (finished) -> Void in
        self.selectedImageView.hidden = false
    }
  }
  
  func moveSelectedToBeer () {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: nil, animations: { () -> Void in
      self.selectedImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0.4 * UIScreen.mainScreen().bounds.width, 0.0)
      }) { (finished) -> Void in
        self.selectedImageView.hidden = false
    }
  }
  
  func searchPackages() {
    if currentSearchType == nil { return }
//    performSegueWithIdentifier("SegueSearchPackagesResults", sender: nil)
  }
}
