//
//  PackageDetailViewController.swift
//  Chopps
//
//  Created by Gilson Gil on 1/30/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

protocol PackageDetailViewControllerDelegate {
  func goToPaymentForPackage(package: Package)
  func goToBeerSelection(package: Package)
  func goToVerificationCode(package: Package, drink: Drink)
}

class PackageDetailViewController: UIViewController {
  @IBOutlet weak var packageTypeImageView: UIImageView!
  @IBOutlet weak var packageDetailLineImageView2: UIImageView!
  @IBOutlet weak var packageDetailLineImageView3: UIImageView!
  @IBOutlet weak var packageOriginalPriceLabel: UILabel!
  @IBOutlet weak var packagePriceLabel: UILabel!
  @IBOutlet weak var packageExpireLabel: UILabel!
  @IBOutlet weak var drink1NameLabel: UILabel!
  @IBOutlet weak var drink1VolumeLabel: UILabel!
  @IBOutlet weak var drink1AmountLabel: UILabel!
  @IBOutlet weak var drink2NameLabel: UILabel!
  @IBOutlet weak var drink2VolumeLabel: UILabel!
  @IBOutlet weak var drink2AmountLabel: UILabel!
  @IBOutlet weak var drink3NameLabel: UILabel!
  @IBOutlet weak var drink3VolumeLabel: UILabel!
  @IBOutlet weak var drink3AmountLabel: UILabel!
  @IBOutlet weak var actionButton: UIButton!
  @IBOutlet weak var tap: UITapGestureRecognizer!
  
  var delegate: PackageDetailViewControllerDelegate?
  var package: Package?
  var bought = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureWithPackage(package?)
    if !IOS_8() {
      modalPresentationStyle = .CurrentContext
      modalPresentationStyle = .FormSheet
    }
    if bought {
      let title = "TOMAR " + (package!.type == .Chopp ? "CHOPP" : "CERVEJA")
      actionButton.setTitle(title, forState: .Normal)
    }
    view.addGestureRecognizer(tap)
  }
  
  @IBAction func buyPackageTapped(sender: UIButton) {
    dismissViewControllerAnimated(true) {
      if self.bought {
        var lastDrink: Drink?
        let consumables = self.package!.drinks.reduce(0) { (count, drink) -> Int in
          if drink.consumed < drink.amount {
            lastDrink = drink
            return count + 1
          } else {
            return count
          }
        }
        if consumables > 1 {
          self.delegate?.goToBeerSelection(self.package!)
        } else if lastDrink != nil {
          self.delegate?.goToVerificationCode(self.package!, drink: lastDrink!)
        }
      } else {
        self.delegate?.goToPaymentForPackage(self.package!)
      }
    }
  }
  
  @IBAction func tapped(sender: UITapGestureRecognizer) {
    if sender.state == .Ended {
      dismissViewControllerAnimated(true, completion: nil)
    }
  }
  
  func configureWithPackage(aPackage: Package?) {
    if let package = aPackage {
      let packageType = package.type ?? .Chopp
      packageTypeImageView.image = UIImage(named: (packageType == .Chopp ? "img_detail_chopps" : "img_detail_beer"))
      let attr = NSAttributedString(string: package.originalPrice, attributes: [NSStrikethroughStyleAttributeName: 1])
      packageOriginalPriceLabel.attributedText = attr
      packagePriceLabel.text = package.salePrice
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "dd/MM/yy"
      packageExpireLabel.text = "Válido até " + (package.expirationDate != nil ? dateFormatter.stringFromDate(package.expirationDate!) : dateFormatter.stringFromDate(package.dateConsumption ?? NSDate(timeIntervalSince1970: 0)))
      if package.drinks.count > 0 {
        drink1AmountLabel.text = String(package.drinks.first!.amount - package.drinks.first!.consumed) + (package.drinks.first!.amount - package.drinks.first!.consumed > 1 ? " unidades" : " unidade")
        drink1NameLabel.text = package.drinks.first!.brand
        drink1VolumeLabel.text = String(package.drinks.first!.volume) + " ml"
        if package.drinks.count > 1 {
          drink2AmountLabel.text = String(package.drinks[1].amount - package.drinks[1].consumed) + (package.drinks[1].amount - package.drinks[1].consumed > 1 ? " unidades" : " unidade")
          drink2NameLabel.text = package.drinks[1].brand
          drink2VolumeLabel.text = String(package.drinks[1].volume) + " ml"
          if package.drinks.count > 2 {
            drink3AmountLabel.text = String(package.drinks[2].amount - package.drinks[2].consumed) + (package.drinks[2].amount - package.drinks[2].consumed > 1 ? " unidades" : " unidade")
            drink3NameLabel.text = package.drinks[2].brand
            drink3VolumeLabel.text = String(package.drinks[2].volume) + " ml"
          } else {
            packageDetailLineImageView3.hidden = true
          }
        } else {
          packageDetailLineImageView2.hidden = true
          packageDetailLineImageView3.hidden = true
        }
      }
    }
  }
}

extension PackageDetailViewController: UIGestureRecognizerDelegate {
  
  func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
    return gestureRecognizer == tap && touch.view == view
  }
}
