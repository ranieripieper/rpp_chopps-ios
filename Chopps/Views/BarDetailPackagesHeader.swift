//
//  BarDetailPackagesHeader.swift
//  Chopps
//
//  Created by Gilson Gil on 2/4/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BarDetailPackagesHeader: UITableViewHeaderFooterView {

  @IBOutlet weak var label: UILabel!
  
  func configureWithTitle(title: String) {
    label.text = title
  }
}
