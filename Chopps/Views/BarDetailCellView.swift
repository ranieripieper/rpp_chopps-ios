//
//  BarDetailCellView.swift
//  Chopps
//
//  Created by Gilson Gil on 10/23/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BarDetailCellView: UIView {
  
  var name: String?
  var distance: String?
  var nearBar = false
  var address: String?
  var phone: String?

  override func drawRect(rect: CGRect) {
    let bgRect = CGRect (origin: CGPoint (x: 30, y: 42), size: CGSize (width: bounds.width, height: bounds.height - 73))
    let bgColor = UIColor (red: 105.0/255, green: 158.0/255, blue: 163.0/255, alpha: 1.0)
    let context = UIGraphicsGetCurrentContext();
    CGContextMoveToPoint(context, bgRect.origin.x, bgRect.origin.y);
    CGContextAddLineToPoint(context, bgRect.width, bgRect.origin.y);
    CGContextAddLineToPoint(context, bgRect.width, bgRect.height);
    CGContextAddLineToPoint(context, bgRect.origin.x, bgRect.height);
    CGContextAddLineToPoint(context, bgRect.origin.x, bgRect.origin.y);
    CGContextSetFillColorWithColor(context, bgColor.CGColor);
    CGContextFillPath(context);
    
    let circleRect = CGRect (origin: CGPoint (x: 12, y: 24), size: CGSize (width: 88, height: 88))
    let yellowColor = UIColor (red: 255.0/255, green: 175.0/255, blue: 0.0/255, alpha: 1.0)
    CGContextSetLineWidth(context, 8.0);
    CGContextSetStrokeColorWithColor(context, yellowColor.CGColor);
    CGContextAddEllipseInRect(context, circleRect);
    CGContextStrokePath(context);
  }
}
