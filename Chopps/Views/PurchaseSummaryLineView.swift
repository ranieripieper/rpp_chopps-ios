//
//  PurchaseSummaryLineView.swift
//  Chopps
//
//  Created by Gilson Gil on 3/5/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

@IBDesignable
class PurchaseSummaryLineView: UIView {
  @IBInspectable var top: Bool = false
  
  override func drawRect(rect: CGRect) {
    let context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
    CGContextSetLineWidth(context, 1.0)
    if top {
      CGContextMoveToPoint(context, bounds.width * 0.6, 1.0)
      CGContextAddLineToPoint(context, 1.0, 1.0)
    } else {
      CGContextMoveToPoint(context, 1.0, 1.0)
    }
    CGContextAddLineToPoint(context, 1.0, bounds.height - 1.0)
    CGContextAddLineToPoint(context, bounds.width, bounds.height - 1.0)
    CGContextStrokePath(context)
  }
}
