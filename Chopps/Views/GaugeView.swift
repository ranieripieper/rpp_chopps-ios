//
//  GaugeView.swift
//  Chopps
//
//  Created by Gilson Gil on 9/26/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import QuartzCore

class GaugeView: UIView {
  var backgroundLayer: CAShapeLayer!
  var backgroundLayerColor: UIColor = UIColor (red: CGFloat(50.0/255), green: CGFloat(50.0/255), blue: CGFloat(50.0/255), alpha: CGFloat(1.0))
  
  var backGaugeLayer: CAShapeLayer!
  var backGaugeThickness: CGFloat = 20.0
  var backGaugeColor: UIColor = UIColor (red: CGFloat(152.0/255), green: CGFloat(206.0/255), blue: CGFloat(209.0/255), alpha: CGFloat(1.0))
  var backGaugeProgress: CGFloat = 1.0
  var backGaugeAnimated: Bool = false
  
  var frontGaugeLayer: CAShapeLayer!
  var frontGaugeThickness: CGFloat = 20.0
  var frontGaugeColor: UIColor = UIColor (red: CGFloat(107.0/255), green: CGFloat(158.0/255), blue: CGFloat(161.0/255), alpha: CGFloat(1.0))
  var frontGaugeProgress: CGFloat = 0.0
  var frontGaugeAnimated: Bool = false
  
  var progressTextLayer: CATextLayer!
  var descriptionTextLayer: CATextLayer!
  
  var progress: CGFloat = 0.0
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  func commonInit() {
    drawBackGauge()
    drawFrontGauge()
    drawProgressTextLayer()
    drawDescriptionTextLayer()
  }
  
  func setProgress(progress: CGFloat, animated: Bool) {
    self.progress = progress
    frontGaugeProgress = progress
    progressTextLayer.string = "\(Int(progress * 100))%"
    if animated {
      animate(frontGaugeLayer, fromValue: 0.0, toValue: progress)
    } else {
      updateGaugeWithProgress(frontGaugeLayer, progress: progress, animated: false)
    }
  }
  
  // MARK : Drawing Methods
  func drawBackGauge() {
    if backGaugeLayer != nil {
      return
    }
    backGaugeLayer = createGaugeWithStrokeEnd(backGaugeColor, width: backGaugeThickness, strokeEnd: backGaugeProgress)
    layer.addSublayer(backGaugeLayer)
  }
  
  func drawFrontGauge() {
    if frontGaugeLayer != nil {
      return
    }
    frontGaugeLayer = createGaugeWithStrokeEnd(frontGaugeColor, width: frontGaugeThickness, strokeEnd: frontGaugeProgress)
    layer.addSublayer(frontGaugeLayer)
  }
  
  func drawFrontGaugeAnimated(animated: Bool) {
    if frontGaugeLayer != nil {
      return
    }
    frontGaugeLayer = createGaugeWithStrokeEnd(frontGaugeColor, width: frontGaugeThickness, strokeEnd: 0)
    self.layer.addSublayer(frontGaugeLayer)
    if animated {
      animate(frontGaugeLayer, fromValue: frontGaugeLayer.strokeEnd, toValue: frontGaugeProgress)
    } else {
      frontGaugeLayer.strokeEnd = frontGaugeProgress
    }
  }
  
  func drawDescriptionTextLayer() {
    if descriptionTextLayer != nil {
      return
    }
    descriptionTextLayer = createTextLayerWithText("Consumidos", fontSize: 10, position: CGPoint (x: 0, y: 90), size: CGSize (width: 150, height: 20))
    layer.addSublayer(descriptionTextLayer)
  }
  
  func drawProgressTextLayer() {
    if progressTextLayer != nil {
      return
    }
    progressTextLayer = createTextLayerWithText("\(Int(frontGaugeProgress * 100))%", fontSize: 38, position: CGPoint (x: 0, y: 50), size: CGSize (width: 150, height: 150))
    layer.addSublayer(progressTextLayer)
  }
  
  func animate (layer: CAShapeLayer, fromValue: CGFloat, toValue: CGFloat) {
    let animation = createGaugeAnimationWithFromValue(fromValue, toValue: toValue)
    layer.addAnimation(animation, forKey: "strokeEnd")
  }
  
  func updateGaugeWithProgress(gauge: CAShapeLayer, progress: CGFloat, animated: Bool) {
    if animated {
      let animation = createGaugeAnimationWithFromValue(gauge.strokeEnd, toValue: progress)
      gauge.addAnimation(animation, forKey: "strokeEnd")
    } else {
      gauge.strokeEnd = progress
    }
  }
  
  // MARK : Helper Methods
  func createGaugeWithStrokeEnd(color: UIColor, width: CGFloat, strokeEnd: CGFloat) -> CAShapeLayer {
    let gaugeRadius = bounds.width / 2 * 0.8
    let gaugePath = UIBezierPath (arcCenter: CGPoint (x: bounds.width / 2, y: bounds.height / 2), radius: gaugeRadius, startAngle: CGFloat (-0.5 * M_PI), endAngle: CGFloat (1.5 * M_PI), clockwise: true)
    let gaugeLayer = CAShapeLayer()
    gaugeLayer.path = gaugePath.CGPath
    gaugeLayer.strokeColor = color.CGColor
    gaugeLayer.fillColor = UIColor.clearColor().CGColor
    gaugeLayer.lineWidth = width
    gaugeLayer.strokeStart = 0
    gaugeLayer.strokeEnd = strokeEnd
    
    return gaugeLayer
  }
  
  func createGaugeAnimationWithFromValue(fromValue: CGFloat, toValue: CGFloat) -> CABasicAnimation {
    let strokeEnd = CABasicAnimation (keyPath: "strokeEnd")
    strokeEnd.fromValue = fromValue
    strokeEnd.toValue = toValue
    strokeEnd.duration = 1
    strokeEnd.timingFunction = CAMediaTimingFunction (controlPoints: 0.25, -0.4, 0.5, 1)
    strokeEnd.removedOnCompletion = false
    strokeEnd.fillMode = kCAFillModeForwards
    
    return strokeEnd
  }
  
  func createTextLayerWithText(text: String, fontSize: CGFloat, position: CGPoint, size: CGSize) -> CATextLayer {
    let textLayer = CATextLayer()
    textLayer.string = text
    textLayer.wrapped = false
    let font = UIFont (name: "Gill Sans", size: fontSize)
    textLayer.font = font
    textLayer.fontSize = fontSize
    textLayer.foregroundColor = UIColor.whiteColor().CGColor
    textLayer.alignmentMode = kCAAlignmentCenter
    textLayer.contentsScale = UIScreen.mainScreen().scale;
    textLayer.frame = CGRect (origin: position, size: size)
    
    return textLayer
  }
}
