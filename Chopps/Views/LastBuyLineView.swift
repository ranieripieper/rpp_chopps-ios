//
//  LastBuyLineView.swift
//  Chopps
//
//  Created by Gilson Gil on 3/5/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LastBuyLineView: UIView {
  override func drawRect(rect: CGRect) {
    let context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
    CGContextSetLineWidth(context, 1.0)
    CGContextMoveToPoint(context, 225.0, 0.0)
    CGContextAddLineToPoint(context, 225.0, 8.0)
    CGContextAddLineToPoint(context, 0.0, 8.0)
    CGContextAddLineToPoint(context, 0.0, 23.0)
    CGContextAddLineToPoint(context, 14.0, 23.0)
    CGContextStrokePath(context)
  }
}
