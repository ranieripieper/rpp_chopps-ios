//
//  GraphView.swift
//  Chopps
//
//  Created by Gilson Gil on 10/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

struct GraphInfo {
  var name: String
  var progress: CGFloat
}

class GraphView: UIView {
  @IBOutlet weak var topGraphImageView: UIImageView!
  @IBOutlet weak var bottomGraphImageView: UIImageView!
  @IBOutlet weak var topGraphImageViewWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var bottomGraphImageViewWidthConstraint: NSLayoutConstraint!
  
  let maxGraphWidth: CGFloat = 188.0
  
  override func awakeFromNib() {
    layoutIfNeeded()
  }
  
  func configureWithTopGraphInfo (top: GraphInfo, bottom: GraphInfo) {
    topGraphImageViewWidthConstraint.constant = max(top.progress * maxGraphWidth, 1)
    bottomGraphImageViewWidthConstraint.constant = bottom.progress * maxGraphWidth
    UIView.animateWithDuration(1.0, delay: 0.5, options: .BeginFromCurrentState, animations: { () -> Void in
      self.layoutIfNeeded()
    }, completion: nil)
  }
}