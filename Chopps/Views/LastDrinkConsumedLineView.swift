//
//  LastDrinkConsumedLineView.swift
//  Chopps
//
//  Created by Gilson Gil on 3/3/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class LastDrinkConsumedLineView: UIView {
  override func drawRect(rect: CGRect) {
    let context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
    CGContextSetLineWidth(context, 1.0)
    CGContextMoveToPoint(context, 0.0, 0.0)
    CGContextAddLineToPoint(context, 55.0, 0.0)
    CGContextAddLineToPoint(context, 55.0, 40.0)
    CGContextAddLineToPoint(context, 63.0, 40.0)
    CGContextStrokePath(context)
  }
}
