//
//  BarCellView.swift
//  Chopps
//
//  Created by Gilson Gil on 10/24/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BarCellView: UIView {
  var name: String?
  var distance: String?
  var package1Name: String?
  var package1Price: String?
  var package2Name: String?
  var package2Price: String?
  var shouldShowPackages = false
  var shouldShowDistance = true
  
  override func drawRect(rect: CGRect) {
    let bgRect = CGRect (origin: CGPoint (x: 80, y: 15), size: CGSize (width: bounds.width, height: 55))
    let bgColor = UIColor (red: 105.0/255, green: 158.0/255, blue: 163.0/255, alpha: 1.0)
    let context = UIGraphicsGetCurrentContext()
    CGContextMoveToPoint(context, bgRect.origin.x, bgRect.origin.y);
    CGContextAddLineToPoint(context, bgRect.width, bgRect.origin.y);
    CGContextAddLineToPoint(context, bgRect.width, bgRect.height);
    CGContextAddLineToPoint(context, bgRect.origin.x, bgRect.height);
    CGContextAddLineToPoint(context, bgRect.origin.x, bgRect.origin.y);
    CGContextSetFillColorWithColor(context, bgColor.CGColor);
    CGContextFillPath(context);
    
    let circleRect = CGRect (origin: CGPoint (x: 18, y: 10), size: CGSize (width: 50, height: 50))
    let yellowColor = UIColor (red: 255.0/255, green: 175.0/255, blue: 0.0/255, alpha: 1.0)
    CGContextSetLineWidth(context, 4.0);
    CGContextSetStrokeColorWithColor(context, yellowColor.CGColor);
    CGContextAddEllipseInRect(context, circleRect);
    CGContextStrokePath(context);
    
    if name != nil {
      if shouldShowDistance {
        (name! as NSString).drawInRect(CGRect (x: 86, y: 17, width: bounds.width - 86 - 20, height: 20), withAttributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont (name: "GillSans-Bold", size: 16)!])
      } else {
        (name! as NSString).drawInRect(CGRect (x: 86, y: 23, width: bounds.width - 86 - 20, height: 30), withAttributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont (name: "GillSans-Bold", size: 20)!])
      }
    }
    if distance != nil {
      (distance! as NSString).drawInRect(CGRect (x: 86, y: 37, width: bounds.width - 86 - 20, height: 14), withAttributes: [NSForegroundColorAttributeName: UIColor (red: 255.0/255, green: 175.0/255, blue: 0.0/255, alpha: 1.0), NSFontAttributeName: UIFont (name: "GillSans-Bold", size: 12)!])
    }
    if shouldShowPackages {
      if package1Name != nil && countElements(package1Name!) > 0 {
        ("Chopp \(package1Name!)" as NSString).drawInRect(CGRect (x: 96, y: 62, width: 88, height: 10), withAttributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont (name: "GillSans", size: 8)!])
      }
      if package1Price != nil {
        (package1Price! as NSString).drawInRect(CGRect (x: 96, y: 70, width: 88, height: 12), withAttributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont (name: "GillSans-Bold", size: 13)!])
      }
      if package2Name != nil && countElements(package2Name!) > 0 {
        ("Chopp \(package2Name!)" as NSString).drawInRect(CGRect (x: 214, y: 62, width: 88, height: 10), withAttributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont (name: "GillSans", size: 8)!])
      }
      if package2Price != nil {
        (package2Price! as NSString).drawInRect(CGRect (x: 214, y: 70, width: 88, height: 12), withAttributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont (name: "GillSans-Bold", size: 13)!])
      }
    }
  }
}
