//
//  SliderView.swift
//  Chopps
//
//  Created by Gilson Gil on 2/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SliderView: UIView {
  
  @IBOutlet weak var slidingView: UIView!
  @IBOutlet weak var priceLabel: UILabel!
  
  var currentValue: Float = 30
  
  override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
    let touch = touches.anyObject() as UITouch
    let x = touch.locationInView(self).x
    let minimum: CGFloat = 36
    let maximum: CGFloat = bounds.width - 64
    slidingView.frame = CGRect(origin: CGPoint(x: max(minimum, min(maximum, x)), y: 0), size: slidingView.bounds.size)
    if slidingView.frame.origin.x + slidingView.frame.width > bounds.width {
      priceLabel.frame = CGRect(origin: CGPoint(x: bounds.width - slidingView.frame.origin.x - priceLabel.bounds.width, y: 0), size: priceLabel.bounds.size)
    } else {
      priceLabel.frame = CGRect(origin: CGPoint(x: 48, y: 0), size: priceLabel.bounds.size)
    }
    priceLabel.text = currentPrice()
  }
  
//  override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
//    let price = currentPrice()
//  }
  
  func currentPrice() -> String {
    let minimum: CGFloat = 36
    let maximum: CGFloat = bounds.width - 64
    let ratio: Double = (Double(slidingView.frame.origin.x) - Double(minimum)) / (Double(maximum) - Double(minimum))
    let price: Double = ratio * (199 - 30) + 30
    let unformattedPrice = String(format: "%.2f", price)
    currentValue = (unformattedPrice as NSString).floatValue
    return String(format: "R$%.2f", price)
  }
}
