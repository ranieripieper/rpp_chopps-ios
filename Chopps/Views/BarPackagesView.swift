//
//  BarPackagesView.swift
//  Chopps
//
//  Created by Gilson Gil on 3/2/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class BarPackagesView: UIView {
  var package: Package?
  
  override func drawRect(rect: CGRect) {
    let leftLineHeight: CGFloat = /*CGFloat(package!.drinks.count)*/3.0 * 33.0 + 38.0
    let context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, UIColor.whiteColor().CGColor)
    CGContextSetLineWidth(context, 1.0)
    CGContextMoveToPoint(context, 23.0, 32.0)
    CGContextAddLineToPoint(context, 8.0, 32.0)
    CGContextAddLineToPoint(context, 8.0, 32.0 + leftLineHeight)
    CGContextAddLineToPoint(context, 38.0, 32.0 + leftLineHeight)
    CGContextStrokePath(context)
    
    UIImage(named: "img_detail_bar_line2")!.drawInRect(CGRect(x: bounds.width - 63 - 16, y: 31, width: 63, height: 42))
    UIImage(named: "img_invite_arrow")!.drawInRect(CGRect(x: 80, y: 28, width: 18, height: 7))
    UIImage(named: "img_invite_arrow")!.drawInRect(CGRect(x: 50, y: 165, width: 18, height: 7))
    
    UIImage(named: (package!.type == .Chopp ? "icn_detail_bar_chopp" : "icn_detail_bar_cerveja"))!.drawInRect(CGRect(x: 36, y: 0, width: 36, height: 58))
    
    let paragraph = NSMutableParagraphStyle()
    paragraph.alignment = .Right
    
    let typeString: NSString = "Pacote de " + (package!.type == .Chopp ? "Chopp" : "Cerveja") + " com"
    typeString.drawInRect(CGRect(x: 105, y: 24, width: bounds.width - 105 - 82, height: 14), withAttributes: [NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 12)!, NSForegroundColorAttributeName: UIColor.whiteColor()])
    
    if package!.drinks.count > 0 {
      let string: NSString = String(package!.drinks[0].amount - package!.drinks[0].consumed) + " " + package!.drinks[0].brand
      let attr = NSMutableAttributedString(string: string, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "GillSans", size: 16)!, NSParagraphStyleAttributeName: paragraph])
      attr.setAttributes([NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 20)!, NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: paragraph], range: string.rangeOfString(String(package!.drinks[0].amount - package!.drinks[0].consumed)))
      attr.drawInRect(CGRect(x: 36, y: 58, width: bounds.width - 36 - 50, height: 21))
    }
    if package!.drinks.count > 1 {
      let string: NSString = String(package!.drinks[1].amount - package!.drinks[1].consumed) + " " + package!.drinks[1].brand
      let attr = NSMutableAttributedString(string: string, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "GillSans", size: 16)!, NSParagraphStyleAttributeName: paragraph])
      attr.setAttributes([NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 20)!, NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: paragraph], range: string.rangeOfString(String(package!.drinks[1].amount - package!.drinks[1].consumed)))
      attr.drawInRect(CGRect(x: 36, y: 91, width: bounds.width - 36 - 50, height: 21))
    }
    if package!.drinks.count > 2 {
      let string: NSString = String(package!.drinks[2].amount - package!.drinks[2].consumed) + " " + package!.drinks[2].brand
      let attr = NSMutableAttributedString(string: string, attributes: [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "GillSans", size: 16)!, NSParagraphStyleAttributeName: paragraph])
      attr.setAttributes([NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 20)!, NSForegroundColorAttributeName: UIColor.whiteColor(), NSParagraphStyleAttributeName: paragraph], range: string.rangeOfString(String(package!.drinks[2].amount - package!.drinks[2].consumed)))
      attr.drawInRect(CGRect(x: 36, y: 124, width: bounds.width - 36 - 50, height: 21))
    }
  }
}
