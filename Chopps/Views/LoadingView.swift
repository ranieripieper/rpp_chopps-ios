//
//  LoadingView.swift
//  Chopps
//
//  Created by Gilson Gil on 11/13/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class LoadingView: UIView {
  
  var progress1: CGFloat = 0.0
  var progress2: CGFloat = 0.0
  var progressView1: UIView?
  var progressView2: UIView?
  let screenWidth = UIScreen.mainScreen().bounds.width
  var shouldPresentProgressView2 = false
  var timer: NSTimer?
  var isAnimating = false
  
  required init(superview: UIView) {
    if superview is UINavigationBar {
      super.init(frame: CGRect(origin: CGPoint(x: 0, y: superview.bounds.height), size: CGSize(width: superview.bounds.width, height: 4)))
    } else {
      super.init(frame: CGRect(origin: CGPointZero, size: CGSize(width: superview.bounds.width, height: 4)))
    }
    backgroundColor = UIColor(red: 255.0/255, green: 205.0/255, blue: 0.0/255, alpha: 1.0)
    clipsToBounds = true
    superview.addSubview(self)
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    backgroundColor = UIColor(red: 255.0/255, green: 205.0/255, blue: 0.0/255, alpha: 1.0)
    clipsToBounds = true
  }
  
  func start() {
    if isAnimating {
      return
    }
    isAnimating = true
    hidden = false
    frame = CGRect(origin: frame.origin, size: CGSize(width: frame.width, height: 0))
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.width, height: 4))
    }) { (finished) -> Void in
      self.timer = NSTimer(timeInterval: 1/60, target: self, selector: "update", userInfo: nil, repeats: true)
      NSRunLoop.mainRunLoop().addTimer(self.timer!, forMode: NSRunLoopCommonModes)
    }
  }
  
  func stop() {
    if !isAnimating {
      return
    }
    isAnimating = false
    timer?.invalidate()
    timer = nil
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.frame = CGRect(origin: self.frame.origin, size: CGSize(width: self.frame.width, height: 0))
    }) { (finished) -> Void in
      self.hidden = true
      self.progressView1?.removeFromSuperview()
      self.progressView1 = nil
      self.progressView2?.removeFromSuperview()
      self.progressView2 = nil
      self.shouldPresentProgressView2 = false
    }
  }
  
  func update() {
    if progressView1 == nil {
      addProgressView1()
    }
    progressView1!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, (progress1 * 2 * 4 / 3 - 1) * screenWidth * 3 / 4, 0)
    progress1 = progress1 + 1/60
    if progress1 >= 1.0 {
      progress1 = 0.0
    }
    if progress1 <= 0.5 {
      progress2 = progress1 + 0.5
    } else {
      progress2 = progress1 - 0.5
    }
    if !shouldPresentProgressView2 && progress1 >= 0.5 {
      shouldPresentProgressView2 = true
    }
    if shouldPresentProgressView2 {
      if progressView2 == nil {
        addProgressView2()
      }
    }
    progressView2?.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, (progress2 * 2 * 4 / 3 - 1) * screenWidth * 3 / 4, 0)
  }
  
  func addProgressView1() {
    progressView1 = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: screenWidth * 3 / 4, height: bounds.height)))
    progressView1!.backgroundColor = UIColor(red: 255.0/255, green: 175.0/255, blue: 0.0/255, alpha: 1.0)
    addSubview(progressView1!)
    progressView1!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -screenWidth * 3 / 4, 0)
  }
  
  func addProgressView2() {
    progressView2 = UIView(frame: CGRect(origin: CGPointZero, size: CGSize(width: screenWidth * 3 / 4, height: bounds.height)))
    progressView2!.backgroundColor = UIColor(red: 255.0/255, green: 175.0/255, blue: 0.0/255, alpha: 1.0)
    addSubview(progressView2!)
    progressView2!.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -screenWidth * 3 / 4, 0)
  }
}
