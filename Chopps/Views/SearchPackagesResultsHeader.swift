//
//  SearchPackagesResultsHeader.swift
//  Chopps
//
//  Created by Gilson Gil on 2/26/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchPackagesResultsHeader: UITableViewHeaderFooterView {
  @IBOutlet weak var barImageView: UIImageView!
  @IBOutlet weak var barCellView: BarCellView!
  
  func configureWithBar(bar: Bar, barDistance: String) {
    barImageView.setImageWithURL(NSURL (string: bar.imageURL))
    barCellView.name = bar.name.uppercaseString
    barCellView.distance = barDistance
    barCellView.shouldShowPackages = false
  }
}
