//
//  SearchView.swift
//  Chopps
//
//  Created by Gilson Gil on 10/16/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class SearchView: UIView {

  @IBOutlet weak var textField: UITextField!
  @IBOutlet weak var myLocationButton: UIButton!
  @IBOutlet weak var tapGesture: UITapGestureRecognizer!
  @IBOutlet weak var centerMapButtonRightSpaceConstraint: NSLayoutConstraint!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    textField.setPaddings(30)
  }
  
  func addTapGesture () {
    addGestureRecognizer(tapGesture)
  }
  
  func removeTapGesture () {
    removeGestureRecognizer(tapGesture)
  }
  
  func expandTextField() {
    centerMapButtonRightSpaceConstraint.constant = -20
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.layoutIfNeeded()
    })
  }
  
  func contractTextField() {
    centerMapButtonRightSpaceConstraint.constant = 42
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.layoutIfNeeded()
    })
  }
}

extension SearchView: UITextFieldDelegate {
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    println(textField.text)
    return true
  }
}