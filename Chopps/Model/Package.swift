//
//  Package.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

class Package: NSObject {
  let packageId: Int
  var userPackageId: Int?
  let type: PackageType
  let originalPrice: String
  let salePrice: String
  var expirationDate: NSDate?
//  var active: Bool
  let dateConsumption: NSDate?
  let lastDateConsumed: NSDate?
  var drinks: [Drink]
  var bar: Bar?
  var state: PackageState = .Available
  
  enum PackageState {
    case Owned
    case Available
  }
  
  enum PackageType {
    case Chopp, Beer
    init(rawValue: Int) {
      switch rawValue {
      case 1:
        self = .Chopp
      case 2:
        self = .Beer
      default:
        self = .Chopp
      }
    }
    func rawValue() -> Int {
      switch self {
      case .Chopp:
        return 1
      case .Beer:
        return 2
      default:
        return 1
      }
    }
  }

  init(dictionary: DictionaryType) {
    packageId = (dictionary as NSDictionary).valueForKeyPath("id") as? Int ?? 0
    type = PackageType(rawValue: (dictionary as NSDictionary).valueForKeyPath("package_type") as? Int ?? 1)
    originalPrice = String(format: "R$%.2f", (dictionary as NSDictionary).valueForKeyPath("real_value") as? Float ?? 0)
    salePrice = String(format: "R$%.2f", (dictionary as NSDictionary).valueForKeyPath("sale_value") as? Float ?? 0)
    drinks = []
    let drinkArray = (dictionary as NSDictionary).valueForKeyPath("package_items") as? [DictionaryType] ?? (dictionary as NSDictionary).valueForKeyPath("package_user_items") as? [DictionaryType] ?? []
    for drinkDictionary in drinkArray {
      drinks.append(Drink(dictionary: drinkDictionary))
    }
    if let barDict = (dictionary as NSDictionary).valueForKeyPath("bar") as? DictionaryType {
      bar = Bar(dictionary: barDict)
    } else if let barDict = (dictionary as NSDictionary).valueForKeyPath("package.bar") as? DictionaryType {
      bar = Bar(dictionary: barDict)
    }
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd"
    expirationDate = dateFormatter.dateFromString(dictionary["dt_expire"] as? String ?? "")
    dateConsumption = dateFormatter.dateFromString(dictionary["dt_consumption"] as? String ?? "")
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
    lastDateConsumed = dateFormatter.dateFromString(dictionary["dt_last_cons"] as? String ?? "")
  }
  
  required init(coder aDecoder: NSCoder) {
    packageId = aDecoder.decodeObjectForKey("packageId") as Int
    type = PackageType(rawValue: aDecoder.decodeObjectForKey("type") as Int)
    originalPrice = aDecoder.decodeObjectForKey("originalPrice") as NSString
    salePrice = aDecoder.decodeObjectForKey("salePrice") as NSString
    expirationDate = aDecoder.decodeObjectForKey("expirationDate") as? NSDate
    drinks = aDecoder.decodeObjectForKey("drinks") as [Drink]
    dateConsumption = aDecoder.decodeObjectForKey("dateConsumption") as? NSDate
    lastDateConsumed = aDecoder.decodeObjectForKey("lastDateConsumed") as? NSDate
    bar = aDecoder.decodeObjectForKey("bar") as Bar?
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(packageId, forKey: "packageId")
    aCoder.encodeObject(type.rawValue(), forKey: "type")
    aCoder.encodeObject(originalPrice, forKey: "originalPrice")
    aCoder.encodeObject(salePrice, forKey: "salePrice")
    aCoder.encodeObject(expirationDate, forKey: "expirationDate")
    aCoder.encodeObject(drinks, forKey: "drinks")
    aCoder.encodeObject(dateConsumption, forKey: "dateConsumption")
    aCoder.encodeObject(lastDateConsumed, forKey: "lastDateConsumed")
    if bar != nil {
      aCoder.encodeObject(bar!, forKey: "bar")
    }
  }
}

// MARK: API
extension Package {
  class func searchWithValue(value: Float, type: Int, page: Int, completionHandler: ([Bar]) -> ()) {
    Networking.sharedInstance.findPackagesWithValue(value, type: type, page: page) { (response) -> () in
      if let error = response as? NSError {
        println("error: \(error)")
      } else if let responseObject = response as? DictionaryType {
        if let packagesArray = responseObject["packages"] as? [DictionaryType] {
          var packages = [Package]()
          var bars = [Bar]()
          for packageDict in packagesArray {
            let package = Package(dictionary: packageDict)
            if package.bar != nil {
              let existingBars = bars.filter({ (bar) -> Bool in
                bar.barId == package.bar?.barId
              })
              if existingBars.count == 0 {
                bars.append(package.bar!)
              }
              package.bar!.packages.append(package)
            }
            packages.append(package)
          }
          completionHandler(bars)
        }
      }
    }
  }
  
  func buy(paymentMethod: PaymentMethod, completionHandler: (Package?) -> ()) {
    Networking.sharedInstance.buyPackage(self, paymentMethod: paymentMethod, completionHandler: { (response) -> () in
      if let error = response as? NSError {
        completionHandler(nil)
      } else if let responseObject = response as? DictionaryType {
        if let packageDict = responseObject["package"] as? DictionaryType {
          let package = Package(dictionary: packageDict)
          completionHandler(package)
        }
      }
    })
  }
}
