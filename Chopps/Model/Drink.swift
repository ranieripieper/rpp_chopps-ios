//
//  Drink.swift
//  Chopps
//
//  Created by Gilson Gil on 1/30/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class Drink: NSObject {
  let id: Int
  let brand: String
  let type: String
  let volume: Int
  let amount: Int
  var consumed: Int
  
  init(dictionary: DictionaryType) {
    id = (dictionary as NSDictionary).valueForKeyPath("id") as? Int ?? 0
    brand = (dictionary as NSDictionary).valueForKeyPath("package_item.brand") as? String ?? (dictionary as NSDictionary).valueForKeyPath("brand") as? String ?? ""
    type = (dictionary as NSDictionary).valueForKeyPath("package_item.type_style") as? String ?? (dictionary as NSDictionary).valueForKeyPath("type_style") as? String ?? ""
    volume = (dictionary as NSDictionary).valueForKeyPath("package_item.volume") as? Int ?? (dictionary as NSDictionary).valueForKeyPath("volume") as? Int ?? 0
    amount = (dictionary as NSDictionary).valueForKeyPath("package_item.qtde") as? Int ?? (dictionary as NSDictionary).valueForKeyPath("qtde") as? Int ?? 0
    consumed = (dictionary as NSDictionary).valueForKeyPath("qtde_cons") as? Int ?? 0
  }
  
  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeObjectForKey("id") as Int
    brand = aDecoder.decodeObjectForKey("brand") as String
    type = aDecoder.decodeObjectForKey("type") as String
    volume = aDecoder.decodeObjectForKey("volume") as Int
    amount = aDecoder.decodeObjectForKey("amount") as Int
    consumed = aDecoder.decodeObjectForKey("consumed") as Int
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(id, forKey: "id")
    aCoder.encodeObject(brand, forKey: "brand")
    aCoder.encodeObject(type, forKey: "type")
    aCoder.encodeObject(volume, forKey: "volume")
    aCoder.encodeObject(amount, forKey: "amount")
    aCoder.encodeObject(consumed, forKey: "consumed")
  }
}

// MARK: API
extension Drink {
  func drinkWithCode(code: String, userPackageId: Int, completionHandler: (Bool) -> ()) {
    Networking.sharedInstance.drink(userPackageId, userPackageItemId: id, code: code) { (response) -> () in
      if let error = response as? NSError {
        println("error: \(error)")
      } else if let responseObject = response as? DictionaryType {
        println(responseObject)
        if let success = responseObject["success"] as? Bool {
          completionHandler(success)
        } else {
          completionHandler(false)
        }
      }
    }
  }
}
