//
//  Person.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

class Person: NSObject {
  var userId: Int
  var name: String
  var email: String
  var pictureURL: String
  var lastPackageConsumed: (barName: String, brand: String, date: String, type: Int, style: String)?
  var packages: [Package]
  var history: [Int: Int]?

  init(userId: Int, name: String, email: String, phone: String, pictureURL: String, packages: [Package]) {
    self.userId = userId
    self.name = name
    self.email = email
    self.pictureURL = pictureURL
    self.packages = packages
    super.init()
  }
  
  required init(coder aDecoder: NSCoder) {
    userId = aDecoder.decodeObjectForKey("userId") as Int
    name = aDecoder.decodeObjectForKey("name") as NSString
    email = aDecoder.decodeObjectForKey("email") as NSString
    pictureURL = aDecoder.decodeObjectForKey("pictureURL") as NSString
    if let barName = aDecoder.decodeObjectForKey("barName") as? String {
      if let brand = aDecoder.decodeObjectForKey("brand") as? String {
        if let date = aDecoder.decodeObjectForKey("date") as? String {
          if let type = aDecoder.decodeObjectForKey("type") as? Int {
            if let style = aDecoder.decodeObjectForKey("style") as? String {
              lastPackageConsumed = (barName, brand, date, type, style)
            }
          }
        }
      }
    }
    packages = aDecoder.decodeObjectForKey("packages") as [Package]
    history = aDecoder.decodeObjectForKey("history") as? [Int: Int]
  }
  
  convenience init(email: String) {
    self.init(userId: 0, name: "", email: email, phone: "", pictureURL: "", packages: [])
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(userId, forKey: "userId")
    aCoder.encodeObject(name, forKey: "name")
    aCoder.encodeObject(email, forKey: "email")
    aCoder.encodeObject(pictureURL, forKey: "pictureURL")
    aCoder.encodeObject(packages as NSArray, forKey: "packages")
    aCoder.encodeObject(lastPackageConsumed?.barName, forKey: "barName")
    aCoder.encodeObject(lastPackageConsumed?.brand, forKey: "brand")
    aCoder.encodeObject(lastPackageConsumed?.date, forKey: "date")
    aCoder.encodeObject(lastPackageConsumed?.type, forKey: "type")
    aCoder.encodeObject(lastPackageConsumed?.style, forKey: "style")
    aCoder.encodeObject(history, forKey: "history")
  }
}

// MARK: Persistence
extension Person {
  class func persistedUser() -> Person? {
    if let userData = NSUserDefaults.standardUserDefaults().objectForKey("LOGGEDUSER") as? NSData {
      return NSKeyedUnarchiver.unarchiveObjectWithData(userData) as Person!
    } else {
      return nil
    }
  }
  
  func persistUser() {
    NSUserDefaults.standardUserDefaults().setObject(NSKeyedArchiver.archivedDataWithRootObject(self), forKey: "LOGGEDUSER")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  func dePersistUser() {
    NSUserDefaults.standardUserDefaults().removeObjectForKey("LOGGEDUSER")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
}

// MARK: API
extension Person {
  class func registerUserWithName(name: String, email: String, birthDate: String, password: String, facebookToken: String?, facebookID: String?, location: CLLocation?, pictureData: UIImage?, completionHandler: (success: Bool, closestBar: Bar?) -> ()) {
    Networking.sharedInstance.registerUserWithName(name, email: email, password: password, birthDate: birthDate, facebookToken: facebookToken, facebookID: facebookID, location: location, pictureData: imageDataWithImage(pictureData)) { (responseObject: AnyObject?) -> () in
      if let error = responseObject as? NSError {
        println(error)
        completionHandler(success: false, closestBar: nil)
      } else if let response = responseObject as? DictionaryType {
        if let response = responseObject as? DictionaryType {
          if response["success"] != nil && response["success"] as Int == 1 {
            KeychainWrapper.setString(password, forKey: "password")
            let person = Person(email: email)
            let closestBar = person.handleRegisterLoginUserInfoResponse(response)
            completionHandler(success: true, closestBar: closestBar)
          }
        }
      }
    }
  }
  
  class func loginUserWithEmail(email: String, password: String, location: CLLocation?, completionHandler: (success: Bool, closestBar: Bar?) -> ()) {
    Networking.sharedInstance.loginWithEmail(email, password: password, location: location) { (responseObject: AnyObject?) -> () in
      if let error = responseObject as? NSError {
        completionHandler(success: false, closestBar: nil)
      } else {
        if let response = responseObject as? DictionaryType {
          if response["success"] != nil && response["success"] as Int == 1 {
            KeychainWrapper.setString(password, forKey: "password")
            let person = Person(email: email)
            let closestBar = person.handleRegisterLoginUserInfoResponse(response)
            completionHandler(success: true, closestBar: closestBar)
          }
        }
      }
    }
  }
  
  func handleRegisterLoginUserInfoResponse(response: DictionaryType) -> (Bar?) {
    userId = (response as NSDictionary).valueForKeyPath("user.id") as Int
    name = (response as NSDictionary).valueForKeyPath("user.name") as String
    email = (response as NSDictionary).valueForKeyPath("user.email") as String
    pictureURL = (response as NSDictionary).valueForKeyPath("user.photo_url") as String
    Bar.setNumberOfBars(response["nr_bars"] as Int)
    var bar: Bar?
    if let barDict = response["bar"] as? DictionaryType {
      bar = Bar(dictionary: barDict)
    }
    if let packagesArray = (response as NSDictionary).valueForKeyPath("last_packages_cons") as? [DictionaryType] {
      packages.removeAll(keepCapacity: false)
      for packageDict in packagesArray {
        let package = Package(dictionary: packageDict)
        packages.append(package)
      }
    }
    if let historyArray = (response as NSDictionary).valueForKeyPath("history") as? [DictionaryType] {
      history?.removeAll(keepCapacity: false)
      history = [Int: Int]()
      for historyDict in historyArray {
        history![historyDict["package_type"] as Int] = historyDict["qtde"] as? Int ?? 0
      }
    }
    if let lastPackage = (response as NSDictionary).valueForKeyPath("last_package") as? DictionaryType {
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss ZZZ"
      var dateString = ""
      if let date = dateFormatter.dateFromString(lastPackage["dt_last_cons"] as? String ?? "") {
        dateFormatter.dateFormat = "dd.MM.yyyy 'às' hh:mm"
        dateString = dateFormatter.stringFromDate(date)
      }
      let lastPackageTuple = (lastPackage["bar_name"] as? String ?? "", lastPackage["brand"] as? String ?? "", dateString, lastPackage["package_type"] as? Int ?? 1, lastPackage["type_style"] as? String ?? "")
      lastPackageConsumed = lastPackageTuple
    }
    persistUser()
    KeychainWrapper.setString((response as NSDictionary).valueForKeyPath("user.auth_token") as String, forKey: "authToken")
    KeychainWrapper.setString(String(userId), forKey: "userId")
    KeychainWrapper.setString(email, forKey: "email")
    return bar
  }
  
  func handleHistoryResponse(response: NSDictionary) -> (DictionaryType, Package?, (Package, PaymentMethod, String)?) {
    if let historyArray = response.valueForKeyPath("drink_summary") as? [DictionaryType] {
      history?.removeAll(keepCapacity: false)
      history = [Int: Int]()
      for historyDict in historyArray {
        history![historyDict["package_type"] as Int] = historyDict["qtde"] as? Int ?? 0
      }
    }
    var package: Package? = nil
    if let packagesArray = response.valueForKeyPath("last_packages_cons") as? [DictionaryType] {
      if let packageDict = packagesArray.first {
        package = Package(dictionary: packageDict)
      }
    }
    if let lastBuyDict = response["last_buy"] as? DictionaryType {
      
    }
    var lastBuy: (Package, PaymentMethod, String)? {
      if let lastBuyDict = response["last_buy"] as? DictionaryType {
        if let packageDict = lastBuyDict["package_user"] as? DictionaryType {
          let lastBuyPackage = Package(dictionary: packageDict)
          if let paymentDict = lastBuyDict["payment_info"] as? DictionaryType {
            let paymentMethod = PaymentMethod(flag: paymentDict["bandeira"] as String, endNumber: paymentDict["last_card_nr"] as String)
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
            let date = dateFormatter.dateFromString(paymentDict["created_at"] as String)
            dateFormatter.dateFormat = "dd.MM.yy"
            let dateString = dateFormatter.stringFromDate(date!)
            return (lastBuyPackage, paymentMethod, dateString)
          }
        }
      }
      return nil
    }
    return (response.valueForKeyPath("package_summary") as DictionaryType, package, lastBuy)
  }
  
  class func logout() {
    let person = persistedUser()
    person?.dePersistUser()
    Networking.sharedInstance.uninstallHeaderToken()
  }
  
//  class func forgotPassword(email: String, completionHandler: (Bool) -> ()) {
//    Networking.sharedInstance.forgotPassword(email, completionHandler: { (response) -> () in
//      if let error = response as? NSError {
//        completionHandler(false)
//      } else {
//        if let responseObject = response as? DictionaryType {
//          if let success = responseObject["success"] as? Bool {
//            completionHandler(success)
//          } else {
//            completionHandler(false)
//          }
//        }
//      }
//    })
//  }
//  
//  func updateProfileWithParams(params: DictionaryType) {
//    let name = params["name"] as String?
//    let email = params["email"] as String?
//    let phone = params["phone"] as String?
//    let password = params["password"] as String?
//    let avatar = params["avatar"] as UIImage?
//  }
  
  func getUserInfo(location: CLLocation?, completionHandler: (closestBar: Bar?) -> ()) {
    Networking.sharedInstance.getUserInfo(location, completionHandler: { (responseObject) -> () in
      if let error = responseObject as? NSError {
        println(error)
      } else if let response = responseObject as? DictionaryType {
        let closestBar = self.handleRegisterLoginUserInfoResponse(response)
        completionHandler(closestBar: closestBar)
      }
    })
  }

  func getUserPackages(onlyActive: Bool, completionHandler: (bars: [Bar]) -> ()) {
    Networking.sharedInstance.getUserPackages(userId, onlyActive: onlyActive, completionHandler: { (response) -> () in
      if let error = response as? NSError {
        println(error)
      } else if let responseObject = response as? DictionaryType {
        println(responseObject)
        if let success = responseObject["success"] as? Bool {
          if success {
            self.packages = []
            var bars = [Bar]()
            if let barsDictArray = responseObject["bars"] as? [DictionaryType] {
              for barDict in barsDictArray {
                let bar = Bar(dictionary: barDict)
                bar.updateBarWithDictionary(barDict)
                bars.append(bar)
                bar.packages.map({ (package: Package) -> () in
                  self.packages.append(package)
                })
              }
            }
            completionHandler(bars: bars)
            return
          }
        }
      }
      completionHandler(bars: [])
    })
  }
  
  func getUserHistory(completionHandler: (DictionaryType, Package?, (Package, PaymentMethod, String)?) -> ()) {
    Networking.sharedInstance.getUserHistory() { (responseObject) -> () in
      if let error = responseObject as? NSError {
        println("error: \(error)")
      } else {
        completionHandler(self.handleHistoryResponse(responseObject as NSDictionary))
      }
    }
  }
}

// MARK: Helpers
func imageDataWithImage(image: UIImage?) -> NSData? {
  if image != nil {
    return UIImagePNGRepresentation(image)
  } else {
    return nil
  }
}
