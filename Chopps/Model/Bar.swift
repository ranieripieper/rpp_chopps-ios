//
//  Bar.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation
import MapKit

class Bar: NSObject {
  
  var barId: Int
  var name: String
  var imageURL: String
  var location: GeoLocation
  var address: String
  var phone: String
  var packages: [Package] = []
  var userHasActivePackages = false
  
  init(dictionary: DictionaryType) {
    self.barId = (dictionary as NSDictionary).valueForKeyPath("id") as Int
    self.name = (dictionary as NSDictionary).valueForKeyPath("name") as String
    self.imageURL = (dictionary as NSDictionary).valueForKeyPath("logo_url") as? String ?? ""
    let latitude = (dictionary as NSDictionary).valueForKeyPath("latitude") as? Double
    let longitude = (dictionary as NSDictionary).valueForKeyPath("longitude") as? Double
    self.location = GeoLocation(latitude: latitude ?? 0, longitude: longitude ?? 0)
    self.phone = (dictionary as NSDictionary).valueForKeyPath("phone") as? String ?? ""
    self.address = ""
    if let street = (dictionary as NSDictionary).valueForKeyPath("address.street") as? String {
      self.address += street
    }
    if let number = (dictionary as NSDictionary).valueForKeyPath("address.number") as? String {
      self.address += ", " + number
    }
    if let neighborhood = (dictionary as NSDictionary).valueForKeyPath("address.neighborhood") as? String {
      self.address += " - " + neighborhood
    }
    if let city = (dictionary as NSDictionary).valueForKeyPath("address.city") as? String {
      self.address += " - " + city
    }
    if let state = (dictionary as NSDictionary).valueForKeyPath("address.state") as? String {
      self.address += " - " + state
    }
    if (dictionary as NSDictionary).valueForKeyPath("user_has_pkg") != nil {
      userHasActivePackages = dictionary["user_has_pkg"]!.boolValue
    }
    if let packagesArray = (dictionary as NSDictionary).valueForKeyPath("packages") as? [DictionaryType] {
      for packageDict in packagesArray {
        let package = Package(dictionary: packageDict)
        packages.append(package)
      }
    }
  }
  
  required init(coder aDecoder: NSCoder) {
    barId = aDecoder.decodeObjectForKey("barId") as Int
    name = aDecoder.decodeObjectForKey("name") as NSString
    imageURL = aDecoder.decodeObjectForKey("imageURL") as NSString
    let latitude = aDecoder.decodeObjectForKey("latitude") as Double
    let longitude = aDecoder.decodeObjectForKey("longitude") as Double
    location = GeoLocation (latitude: latitude, longitude: longitude)
    address = aDecoder.decodeObjectForKey("address") as NSString
    phone = aDecoder.decodeObjectForKey("phone") as NSString
    packages = aDecoder.decodeObjectForKey("packages") as [Package]
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(barId, forKey: "barId")
    aCoder.encodeObject(name, forKey: "name")
    aCoder.encodeObject(imageURL, forKey: "imageURL")
    aCoder.encodeObject(location.latitude, forKey: "latitude")
    aCoder.encodeObject(location.longitude, forKey: "longitude")
    aCoder.encodeObject(address, forKey: "address")
    aCoder.encodeObject(phone, forKey: "phone")
    aCoder.encodeObject(packages, forKey: "packages")
  }
}

// MARK: Class Functions
extension Bar {
  class func getNumberOfBars() -> Int {
    return NSUserDefaults.standardUserDefaults().integerForKey("NumberOfBars")
  }
  
  class func setNumberOfBars (numberOfBars: Int) {
    NSUserDefaults.standardUserDefaults().setInteger(numberOfBars, forKey: "NumberOfBars")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
}

// MARK: Instance Functions
extension Bar {
  func updateBarWithDictionary(dictionary: DictionaryType) {
    self.barId = (dictionary as NSDictionary).valueForKeyPath("id") as Int
    self.name = (dictionary as NSDictionary).valueForKeyPath("name") as String
    self.imageURL = (dictionary as NSDictionary).valueForKeyPath("logo_url") as String
    let latitude = (dictionary as NSDictionary).valueForKeyPath("latitude") as? Double
    let longitude = (dictionary as NSDictionary).valueForKeyPath("longitude") as? Double
    self.location = GeoLocation(latitude: latitude ?? 0, longitude: longitude ?? 0)
    self.phone = (dictionary as NSDictionary).valueForKeyPath("phone") as? String ?? ""
    self.address = ""
    if let street = (dictionary as NSDictionary).valueForKeyPath("address.street") as? String {
      self.address += street
    }
    if let number = (dictionary as NSDictionary).valueForKeyPath("address.number") as? String {
      self.address += ", " + number
    }
    if let neighborhood = (dictionary as NSDictionary).valueForKeyPath("address.neighborhood") as? String {
      self.address += " - " + neighborhood
    }
    if let city = (dictionary as NSDictionary).valueForKeyPath("address.city") as? String {
      self.address += " - " + city
    }
    if let state = (dictionary as NSDictionary).valueForKeyPath("address.state") as? String {
      self.address += " - " + state
    }
    if (dictionary as NSDictionary).valueForKeyPath("user_has_pkg") != nil {
      userHasActivePackages = dictionary["user_has_pkg"]!.boolValue
    }
    if let packagesArray = (dictionary as NSDictionary).valueForKeyPath("packages") as? [DictionaryType] {
      packages.removeAll(keepCapacity: false)
      for packageDict in packagesArray {
        let package = Package(dictionary: packageDict)
        package.bar = self
        packages.append(package)
      }
    }
  }
}

// MARK: API
extension Bar {
  class func getBarsWithLocation(location: CLLocation?, search: String?, page: Int, completionHandler: ([Bar]) -> ()) {
    Networking.sharedInstance.getBarsWithLocation(location, search: search, page: page) { responseObject in
      if let error = responseObject as? NSError {
        println(error)
      } else if let response = responseObject as? DictionaryType {
        var bars = [Bar]()
        for dict in response["bars"] as [DictionaryType] {
          let bar = Bar(dictionary: dict)
          bars.append(bar)
        }
        completionHandler(bars)
      } else {
        
      }
    }
  }
  
  func getBarInfo(completionHandler: () -> ()) {
    Networking.sharedInstance.getBarInfoWithId(barId, completionHandler: { (responseObject) -> () in
      if let error = responseObject as? NSError {
        println("error: \(error)")
      } else if let response = responseObject as? DictionaryType {
        if let success = response["success"] as? Bool {
          if success {
            if var barInfoDict = response["bar"] as? DictionaryType {
              if let userHasPackages = response["user_has_pkg"] as? Bool {
                barInfoDict["user_has_pkg"] = userHasPackages
              }
              self.updateBarWithDictionary(barInfoDict)
              if let pkgUserArray = response["pkg_user"] as? [DictionaryType] {
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                for pkgUserDict in pkgUserArray {
                  let packageId = (pkgUserDict as NSDictionary).valueForKeyPath("package.id") as Int
                  if let filteredPackage = self.packages.filter({ (aPackage) -> Bool in
                    aPackage.packageId == packageId
                  }).first {
                    filteredPackage.state = .Owned
                    filteredPackage.userPackageId = pkgUserDict["id"] as? Int
                    filteredPackage.drinks = []
                    for drinkDict in pkgUserDict["package_user_items"] as [DictionaryType] {
                      filteredPackage.drinks.append(Drink(dictionary: drinkDict))
                    }
                    if let expireString = pkgUserDict["dt_expire"] as? String {
                      if let date = dateFormatter.dateFromString(expireString) {
                        filteredPackage.expirationDate = date
                      }
                    }
                  }
                }
              }
              completionHandler()
            }
          }
        }
      }
    })
  }
  
  func getUserNotConsumedPackages(completionHandler: (AnyObject?) -> ()) {
    Networking.sharedInstance.getUserNotConsumedPackages(barId, completionHandler: { (response) -> () in
      if let error = response as? NSError {
        println("error: \(error)")
      } else if let responseObject = response as? DictionaryType {
        if let success = responseObject["success"] as? Bool {
          if success {
            if let packagesDict = responseObject["packages"] as? [DictionaryType] {
              var packages = [Package]()
              for packageDict in packagesDict {
                let package = Package(dictionary: packageDict)
                packages.append(package)
              }
              completionHandler(packages)
            }
          }
        }
      }
    })
  }
}

extension Bar: MKAnnotation {
  var coordinate: CLLocationCoordinate2D {
    return location.coordinate
  }
  var title: String {
    return name
  }
}
