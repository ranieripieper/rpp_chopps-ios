//
//  PaymentMethod.swift
//  Chopps
//
//  Created by Gilson Gil on 10/31/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class PaymentMethod: NSObject {
  var pid: Int?
  let name: String
  let number: String
  let flag: String?
  let expire: String
  var securityCode: String?
  
  init(name: String, number: String, expire: String) {
    self.name = name
    self.number = number
    self.flag = PaymentMethod.flagWithNumber(number)
    self.expire = expire
    super.init()
  }
  
  init(flag: String, endNumber: String) {
    self.name = ""
    self.number = endNumber
    if flag.lowercaseString == "visa" {
      self.flag = "VISA"
    } else {
      self.flag = flag.capitalizedString
    }
    self.expire = ""
  }
  
  required init(coder aDecoder: NSCoder) {
    pid = aDecoder.decodeObjectForKey("pid") as? Int
    name = aDecoder.decodeObjectForKey("name") as NSString
    number = aDecoder.decodeObjectForKey("number") as NSString
    flag = aDecoder.decodeObjectForKey("flag") as NSString
    expire = aDecoder.decodeObjectForKey("expire") as NSString
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(pid, forKey: "pid")
    aCoder.encodeObject(name, forKey: "name")
    aCoder.encodeObject(number, forKey: "number")
    aCoder.encodeObject(flag, forKey: "flag")
    aCoder.encodeObject(expire, forKey: "expire")
  }
  
  func save() -> Bool {
    return PaymentMethod.savePaymentMethod(self)
  }
  
  func delete() -> Bool {
    return PaymentMethod.deletePaymentMethod(self)
  }
  
  func flagForAPI() -> String {
    return flag!.stringByReplacingOccurrencesOfString("Master", withString: "mastercard")
  }
}

extension PaymentMethod {
  class func savePaymentMethod(paymentMethod: PaymentMethod) -> Bool {
    if let key = keyForCurrentUser() {
      if let paymentMethodsData = KeychainWrapper.dataForKey(key) {
        paymentMethod.pid = nextPid()
        var paymentMethods = NSKeyedUnarchiver.unarchiveObjectWithData(paymentMethodsData) as [String: PaymentMethod]
        paymentMethods[paymentMethod.number] = paymentMethod
        return KeychainWrapper.setData(NSKeyedArchiver.archivedDataWithRootObject(paymentMethods), forKey: key)
      } else {
        paymentMethod.pid = 0
        let paymentMethods = [paymentMethod.number: paymentMethod]
        return KeychainWrapper.setData(NSKeyedArchiver.archivedDataWithRootObject(paymentMethods), forKey: key)
      }
    } else {
      return false
    }
  }
  
  class func deletePaymentMethod(paymentMethod: PaymentMethod) -> Bool {
    if keyForCurrentUser() != nil {
      let key = keyForCurrentUser()!
      var paymentMethodsData = KeychainWrapper.dataForKey(key)
      if paymentMethodsData != nil {
        var paymentMethods = NSKeyedUnarchiver.unarchiveObjectWithData(paymentMethodsData!) as [String: PaymentMethod]
        if paymentMethods.count == 1 {
          return KeychainWrapper.removeObjectForKey(key)
        }
        paymentMethods.removeValueForKey(paymentMethod.number)
        return KeychainWrapper.setData(NSKeyedArchiver.archivedDataWithRootObject(paymentMethods), forKey: key)
      }
    } else {
      return false
    }
    return true
  }
  
  class func hasPaymentMethods() -> Bool {
    if keyForCurrentUser() != nil {
      let key = keyForCurrentUser()!
      let paymentMethodsData = KeychainWrapper.dataForKey(key)
      return paymentMethodsData != nil
    } else {
      return false
    }
  }
  
  class func paymentMethods() -> [PaymentMethod] {
    if keyForCurrentUser() != nil {
      let key = keyForCurrentUser()!
      let paymentMethodsData = KeychainWrapper.dataForKey(key)
      if paymentMethodsData == nil {
        return []
      }
      let paymentMethods = NSKeyedUnarchiver.unarchiveObjectWithData(paymentMethodsData!) as [String: PaymentMethod]
      var methods = [PaymentMethod]()
      for method in paymentMethods {
        methods.append(method.1)
      }
      methods.sort { (method1, method2) -> Bool in
        method1.pid > method2.pid
      }
      return methods
    } else {
      return []
    }
  }
  
  class func nextPid() -> Int {
    if keyForCurrentUser() != nil {
      let key = keyForCurrentUser()!
      let paymentMethodsData = KeychainWrapper.dataForKey(key)
      if paymentMethodsData == nil {
        return 0
      }
      let paymentMethods = NSKeyedUnarchiver.unarchiveObjectWithData(paymentMethodsData!) as [String: PaymentMethod]
      var methods = [PaymentMethod]()
      for method in paymentMethods {
        methods.append(method.1)
      }
      var largestPid = 0
      for method in methods {
        if largestPid < method.pid! {
          largestPid = method.pid!
        }
      }
      return largestPid + 1
    } else {
      return 0
    }
  }
  
  class func isCreditCardValidWithNumber(number: String) -> Bool {
    return Luhn.validateString(number)
  }
  
  class func flagWithNumber(number: String) -> String? {
    let creditCardType = Luhn.typeFromString(number)
    switch creditCardType {
    case .Amex:
      return "Amex"
    case .DinersClub:
      return "Diners"
    case .Discover:
      return "Discover"
    case .JCB:
      return "JCB"
    case .Mastercard:
      return "Master"
    case .Visa:
      return "VISA"
    default:
      return nil
    }
  }
  
  class func keyForCurrentUser() -> String? {
    if Person.persistedUser() != nil {
      return "PaymentMethods_\(Person.persistedUser()!.email)"
    } else {
      return nil
    }
  }
}
