//
//  BarDetailTableViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/23/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BarDetailTableViewDelegate: NSObject {
  
  @IBOutlet weak var barDetailViewController: BarDetailViewController!
  
  var bar: Bar?
  var barDistance: String?
  var datasource: [(Package.PackageState, [Package])] = []
}

extension BarDetailTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1 + datasource.count
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 1
    }
    return datasource[section - 1].1.count
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return section == 0 ? 0 : 50
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("BarDetailPackagesHeader") as BarDetailPackagesHeader
    let data = datasource[section - 1]
    header.configureWithTitle(data.0 == .Owned ? "MEUS PACOTES" : "PACOTES DISPONÍVEIS")
    return header
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return 200
    } else {
      return datasource[indexPath.section - 1].0 == .Owned ? 255 : 273
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    var cell: UITableViewCell?
    if indexPath.section == 0 {
      let barDetailCell = tableView.dequeueReusableCellWithIdentifier(BarDetailCell.defaultIdentifier(), forIndexPath: indexPath) as BarDetailCell
      barDetailCell.configureWithBarName(bar!.name, logoURL: bar!.imageURL, distance: barDistance ?? "-", address: bar!.address, phone: bar!.phone)
      barDetailCell.delegate = barDetailViewController
      cell = barDetailCell
    } else {
      let barDetailPackageCell = tableView.dequeueReusableCellWithIdentifier(BarDetailPackageCell.defaultIdentifier(), forIndexPath: indexPath) as BarDetailPackageCell
      let data = datasource[indexPath.section - 1]
      let package = data.1[indexPath.row]
      barDetailPackageCell.configureWithPackage(package, packageBought: data.0 == .Owned)
      barDetailPackageCell.delegate = barDetailViewController
      cell = barDetailPackageCell
    }
    return cell!
  }
}
