//
//  VerificationCodeTextFieldDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/24/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class VerificationCodeTextFieldDelegate: NSObject {
  
  @IBOutlet weak var verificationCodeViewController: VerificationCodeViewController!
}

extension VerificationCodeTextFieldDelegate: UITextFieldDelegate {
  
  func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
    let newString = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
    verificationCodeViewController.enableVerifyButton(countElements(newString) > 0)
    return true
  }
}
