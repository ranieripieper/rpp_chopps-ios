//
//  MapView.swift
//  Chopps
//
//  Created by Gilson Gil on 10/23/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

class MapView: MKMapView {

  @IBOutlet weak var tapRecognizer: UITapGestureRecognizer!
  
  override func awakeFromNib() {
    addTapGesture()
  }
  
  func addTapGesture () {
    addGestureRecognizer(tapRecognizer)
  }
  
  func removeTapGesture () {
    removeGestureRecognizer(tapRecognizer)
  }
}
