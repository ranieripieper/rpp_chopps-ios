//
//  SearchPackagesResultsTableViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 2/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchPackagesResultsTableViewDelegate: NSObject {
  @IBOutlet weak var searchPackagesResultsViewController: SearchPackagesResultsViewController!
  
  var datasource = [Bar]()
}

extension SearchPackagesResultsTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return datasource.count
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("SearchPackagesResultsHeader") as SearchPackagesResultsHeader
    let bar = datasource[section]
    var barDistance: String {
      if let userLocation = (UIApplication.sharedApplication().delegate as AppDelegate).locationManager.userLocation {
        return String(format: "%.1fkm", bar.location.distanceBetween(userLocation) / 1000)
      } else {
        return "--km"
      }
    }
    header.configureWithBar(bar, barDistance: barDistance)
    return header
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let bar = datasource[section]
    return bar.packages.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("SearchPackagesResultsCell", forIndexPath: indexPath) as SearchPackagesResultsCell
    cell.configureWithPackage(datasource[indexPath.section].packages[indexPath.row])
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    searchPackagesResultsViewController.showDetailsOfPackage(datasource[indexPath.section].packages[indexPath.row])
  }
}

extension SearchPackagesResultsTableViewDelegate: UIScrollViewDelegate {
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if scrollView.contentOffset.y + scrollView.bounds.height >= scrollView.contentSize.height {
      searchPackagesResultsViewController.search(nil)
    }
  }
}
