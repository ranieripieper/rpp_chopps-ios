//
//  TableView.swift
//  Chopps
//
//  Created by Gilson Gil on 10/22/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class TableView: UITableView, UIGestureRecognizerDelegate {
  
  @IBOutlet weak var searchBarViewController: SearchBarViewController!

  override func awakeFromNib() {
    let tap = UITapGestureRecognizer (target: self, action: "tapped:")
    tap.delegate = self
    addGestureRecognizer(tap)
  }
  
  override func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
    return (gestureRecognizer.locationInView(gestureRecognizer.view).y > -50 && !gestureRecognizer.isKindOfClass(UITapGestureRecognizer)) || (gestureRecognizer.isKindOfClass(UITapGestureRecognizer) && gestureRecognizer.locationInView(gestureRecognizer.view).y < -50)
  }
  
  func tapped (tapGestureRecognizer: UITapGestureRecognizer) {
    println("tapped at: \(tapGestureRecognizer.locationInView(tapGestureRecognizer.view).y)")
    searchBarViewController.openMap()
  }
}
