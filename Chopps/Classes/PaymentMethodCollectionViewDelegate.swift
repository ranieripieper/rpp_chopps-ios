//
//  PaymentMethodCollectionViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/31/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class PaymentMethodCollectionViewDelegate: NSObject {
  
  @IBOutlet weak var paymentMethodViewController: PaymentMethodViewController?
  @IBOutlet weak var editPaymentMethodViewController: EditPaymentMethodsViewController?
  
  var paymentMethods = PaymentMethod.paymentMethods()
}

extension PaymentMethodCollectionViewDelegate: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 5.0, left: 10.0, bottom: 5.0, right: 10.0)
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return CGSize(width: 140.0, height: 140.0)
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return paymentMethods.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let paymentMethod = paymentMethods[indexPath.item]
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PaymentMethodCell.defaultIdentifier(), forIndexPath: indexPath) as PaymentMethodCell
    cell.configureWithFlag(paymentMethod.flag!, number: paymentMethod.number)
    return cell
  }
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    paymentMethodViewController?.selectedPaymentMethod = paymentMethods[indexPath.item]
    editPaymentMethodViewController?.selectedPaymentMethod = paymentMethods[indexPath.item]
    editPaymentMethodViewController?.selectedItem = indexPath.item
    for cell in collectionView.visibleCells() {
      cell.setNeedsDisplay()
    }
  }
}
