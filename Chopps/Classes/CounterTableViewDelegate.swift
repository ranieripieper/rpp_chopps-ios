//
//  CounterTableViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/28/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class CounterTableViewDelegate: NSObject {
  
  var counterType: Int? // 0->units, 1->tens, 2->hundreds
  var counter: Int?
  var algarismus: Int?
  var tableView: UITableView?
  
  func configureWithTableView (tableView: UITableView?, counterType: Int, counter: Int) {
    if self.tableView == nil {
      self.tableView = tableView
    }
    self.tableView!.registerNib(UINib(nibName: CounterCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellReuseIdentifier: CounterCell.defaultIdentifier())
    self.counterType = counterType
    self.counter = counter
    algarismus = counter >= 100 ? 3 : (counter >= 10 ? 2 : 1)
  }
  
  func animateCounter () {
    updateCounterAnimated(true, after: 0.5)
  }
  
  func updateCounterAnimated (animated: Bool, after: Double) {
    delay(after, { () in
      self.tableView!.setContentOffset(CGPoint (x: 0, y: CGFloat (Int (self.counter! / Int(pow(10, CGFloat(self.counterType!))))) * self.tableView!.bounds.height), animated: animated)
    })
  }
}

extension CounterTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  
  func tableView (tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return Int(pow(Double(10), Double(algarismus! - counterType!)))
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(CounterCell.defaultIdentifier(), forIndexPath: indexPath) as CounterCell
    cell.configureWithNumber(indexPath.row % 10)
    return cell
  }
}
