//
//  DateFormatter.swift
//  Chopps
//
//  Created by Gilson Gil on 11/11/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

class DateFormatter {
  
  class func dottedStringForDate(date: NSDate) -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd.MM.yy"
    return dateFormatter.stringFromDate(date)
  }
  
  class func hyphenedStringForDate(date: NSDate) -> String {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    return dateFormatter.stringFromDate(date)
  }
  
  class func dottedDateForString(string: String) -> NSDate {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd.MM.yyyy"
    return dateFormatter.dateFromString(string)!
  }
  
  class func hyphenedDateForString(string: String) -> NSDate {
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
    return dateFormatter.dateFromString(string) ?? NSDate(timeIntervalSince1970: 0)
  }
}
