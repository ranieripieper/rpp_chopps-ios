//
//  Facebook.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

class Facebook {
  
  class func getSession () -> FBSession {
    return FBSession (appID: "844633495569708", permissions: ["public_profile"], defaultAudience: .OnlyMe, urlSchemeSuffix: "", tokenCacheStrategy: nil)
  }
  
  class func getAccessToken () -> String? {
    return FBSession.activeSession().accessTokenData?.accessToken?
  }
  
  class func loginWithCompletionHandler (completionHandler: (Bool) -> ()) {
    if !FBSession.activeSession().isOpen {
      FBSession.openActiveSessionWithReadPermissions([], allowLoginUI: true, completionHandler: { (fbSession, fbSessionState, error) -> Void in
        if error == nil {
          println("success")
          completionHandler(true)
        } else {
          println("error: \(error)")
          completionHandler(false)
        }
      })
    } else {
      completionHandler(true)
    }
  }
}
