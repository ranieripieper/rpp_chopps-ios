//
//  Notification.swift
//  Chopps
//
//  Created by Gilson Gil on 2/5/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Box<T> {
  let unbox: T
  init(_ value: T) { self.unbox = value }
}

struct Notification {
  let name: String
}

func postNotification<A>(note: Notification, value: A) {
  let userInfo = ["value": Box(value)]
  NSNotificationCenter.defaultCenter().postNotificationName(note.name, object: nil, userInfo: userInfo)
}

class NotificationObserver {
  let observer: NSObjectProtocol
  
  init(notification: Notification, closure aClosure: (AnyObject?) -> ()) {
    observer = NSNotificationCenter.defaultCenter().addObserverForName(notification.name, object: nil, queue: nil) { notif in
      aClosure(notif.userInfo)
    }
  }
  
  deinit {
    NSNotificationCenter.defaultCenter().removeObserver(observer)
  }
}
