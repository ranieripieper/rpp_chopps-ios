//
//  SearchPackageTableViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/31/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class SearchPackageTableViewDelegate: NSObject {
  
  @IBOutlet weak var searchPackageViewController: SearchPackageViewController!
  
  var bars: [Bar] = []
}

extension SearchPackageTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1 + bars.count
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.row == 0 {
      return 295
    } else {
      return 90
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    if indexPath.row == 0 {
      let cell = tableView.dequeueReusableCellWithIdentifier(SearchPackageParametersCell.defaultIdentifier(), forIndexPath: indexPath) as SearchPackageParametersCell
//      cell.delegate = searchPackageViewController
      return cell
    } else {
      let bar = bars[indexPath.row - 1]
      let cell = tableView.dequeueReusableCellWithIdentifier(BarCell.defaultIdentifier(), forIndexPath: indexPath) as BarCell
      cell.configureWithBar(bar, barDistance: "1,2km", shouldShowPackages: true)
      return cell
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if indexPath.row > 0 {
      let bar = bars[indexPath.row - 1]
      searchPackageViewController.performSegueWithIdentifier("SegueBarDetail", sender: bar)
    }
  }
}
