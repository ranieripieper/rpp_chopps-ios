//
//  BeersConsumedCollectionViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/28/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BeersConsumedCollectionViewDelegate: NSObject {
  
  var total: Int = 0
  var consumed: Int = 0
}

extension BeersConsumedCollectionViewDelegate: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return total
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let consumedBeersCell = collectionView.dequeueReusableCellWithReuseIdentifier("ConsumedBeerCell", forIndexPath: indexPath) as ConsumedBeerCell
    consumedBeersCell.configureWithConsumed(indexPath.item > total - consumed, type: "claro")
    if indexPath.item == total - consumed {
      delay(1.0, { () -> () in
        consumedBeersCell.animateWithType("claro")
      })
    }
    return consumedBeersCell
  }
}
