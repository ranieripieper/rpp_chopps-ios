//
//  HistoryTableViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 11/5/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HistoryTableViewDelegate: NSObject {
  @IBOutlet weak var historyViewController: HistoryViewController!
  
  var latestChoppTakenPackage: Package?
  var latestPurchasedPackage: Package?
  var latestPurchasePaymentMethod: PaymentMethod?
  var latestPurchaseDateString: String?
  var person = Person.persistedUser()
  var actives = ["nr_expired": 0, "nr_active": 0]
}

extension HistoryTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 5
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.row == 0 {
      return 90
    } else if indexPath.row == 1 {
      return 240
    } else if indexPath.row == 2 {
      return 260
    } else {
      return 110
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    var cell: UITableViewCell?
    if indexPath.row == 0 {
      let historyChoppsTakenCell = tableView.dequeueReusableCellWithIdentifier("HistoryChoppsTakenCell", forIndexPath: indexPath) as HistoryChoppsTakenCell
      if person != nil {
        historyChoppsTakenCell.configureWithTypes(person!.history!)
      }
      cell = historyChoppsTakenCell
    } else if indexPath.row == 1 {
      let historyLatestChoppTakenCell = tableView.dequeueReusableCellWithIdentifier("HistoryLatestChoppTakenCell", forIndexPath: indexPath) as HistoryLatestChoppTakenCell
      if latestChoppTakenPackage != nil {
        historyLatestChoppTakenCell.configureWithPackage(latestChoppTakenPackage!)
        historyLatestChoppTakenCell.historyViewController = historyViewController
      }
      cell = historyLatestChoppTakenCell
    } else if indexPath.row == 2 {
      let historyLatestPurchaseCell = tableView.dequeueReusableCellWithIdentifier("HistoryLatestPurchaseCell", forIndexPath: indexPath) as HistoryLatestPurchaseCell
      if latestPurchasedPackage != nil && latestPurchasePaymentMethod != nil {
        historyLatestPurchaseCell.configureWithPackage(latestPurchasedPackage!, paymentMethod: latestPurchasePaymentMethod!, date: latestPurchaseDateString!)
      }
      cell = historyLatestPurchaseCell
    } else if indexPath.row == 3 {
      let historyActivePackagesCell = tableView.dequeueReusableCellWithIdentifier("HistoryPackagesCell", forIndexPath: indexPath) as HistoryPackagesCell
      historyActivePackagesCell.configureWithCount(actives["nr_active"]!, active: true)
      historyActivePackagesCell.delegate = historyViewController
      cell = historyActivePackagesCell
    } else if indexPath.row == 4 {
      let historyDeactivePackagesCell = tableView.dequeueReusableCellWithIdentifier("HistoryPackagesCell", forIndexPath: indexPath) as HistoryPackagesCell
      historyDeactivePackagesCell.configureWithCount(actives["nr_expired"]!, active: false)
      historyDeactivePackagesCell.delegate = historyViewController
      cell = historyDeactivePackagesCell
    }
    return cell!
  }
}

extension HistoryTableViewDelegate: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    historyViewController.handleScrollViewDidScroll(scrollView)
  }
}
