//
//  Helper.swift
//  Chopps
//
//  Created by Gilson Gil on 10/22/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

func delay(delay:Double, closure:()->()) {
  dispatch_after(
    dispatch_time(
      DISPATCH_TIME_NOW,
      Int64(delay * Double(NSEC_PER_SEC))
    ),
    dispatch_get_main_queue(), closure)
}

func IOS_8() -> Bool {
  switch UIDevice.currentDevice().systemVersion.compare("8.0.0", options: NSStringCompareOptions.NumericSearch) {
  case .OrderedSame, .OrderedDescending:
    return true
  case .OrderedAscending:
    return false
  }
}
