//
//  ChoppTypes.swift
//  Chopps
//
//  Created by Gilson Gil on 11/10/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

class ChoppTypes: NSObject {
  
  let id: Int
  let singleDescription: String
  let pluralDescription: String
  
  init(id: Int, singleDescription: String) {
    self.id = id
    self.singleDescription = singleDescription
    self.pluralDescription = singleDescription + "s"
  }
  
  required init(coder aDecoder: NSCoder) {
    id = aDecoder.decodeObjectForKey("id") as Int
    singleDescription = aDecoder.decodeObjectForKey("singleDescription") as NSString
    pluralDescription = aDecoder.decodeObjectForKey("pluralDescription") as NSString
  }
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(id, forKey: "id")
    aCoder.encodeObject(singleDescription, forKey: "singleDescription")
    aCoder.encodeObject(pluralDescription, forKey: "pluralDescription")
  }
  
  class func choppTypeWithDictionary(dictionary: DictionaryType) -> ChoppTypes {
    var choppType = choppTypeForType(dictionary["type"] as Int)
    if choppType == nil {
      choppType = ChoppTypes(id: dictionary["type"] as Int, singleDescription: dictionary["desc"] as String)
    }
    return choppType!
  }
  
  class func persistTypes(choppTypes: [Int: ChoppTypes]) {
    let data = NSKeyedArchiver.archivedDataWithRootObject(choppTypes)
    NSUserDefaults.standardUserDefaults().setObject(data, forKey: "ChoppTypes")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  class func dePersistTypes() {
    NSUserDefaults.standardUserDefaults().removeObjectForKey("ChoppTypes")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  class func singleDescriptionForType(type: Int) -> String {
    let choppType = choppTypeForType(type)
    if choppType != nil {
      return choppType!.singleDescription
    }
    return ""
  }
  
  class func pluralDescriptionForType(type: Int) -> String {
    let choppType = choppTypeForType(type)
    if choppType != nil {
      return choppType!.pluralDescription
    }
    return ""
  }
  
  class func choppTypeForType(type: Int) -> ChoppTypes? {
    if let data = NSUserDefaults.standardUserDefaults().objectForKey("ChoppTypes") as? NSData {
      if let choppTypes = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [Int: ChoppTypes] {
        return choppTypes[type]
      }
    }
    return nil
  }
}

//chopp_types: (
//  {
//    desc = Claro;
//    id = 1;
//  },
//  {
//    desc = Escuro;
//    id = 2;
//  }
//)