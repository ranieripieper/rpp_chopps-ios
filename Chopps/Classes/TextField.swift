//
//  TextField.swift
//  Chopps
//
//  Created by Gilson Gil on 10/14/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

extension UITextField {
  
  func setPaddings () {
    setPaddings(20)
  }
  
  func setPaddings (width: CGFloat) {
    leftView = UIView (frame: CGRect (origin: CGPointZero, size: CGSize (width: width, height: bounds.height)))
    leftViewMode = .Always
    rightView = UIView (frame: CGRect (origin: CGPointZero, size: CGSize (width: width - 4, height: bounds.height)))
    rightViewMode = .Always
  }
  
  func shouldChangeCharactersInRange(range: NSRange, replacementString string: String, pattern: String) -> Bool {
    let numberWithSeparator = NSCharacterSet(charactersInString: "0123456789")
    let nonNumberSetWithOutSeparator = numberWithSeparator.invertedSet
    
    let currentText = text
    
    var currentMutatingText = NSMutableString(string: currentText)
    
    if countElements(string.stringByTrimmingCharactersInSet(nonNumberSetWithOutSeparator)) <= 0 {
      if countElements(string) == 0 {
        (currentMutatingText as NSMutableString).deleteCharactersInRange(range)
      }
    } else {
      if currentMutatingText.length < countElements(pattern) {
        currentMutatingText.insertString(string, atIndex: range.location)
      }
    }
    setText(currentMutatingText, pattern: pattern)
    return false
  }
  
  func setText(text: String, pattern: String) {
    let numberWithSeparator = NSCharacterSet(charactersInString: "0123456789")
    let nonNumberWithOutSeparator = numberWithSeparator.invertedSet
    
    var plainText = text.stringByTrimmingCharactersInSet(nonNumberWithOutSeparator)
    plainText = plainText.stringByReplacingOccurrencesOfString("/", withString: "")
    var currentMutatingText = NSMutableString(string: plainText)
    var targetString = NSMutableString()
    
    var j = 0
    for var i = 0; i < currentMutatingText.length; i++ {
      if pattern.substringWithRange(Range<String.Index>(start: advance(pattern.startIndex, j), end: advance(pattern.startIndex, j + 1))) == "x" {
        targetString.insertString(currentMutatingText.substringWithRange(NSMakeRange(i, 1)), atIndex: j)
      } else {
        targetString.insertString("/", atIndex: j++)
        targetString.insertString(currentMutatingText.substringWithRange(NSMakeRange(i, 1)), atIndex: j)
      }
      j++
    }
    self.text = targetString
  }
  
  func plainText() -> String {
    return text.stringByReplacingOccurrencesOfString("/", withString: "")
  }
}
