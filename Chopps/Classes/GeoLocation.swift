//
//  GeoLocation.swift
//  Chopps
//
//  Created by Gilson Gil on 10/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation
import MapKit

struct GeoLocation {
  var latitude: Double
  var longitude: Double
  
  func distanceBetween(other: GeoLocation) -> Double {
    let locationA = CLLocation (latitude: latitude, longitude: longitude)
    let locationB = CLLocation (latitude: other.latitude, longitude: other.longitude)
    return locationA.distanceFromLocation(locationB)
  }
  
  func distanceBetween(other: CLLocation?) -> Double {
    let location = CLLocation(latitude: latitude, longitude: longitude)
    return location.distanceFromLocation(other)
  }
}

extension GeoLocation {
  var coordinate: CLLocationCoordinate2D {
    return CLLocationCoordinate2D (latitude: latitude, longitude: longitude)
  }
  var mapPoint: MKMapPoint {
    return MKMapPointForCoordinate(coordinate)
  }
}

extension GeoLocation: Equatable {}

func ==(lhs: GeoLocation, rhs: GeoLocation) -> Bool {
  return lhs.latitude == rhs.longitude && lhs.longitude == rhs.longitude
}
