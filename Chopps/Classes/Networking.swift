//
//  Networking.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation

typealias DictionaryType = [String: AnyObject]

class Networking: AFHTTPSessionManager {
  class var sharedInstance: Networking {
    struct Static {
      static var instance: Networking?
      static var token: dispatch_once_t = 0
    }
    dispatch_once(&Static.token) {
      Static.instance = Networking()
      Static.instance!.installHeaderToken()
      AFNetworkActivityIndicatorManager.sharedManager().enabled = true
    }
    return Static.instance!
  }
  
  override init() {
    super.init(baseURL: NSURL(string: "https://choppsnbeer.herokuapp.com/api/v1/"), sessionConfiguration: nil)
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  func installHeaderToken() {
    let id = KeychainWrapper.stringForKey("userId")
    let authToken = KeychainWrapper.stringForKey("authToken")
    if id != nil && authToken != nil {
      requestSerializer.setValue(authToken!, forHTTPHeaderField: "User-Token")
    }
  }
  
  func uninstallHeaderToken() {
    requestSerializer.setValue(nil, forHTTPHeaderField: "User-Token")
  }
}

// MARK: Person
extension Networking {
  func registerUserWithName(name: String, email: String, password: String, birthDate: String, facebookToken: String?, facebookID: String?, location: CLLocation?, pictureData: NSData?, completionHandler: (AnyObject?) -> ()) {
    
    var params: [String: AnyObject] = ["user": ["name": name, "email": email, "password": password, "password_confirmation": password, "dt_birthday": birthDate]]
    if facebookToken != nil && facebookID != nil {
      params["user[facebook_token]"] = facebookToken
      params["user[facebook_id]"] = facebookID
    }
    if location != nil {
      params["lng"] = location!.coordinate.longitude
      params["lat"] = location!.coordinate.latitude
    }
    
    println(params)
    POST("users.json", parameters: params, constructingBodyWithBlock: { (formData: AFMultipartFormData!) -> Void in
      if pictureData != nil {
        formData.appendPartWithFileData(pictureData, name: "user[photo]", fileName: "avatar", mimeType: "image/png")
      }
    }, success: { (dataTask: NSURLSessionDataTask!, response: AnyObject!) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
    }) { (dataTask: NSURLSessionDataTask!, error: NSError!) -> Void in
      completionHandler(error)
    }
  }
  
  func loginWithEmail(email: String, password: String, location: CLLocation?, completionHandler: (AnyObject?) -> ()) {
    var params: DictionaryType
    if location != nil {
      params = ["user": ["email": email, "password": password], "lng": location!.coordinate.longitude, "lat": location!.coordinate.latitude]
    } else {
      params = ["user": ["email": email, "password": password]]
    }
    POST("users/login.json", parameters: params, success: { (dataTask: NSURLSessionDataTask!, response: AnyObject!) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
      }) { (dataTask: NSURLSessionDataTask!, error: NSError!) -> Void in
        completionHandler(error)
    }
  }
  
  func forgotPassword(email: String, completionHandler: (AnyObject?) -> ()) {
    let params = ["user": ["email": email]]
    POST("reset_password.json", parameters: params, success: { (dataTask, response) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
  
  func getUserInfo(location: CLLocation?, completionHandler: (AnyObject?) -> ()) {
    var url = ""
      if location != nil {
        url = "user/user.json?lat=\(location!.coordinate.latitude)&lng=\(location!.coordinate.longitude)"
      } else {
        url = "user/user.json"
      }
    let ongoingUserInfoDataTask = dataTasks.filter { (dataTask) -> Bool in
      dataTask.taskDescription == "UserInfoDataTask"
    }
    for task in ongoingUserInfoDataTask {
      if let dataTask = task as? NSURLSessionDataTask {
        dataTask.cancel()
      }
    }
    let userInfoDataTask = GET(url, parameters: nil, success: { (dataTask, response) -> Void in
      if let responseObject = response as? DictionaryType {
        if let success = responseObject["success"] as? Bool {
          if success {
            completionHandler(responseObject)
          } else {
            completionHandler(nil)
          }
        }
      }
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
    userInfoDataTask.taskDescription = "UserInfoDataTask"
  }

  func getUserPackages(onlyActive: Bool, completionHandler: (AnyObject?) -> ()) {
    getUserPackages(0, onlyActive: onlyActive, completionHandler: completionHandler)
  }

//  https://choppsnbeer.herokuapp.com/api/v1/user/my_packages.json?User-Token=1:bhHwaXbwUmbEUnMp3Cjz&bar_id=1
  func getUserPackages(barId: Int, onlyActive: Bool, completionHandler: (AnyObject?) -> ()) {
    var url: String
    let active = onlyActive ? "true" : "false"
    if barId == 0 {
      url = "user/my_packages.json"//?only_active=\(active)"
    } else {
      url = "user/my_packages.json"//?bar_id=\(barId)&only_active=\(active)"
    }
    println(url)
    GET(url, parameters: nil, success: { (dataTask, response) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
  
//  https://choppsnbeer.herokuapp.com/api/v1/user/history.json
  func getUserHistory(completionHandler: (AnyObject?) -> ()) {
    GET("user/history.json", parameters: nil, success: { (dataTask, response) -> Void in
      completionHandler(response)
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
  
  func saveProfile(userId: String, name: String, email: String, phone: String, password: String, image: UIImage) {
    
  }
}

// MARK: Bar

extension Networking {
  func getBarsWithLocation(location: CLLocation?, search: String?, page: Int, completionHandler: (AnyObject?) -> ()) {
    var url = "bars.json?"
    if location != nil {
      url += "lat=\(location!.coordinate.latitude)&lng=\(location!.coordinate.longitude)"
    }
    if search != nil {
      url += "&search=\(search!)"
    }
    url += "&page=\(page)"
    println(url)
    GET(url, parameters: nil, success: { (dataTask, response) -> Void in
      completionHandler(response)
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
  
  func getBarInfoWithId(barId: Int, completionHandler: (AnyObject?) -> ()) {
    GET("bars/\(barId).json", parameters: nil, success: { (dataTask, response) -> Void in
      completionHandler(response)
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
  
  func getUserNotConsumedPackages(barId: Int, completionHandler: (AnyObject?) -> ()) {
    GET("user/my_packages_not_consumed.json?bar_id=\(barId)", parameters: nil, success: { (dataTask, response) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
}

// MARK: Package

extension Networking {
  
  func buyPackage (package: Package, user: Person, completionHandler: (AnyObject?) -> ()) {
//    GET("buy_package.json?bar_id=\(package.bar!.barId)&package_id=\(package.packageId)&user_id=\(user.userId)", parameters: nil, success: { (dataTask, response) -> Void in
//      
//    }) { (dataTask, error) -> Void in
//      completionHandler(error)
//    }
  }
  
//  buy_package.json -d "{ \"bar_id\": \"545a6f2752616e2c33070000\", \"package_id\":\"545a6fad52616e2c330d0000\"}"
//  https://choppsnbeer.herokuapp.com/api/v1/user/buy_package.json -d "{ \"bar_id\": \"1\", \"package_id\":\"2\", \"bandeira\":\"visa\",\"cartao_numero\": "4012001038443335\",\"cartao_validade\":\"201805\",\"cartao_seguranca\":\"123\",\"cartao_portador\":\"Teste\", \"User-Token\":\":bqE8GBEG8Q4Kskkso7rz\"}" 
  func buyPackage(package: Package, paymentMethod: PaymentMethod, completionHandler: (AnyObject?) -> ()) {
    let expire = "20" + paymentMethod.expire.substringFromIndex(advance(paymentMethod.expire.startIndex, countElements(paymentMethod.expire) - 2)) + paymentMethod.expire.substringToIndex(advance(paymentMethod.expire.startIndex, 2))
    let params = ["bar_id": package.bar!.barId, "package_id": package.packageId, "bandeira": paymentMethod.flagForAPI().lowercaseString, "cartao_numero": paymentMethod.number.stringByReplacingOccurrencesOfString(" ", withString: ""), "cartao_validade": expire, "cartao_seguranca": paymentMethod.securityCode!, "cartao_portador": paymentMethod.name]
    println(params)
    POST("user/buy_package.json", parameters: params, success: { (dataTask, response) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
  
//http://choppsdds.dev.com:3000/api/v1/packages/search.json?value=100&type=2
  func findPackagesWithValue(value: Float, type: Int, page: Int, completionHandler: (AnyObject?) -> ()) {
    let valueString = String(format: "%.2f", value)
    GET("packages/search.json?value=\(valueString)&type=\(type)", parameters: nil, success: { (dataTask, response) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
    }) { (dataTask, error) -> Void in
      completionHandler(error)
    }
  }
}

// MARK: Drink
extension Networking {
  //  http://choppsdds.dev.com:3000/api/v1/user/drink_chopp.json?User-Token=1:bhHwaXbwUmbEUnMp3Cjz&pkg_user_id=1&pkg_user_item_id=5&pwd_unlock=123
  func drink(userPackageId: Int, userPackageItemId: Int, code: String, completionHandler: (AnyObject?) -> ()) {
    let params = ["pkg_user_id": userPackageId, "pkg_user_item_id": userPackageItemId, "pwd_unlock": code]
    POST("user/drink_chopp.json", parameters: params, success: { (dataTask, response) -> Void in
      if let responseObject = response as? DictionaryType {
        completionHandler(responseObject)
      } else {
        completionHandler(nil)
      }
      }) { (dataTask, error) -> Void in
        completionHandler(error)
    }
  }
}
