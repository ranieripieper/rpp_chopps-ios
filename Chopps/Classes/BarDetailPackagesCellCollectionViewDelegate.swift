//
//  BarDetailPackagesCellCollectionViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/29/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol BarDetailPackagesCellCollectionViewDelegateDelegate {
  func buyPackage(package: Package)
}

class BarDetailPackagesCellCollectionViewDelegate: NSObject {
  
  weak var collectionView: UICollectionView?
  weak var delegate: BarDetailPackagesCellCollectionViewDelegateDelegate?
  
//  var total: Int
//  var packages: [Package]
//  var packagesBought: [String]
  
//  init(total: Int, packages: [Package], packagesBought: [String]) {
//    self.total = total
//    let filteredPackages = packages.filter {
//      $0.total == total
//    }
//    self.packages = filteredPackages.sorted({ (package1, package2) -> Bool in
//      return true
//      return package1.brand < package2.brand || package1.type < package2.type
//    })
//    self.packagesBought = packagesBought
//    self.packages = packages.sorted({ (package1, package2) -> Bool in
//      return package1.brand < package2.brand || package1.type < package2.type
//    })
//    super.init()
//  }
}

//extension BarDetailPackagesCellCollectionViewDelegate: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
//  
//  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//    return packages.count
//  }
//  
//  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//    let package = packages[indexPath.item]
//    let barDetailPackageCell = collectionView.dequeueReusableCellWithReuseIdentifier(BarDetailPackageCell.defaultIdentifier(), forIndexPath: indexPath) as BarDetailPackageCell
//    barDetailPackageCell.configureWithPackageName(package.brand, packageDescription: package.packageDescription, packageType: package.type, total: package.total, fullPrice: package.fullPrice, discountedPrice: package.discountedPrice, packageBought: contains(packagesBought, package.packageId))
//    barDetailPackageCell.delegate = self
//    return barDetailPackageCell
//  }
//}
//
//extension BarDetailPackagesCellCollectionViewDelegate: BarDetailPackageCellDelegate {
//  
//  func buyPackageAtCell(barDetailPackageCell: BarDetailPackageCell) {
//    let indexPath = collectionView?.indexPathForCell(barDetailPackageCell)
//    if indexPath != nil {
//      let package = packages[indexPath!.item]
//      delegate?.buyPackage(package)
//    }
//  }
//}
