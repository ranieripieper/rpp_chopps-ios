//
//  MyPackagesCollectionViewFlowLayout.swift
//  Chopps
//
//  Created by Gilson Gil on 12/18/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MyPackagesCollectionViewFlowLayout: UICollectionViewFlowLayout {
  
  override func awakeFromNib() {
    super.awakeFromNib()
    scrollDirection = .Horizontal
    minimumLineSpacing = 0
    minimumInteritemSpacing = 0
  }

  override func targetContentOffsetForProposedContentOffset(proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
    var offsetAdjustment = proposedContentOffset.x
    
    let rawPageValue = collectionView!.contentOffset.x / pageWidth()
    let currentPage = (velocity.x > 0.0) ? floor(rawPageValue) : ceil(rawPageValue)
    let nextPage = (velocity.x > 0.0) ? ceil(rawPageValue) : floor(rawPageValue)
    
    let pannedLessThanAPage = fabs(1.0 + currentPage - rawPageValue) > 0.5
    let flicked = fabs(velocity.x) > flickVelocity()
    
    if pannedLessThanAPage && flicked {
      offsetAdjustment = nextPage * self.pageWidth()
    } else {
      offsetAdjustment = round(rawPageValue) * pageWidth()
    }
    return CGPoint(x: offsetAdjustment, y: proposedContentOffset.y)
  }
  
  func pageWidth() -> CGFloat {
    return itemSize.width + minimumLineSpacing
  }
  
  func flickVelocity() -> CGFloat {
    return 0.3
  }
}
