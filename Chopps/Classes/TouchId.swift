//
//  TouchId.swift
//  Chopps
//
//  Created by Gilson Gil on 10/1/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import Foundation
import LocalAuthentication

class TouchId {
  
  class func canAuthenticateWithTouchId () -> Bool {
    let context = LAContext()
    return context.canEvaluatePolicy(.DeviceOwnerAuthenticationWithBiometrics, error: nil)
  }
  
  class func authenticateWithTouchId (completionHandler: (success: Bool, error: NSError?) -> ()) {
    let context = LAContext()
    let reason = "Usar Touch ID para logar"
    
    if canAuthenticateWithTouchId() {
      context.evaluatePolicy(.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { (success: Bool, error: NSError!) -> Void in
        if success {
          completionHandler(success: true, error: nil)
        } else {
          completionHandler(success: false, error: error)
        }
      }
    }
  }
}
