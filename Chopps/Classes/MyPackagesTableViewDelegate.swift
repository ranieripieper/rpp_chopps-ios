//
//  MyPackagesTableViewDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 11/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class MyPackagesTableViewDelegate: NSObject {
  
  @IBOutlet weak var myPackageViewController: MyPackagesViewController!
  
  var bars: [Bar] = []
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
}

extension MyPackagesTableViewDelegate: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return bars.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let bar = bars[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(MyPackageDetailCell.defaultIdentifier(), forIndexPath: indexPath) as MyPackageDetailCell
    let barDistance = bar.location.distanceBetween(myPackageViewController.locationManager.userLocation!) / 1000
    let barDistanceString = String(format: "%.1fkm", barDistance)
    cell.configureWithBar(bar, distance: barDistanceString)
    cell.delegate = myPackageViewController
    return cell
  }
}
