//
//  ProfileCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var barNameLabel: UILabel!
  @IBOutlet weak var lastDrinkTakenDateLabel: UILabel!
  @IBOutlet weak var lastDrinkTakenDetailsLabel: UILabel!

  func configureWithAvatar (avatarURL: String, name: String, barName: String, lastChoppTakenDate: String, brand: String, type: String) {
    avatarImageView.setImageWithURL(NSURL (string: avatarURL))
    nameLabel.text = name
    barNameLabel.text = barName
    lastDrinkTakenDateLabel.text = lastChoppTakenDate
    lastDrinkTakenDetailsLabel.text = brand + ", " + type
  }
}
