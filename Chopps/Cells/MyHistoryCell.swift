//
//  MyHistoryCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

protocol MyHistoryCellDelegate {
  func myHistoryButtonTapped(myHistoryCell: MyHistoryCell)
}

class MyHistoryCell: UITableViewCell {
  @IBOutlet weak var graphView: GraphView!
  
  var delegate: MyHistoryCellDelegate?
  var didAnimateCell = false
  var choppTypes: [Int: Int]?
  
  @IBAction func myHistoryButtonTapped(myHistoryCell: MyHistoryCell) {
    delegate?.myHistoryButtonTapped(self)
  }
  
  func configureWithChoppTypes(choppTypes: [Int: Int]) {
    self.choppTypes = choppTypes
  }
  
  func animateCell() {
    didAnimateCell = true
    if choppTypes != nil && choppTypes!.count > 1 {
      let choppTypes1String = ChoppTypes.pluralDescriptionForType(1).uppercaseString
      let choppTypes1Amount = min(CGFloat(choppTypes![1]!), 9.0) / 9.0
      let choppTypes2String = ChoppTypes.pluralDescriptionForType(2).uppercaseString
      let choppTypes2Amount = min(CGFloat(choppTypes![2]!), 9.0) / 9.0
      graphView.configureWithTopGraphInfo(GraphInfo(name: "CHOPP'S \(choppTypes1String)", progress: choppTypes1Amount), bottom: GraphInfo(name: "CHOPP'S \(choppTypes2String)", progress: choppTypes2Amount))
    }
  }
}
