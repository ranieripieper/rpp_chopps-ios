//
//  PackagesCell.swift
//  Chopps
//
//  Created by Gilson Gil on 9/26/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class PackagesCell: UICollectionViewCell {

  @IBOutlet weak var gaugeView: GaugeView!
  @IBOutlet weak var barNameLabel: UILabel!
  @IBOutlet weak var drink1Label: UILabel!
  @IBOutlet weak var drink2Label: UILabel!
  @IBOutlet weak var drink3Label: UILabel!
  @IBOutlet weak var expirationDateLabel: UILabel!
  
  func configureCellWithBarName(barName: String, drink1Name: String?, drink2Name: String?, drink3Name: String?, expirationDate: String, progress: CGFloat, animated: Bool) {
    let ani = (animated && gaugeView.progress != progress) as Bool
    gaugeView.setProgress(progress, animated:ani)
    barNameLabel.text = barName
    expirationDateLabel.text = expirationDate
    drink1Label.text = drink1Name
    drink2Label.text = drink2Name
    drink3Label.text = drink3Name
  }
  
  func animateCellWithProgress(progress: CGFloat) {
    gaugeView.setProgress(progress, animated: true)
  }
}
