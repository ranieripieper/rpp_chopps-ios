//
//  BarDetailCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/23/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol BarDetailCellDelegate {
  func call(barDetailCell: BarDetailCell)
  func drive(barDetailCell: BarDetailCell)
}

class BarDetailCell: UITableViewCell {
  
  @IBOutlet weak var barDetailCellView: BarDetailCellView!
  @IBOutlet weak var barLogoImageView: UIImageView!
  @IBOutlet weak var middleLinesImageView: UIImageView!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressButton: UIButton!
  @IBOutlet weak var phoneButton: UIButton!
  
  weak var delegate: BarDetailCellDelegate?

  func configureWithBarName (name: String, logoURL: String, distance: String, address: String, phone: String) {
    barDetailCellView.setNeedsDisplay()
    setNeedsDisplay()
    barLogoImageView.setImageWithURL(NSURL (string: logoURL))
    nameLabel.text = name.uppercaseString
    distanceLabel.text = distance
    addressButton.setTitle(address, forState: .Normal)
    phoneButton.setTitle(phone, forState: .Normal)
  }
  
  @IBAction func call(sender: UIButton) {
    delegate?.call(self)
  }
  
  @IBAction func drive(sender: UIButton) {
    delegate?.drive(self)
  }
}
