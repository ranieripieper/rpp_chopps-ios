//
//  NearestBarCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/22/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol NearestBarCellDelegate {
  
  func letsGetaBeerButtonTapped (nearesBarCell: NearestBarCell)
}

class NearestBarCell: UITableViewCell {

  @IBOutlet weak var barNameLabel: UILabel!
  @IBOutlet weak var barIconImageViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var drinkButton: UIButton!
  
  weak var delegate: NearestBarCellDelegate?
  
  func configureWithBarName (name: String) {
    barNameLabel.text = name.uppercaseString
    if countElements(name) == 0 {
      barIconImageViewHeightConstraint.constant = 0
      drinkButton.setTitle("QUERO BEBER!", forState: .Normal)
    } else {
      barIconImageViewHeightConstraint.constant = 16
      drinkButton.setTitle("QUERO BEBER AQUI!", forState: .Normal)
    }
    UIView.animateWithDuration(0.3, animations: { () -> Void in
      self.layoutIfNeeded()
    })
  }
  
  @IBAction func letsGetaBeer (sender: UIButton) {
    delegate?.letsGetaBeerButtonTapped(self)
  }
}
