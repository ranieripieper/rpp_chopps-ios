//
//  SettingsCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol SettingsCellDelegate {
  
  func settingsButtonTapped (settingsCell: SettingsCell)
}

class SettingsCell: UITableViewCell {
  
  weak var delegate: SettingsCellDelegate?
  
  @IBAction func settingsButtonTapped () {
    delegate?.settingsButtonTapped(self)
  }
}
