//
//  HistoryLatestChoppTakenCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/5/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HistoryLatestChoppTakenCell: UITableViewCell {
  @IBOutlet weak var barCellView: BarCellView!
  @IBOutlet weak var latestDrinkTakenDateDrinkTypeLabel: UILabel!
  @IBOutlet weak var latestDrinkTakenDateLabel: UILabel!
  @IBOutlet weak var drink1Label: UILabel!
  @IBOutlet weak var drink2Label: UILabel!
  @IBOutlet weak var drink3Label: UILabel!
  @IBOutlet weak var packageExpireLabel: UILabel!
  @IBOutlet weak var barImageView: UIImageView!
  @IBOutlet weak var packageTypeImageView: UIImageView!
  
  weak var historyViewController: HistoryViewController?
  weak var package: Package?
  
  func configureWithPackage(package: Package) {
    self.package = package
    barCellView.name = package.bar?.name
    barCellView.shouldShowDistance = false
    barCellView.setNeedsDisplay()
    if package.type == .Chopp {
      latestDrinkTakenDateDrinkTypeLabel.text = "Chopp debitado dia"
      packageTypeImageView.image = UIImage(named: "img_code_chopp")
    } else {
      latestDrinkTakenDateDrinkTypeLabel.text = "Cerveja debitada dia"
      packageTypeImageView.image = UIImage(named: "img_code_beer")
    }
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd.MM.yy"
    latestDrinkTakenDateLabel.text = dateFormatter.stringFromDate(package.lastDateConsumed ?? NSDate(timeIntervalSince1970: 0))
    let fontDict = [NSFontAttributeName: UIFont(name: "GillSans", size: 16)!]
    let boldFontDict = [NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 18)!]
    if package.drinks.count > 0 {
      let remaining = package.drinks[0].amount - package.drinks[0].consumed
      let string = "\(remaining) \(package.drinks[0].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(remaining)))
      drink1Label.attributedText = attr
    } else {
      drink1Label.text = ""
    }
    if package.drinks.count > 1 {
      let remaining = package.drinks[1].amount - package.drinks[1].consumed
      let string = "\(remaining) \(package.drinks[1].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(remaining)))
      drink2Label.attributedText = attr
    } else {
      drink2Label.text = ""
    }
    if package.drinks.count > 2 {
      let remaining = package.drinks[2].amount - package.drinks[2].consumed
      let string = "\(remaining) \(package.drinks[2].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(remaining)))
      drink3Label.attributedText = attr
    } else {
      drink3Label.text = ""
    }
    packageExpireLabel.text = dateFormatter.stringFromDate(package.expirationDate ?? package.dateConsumption ?? NSDate(timeIntervalSince1970: 0))
    barImageView.setImageWithURL(NSURL(string: package.bar?.imageURL ?? ""))
  }
  
  @IBAction func goToDetail(sender: UIButton) {
    historyViewController?.showPackageDetails(package!)
  }
}
