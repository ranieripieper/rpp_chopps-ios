//
//  SearchBarCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol SearchBarCellDelegate {
  
  func searchBarButtonTapped (searchBarCell: SearchBarCell)
  func buyPackageButtonTapped (searchBarCell: SearchBarCell)
}

class SearchBarCell: UITableViewCell {
  
  weak var delegate: SearchBarCellDelegate?
  
  @IBAction func searchBarButtonTapped () {
    delegate?.searchBarButtonTapped(self)
  }
  
  @IBAction func buyPackageButtonTapped () {
    delegate?.buyPackageButtonTapped(self)
  }
}
