//
//  BarCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/17/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BarCell: UITableViewCell {

  @IBOutlet weak var barCellView: BarCellView!
  @IBOutlet weak var barImageView: UIImageView!
  @IBOutlet weak var yellowChoppImageView: UIImageView!
  @IBOutlet weak var darkChoppImageView: UIImageView!
  
  func configureWithBar (bar: Bar, barDistance: String, shouldShowPackages: Bool) {
    barImageView.setImageWithURL(NSURL (string: bar.imageURL))
    barCellView.name = bar.name.uppercaseString
    barCellView.distance = barDistance
    barCellView.shouldShowPackages = shouldShowPackages
    yellowChoppImageView.hidden = !shouldShowPackages
    darkChoppImageView.hidden = !shouldShowPackages
    if shouldShowPackages {
      var yellowChoppPackage: Package?
      var darkChoppPackage: Package?
      for package in bar.packages {
//        if package.type == 1 {
//          yellowChoppPackage = package
//        } else if package.type == 2 {
//          darkChoppPackage = package
//        }
      }
//      barCellView.package1Name = yellowChoppPackage?.brand ?? ""
//      barCellView.package1Price = yellowChoppPackage?.discountedPrice ?? ""
//      barCellView.package2Name = darkChoppPackage?.brand ?? ""
//      barCellView.package2Price = darkChoppPackage?.discountedPrice ?? ""
      yellowChoppImageView.hidden = yellowChoppPackage == nil
      darkChoppImageView.hidden = darkChoppPackage == nil
    }
    barCellView.setNeedsDisplay()
  }
}
