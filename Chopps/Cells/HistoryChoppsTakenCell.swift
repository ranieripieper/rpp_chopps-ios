//
//  HistoryChoppsTakenCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/5/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HistoryChoppsTakenCell: UITableViewCell {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var types: [Int: Int]?
  
  func configureWithTypes(types: [Int: Int]) {
    self.types = types
    collectionView.reloadData()
  }
}

extension HistoryChoppsTakenCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsZero
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 1
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return CGSize(width: 159, height: 89)
  }
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if types != nil {
      return types!.count
    }
    return 0
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("HistoryChoppsTakenDetailCell", forIndexPath: indexPath) as HistoryChoppsTakenDetailCell
    if types != nil {
      let type = indexPath.item + 1
      let count = types![type]!
      let lastItem = types!.count - 1 == indexPath.item
      cell.configureWithType(type, count: count, lastItem: lastItem)
    }
    return cell
  }
}
