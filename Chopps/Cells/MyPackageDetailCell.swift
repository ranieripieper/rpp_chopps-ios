//
//  MyPackageDetailCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol MyPackageDetailCellDelegate {
  func getABeerFromPackage(package: Package)
}

class MyPackageDetailCell: UITableViewCell {
  @IBOutlet weak var barCellView: BarCellView!
  @IBOutlet weak var barImageView: UIImageView!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var myPackagesCollectionViewFlowLayout: MyPackagesCollectionViewFlowLayout!
  
  weak var delegate: MyPackageDetailCellDelegate?
  var packages: [Package] = []
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.registerNib(UINib(nibName: MyPackageDetailPackageCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: MyPackageDetailPackageCell.defaultIdentifier())
    myPackagesCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 0)
  }
  
  func configureWithBar(bar: Bar, distance: String) {
    barCellView.name = bar.name
    barCellView.distance = distance
    barImageView.setImageWithURL(NSURL(string: bar.imageURL))
    packages = bar.packages
    barCellView.setNeedsDisplay()
    collectionView.reloadData()
  }
}

extension MyPackageDetailCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return packages.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let package = packages[indexPath.item]
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(MyPackageDetailPackageCell.defaultIdentifier(), forIndexPath: indexPath) as MyPackageDetailPackageCell
//    cell.configureWithPackageType(package.type, name: "", packageDescription: "", total: package.total, consumed: package.consumed, expire: DateFormatter.dottedStringForDate(package.expirationDate))
    cell.delegate = self
    return cell
  }
}

extension MyPackageDetailCell: MyPackageDetailPackageCellDelegate {
  
  func getABeerAtCell(myPackageDetailPackageCell: MyPackageDetailPackageCell) {
    let indexPath = collectionView.indexPathForCell(myPackageDetailPackageCell)
    if indexPath != nil {
      let package = packages[indexPath!.item]
      delegate?.getABeerFromPackage(package)
    }
  }
}
