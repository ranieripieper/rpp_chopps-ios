//
//  MyPackageDetailPackageCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/3/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol MyPackageDetailPackageCellDelegate {
  func getABeerAtCell(myPackageDetailPackageCell: MyPackageDetailPackageCell)
}

class MyPackageDetailPackageCell: UICollectionViewCell {

  @IBOutlet weak var packageTypeImageView: UIImageView!
  @IBOutlet weak var packageNameLabel: UILabel!
  @IBOutlet weak var packageDescriptionLabel: UILabel!
  @IBOutlet weak var packageRemainingLabel: UILabel!
  @IBOutlet weak var expireLabel: UILabel!
  
  weak var delegate: MyPackageDetailPackageCellDelegate?
  
  func configureWithPackageType(type: Int, name: String, packageDescription: String, total: Int, consumed: Int, expire: String) {
    packageTypeImageView.image = UIImage(named: "img_meus_pacotes_\(ChoppTypes.singleDescriptionForType(type).lowercaseString)\(total / 10)")
    packageNameLabel.text = name
    packageDescriptionLabel.text = "\(total) Chopps \(ChoppTypes.pluralDescriptionForType(type))"
    packageRemainingLabel.text = "\(total - consumed)"
    expireLabel.text = expire
  }
  
  @IBAction func getABeer(sender: UIButton) {
    delegate?.getABeerAtCell(self)
  }
}
