//
//  ConsumedBeerCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/28/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class ConsumedBeerCell: UICollectionViewCell {
  
  @IBOutlet weak var beerImageView: UIImageView!

  func configureWithConsumed (consumed: Bool, type: String) {
    if consumed {
      beerImageView.image = UIImage (named: "img_invite_chopp_\(type)_diseable")
    } else {
      beerImageView.image = UIImage (named: "img_invite_chopp_\(type)_eable")
    }
  }
  
  func animateWithType (type: String) {
    let crossFade = CABasicAnimation ()
    crossFade.keyPath = "contents"
    crossFade.duration = 0.3
    let image = UIImage (named: "img_invite_chopp_\(type)_diseable")
    crossFade.toValue = image!.CGImage
    crossFade.fillMode = kCAFillModeForwards
    crossFade.removedOnCompletion = false
    beerImageView.layer.addAnimation(crossFade, forKey: "animateContents")
  }
}
