//
//  BeerSelectionCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/24/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BeerSelectionCell: UITableViewCell {

  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var typeLabel: UILabel!
  @IBOutlet weak var packageImageView: UIImageView!

  func configureWithName (name: String, type: String) {
    nameLabel.text = name
    typeLabel.text = type
    if type == "Claro" {
      packageImageView.image = UIImage(named: "img_chopp_claro")!
    } else {
      packageImageView.image = UIImage(named: "img_chopp_escuro")!
    }
  }
}
