//
//  HistoryChoppsTakenDetailCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/5/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HistoryChoppsTakenDetailCell: UICollectionViewCell {

  @IBOutlet weak var borderLineImageView: UIImageView!
  @IBOutlet weak var typeImageView: UIImageView!
  @IBOutlet weak var typeLabel: UILabel!
  @IBOutlet weak var counterLabel: UILabel!
  
  func configureWithType(type: Int, count: Int, lastItem: Bool) {
    var typeString: String
    if type == 2 {
      typeString = "CERVEJAS TOMADAS"
    } else {
      typeString = "CHOPP'S TOMADOS"
    }
    typeImageView.image = UIImage(named: "img_code_" + (type == 1 ? "chopp" : "beer"))
    let attributedString = NSMutableAttributedString(string: typeString)
    attributedString.setAttributes([NSFontAttributeName : UIFont(name: "GillSans-Bold", size: 8)!], range: NSMakeRange(0, 8))
    typeLabel.attributedText = attributedString
    var leftZeros: String
    if count > 999 {
      leftZeros = ""
    } else if count > 99 {
      leftZeros = "0"
    } else if count > 9 {
      leftZeros = "00"
    } else {
      leftZeros = "000"
    }
    counterLabel.text = "\(leftZeros)\(count)"
    borderLineImageView.hidden = lastItem
  }
}
