//
//  BarDetailPackagesCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/29/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol BarDetailPackagesCellDelegate {
  func buyPackage(package: Package)
}

class BarDetailPackagesCell: UITableViewCell {
  
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var myPackagesCollectionViewFlowLayout: MyPackagesCollectionViewFlowLayout!

  var barDetailPackagesCellCollectionViewDelegate: BarDetailPackagesCellCollectionViewDelegate?
  weak var delegate: BarDetailPackagesCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView?.registerNib(UINib(nibName: BarDetailPackageCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: BarDetailPackageCell.defaultIdentifier())
  }
  
  func configureWithPackages (packages: [Package], total: Int, packagesBought: [String]) {
//    barDetailPackagesCellCollectionViewDelegate = BarDetailPackagesCellCollectionViewDelegate(total: total, packages: packages, packagesBought: packagesBought)
    barDetailPackagesCellCollectionViewDelegate!.delegate = self
    barDetailPackagesCellCollectionViewDelegate!.collectionView = collectionView
    myPackagesCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
//    collectionView.delegate = barDetailPackagesCellCollectionViewDelegate
//    collectionView.dataSource = barDetailPackagesCellCollectionViewDelegate
    collectionView.reloadData()
  }
}

extension BarDetailPackagesCell: BarDetailPackagesCellCollectionViewDelegateDelegate {
  
  func buyPackage(package: Package) {
    delegate?.buyPackage(package)
  }
}
