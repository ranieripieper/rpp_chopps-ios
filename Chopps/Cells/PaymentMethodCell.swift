//
//  PaymentMethodCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class PaymentMethodCell: UICollectionViewCell {
  
  @IBOutlet weak var flagImageView: UIImageView!
  @IBOutlet weak var flagLabel: UILabel!
  @IBOutlet weak var numberLabel: UILabel!

  func configureWithFlag(flag: String, number: String) {
    flagImageView.image = UIImage(named: "icn_pay_cartao_\(flag.lowercaseString)")
    flagLabel.text = "Cartão \(flag)"
    numberLabel.text = finalNumbersFromNumber(number)
  }
  
  private func finalNumbersFromNumber(number: String) -> String {
    let string = (number as NSString).substringFromIndex(countElements(number) - 4)
    return "Final \(string)"
  }
  
  override func drawRect(rect: CGRect) {
    if selected {
      let color = UIColor(red: 105.0/255, green: 158.0/255, blue: 162.0/255, alpha: 1.0).CGColor
      let context = UIGraphicsGetCurrentContext()
      CGContextSetFillColorWithColor(context, color)
      CGContextAddEllipseInRect(context, bounds)
      CGContextFillPath(context)
    }
  }
}
