//
//  CounterCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class CounterCell: UITableViewCell {
  
  @IBOutlet weak var numberLabel: UILabel!

  func configureWithNumber (number: Int) {
    numberLabel.text = String(number)
  }
}
