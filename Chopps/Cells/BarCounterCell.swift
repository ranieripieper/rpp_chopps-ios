//
//  BarCounterCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class BarCounterCell: UITableViewCell {
  
  @IBOutlet weak var barCounterUnit: UITableView!
  @IBOutlet weak var barCounterTens: UITableView!
  @IBOutlet weak var barCounterHundreds: UITableView!
  @IBOutlet weak var barCounterUnitWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var barCounterTensWidthConstraint: NSLayoutConstraint!
  @IBOutlet weak var barCounterHundredsWidthConstraint: NSLayoutConstraint!
  
  var hundredsCounterTableViewDelegate: CounterTableViewDelegate?
  var tensCounterTableViewDelegate: CounterTableViewDelegate?
  var unitsCounterTableViewDelegate: CounterTableViewDelegate?

  func configureWithBarCount (barCount: Int) {
    if barCount >= 100 {
      barCounterUnitWidthConstraint.constant = 47
      barCounterTensWidthConstraint.constant = 47
      barCounterHundredsWidthConstraint.constant = 47
      if hundredsCounterTableViewDelegate == nil {
        hundredsCounterTableViewDelegate = CounterTableViewDelegate ()
      }
      if tensCounterTableViewDelegate == nil {
        tensCounterTableViewDelegate = CounterTableViewDelegate ()
      }
      if unitsCounterTableViewDelegate == nil {
        unitsCounterTableViewDelegate = CounterTableViewDelegate ()
      }
      hundredsCounterTableViewDelegate?.configureWithTableView(barCounterHundreds, counterType: 2, counter: barCount)
      tensCounterTableViewDelegate?.configureWithTableView(barCounterTens, counterType: 1, counter: barCount)
      unitsCounterTableViewDelegate?.configureWithTableView(barCounterUnit, counterType: 0, counter: barCount)
      barCounterHundreds.dataSource = hundredsCounterTableViewDelegate
      barCounterTens.dataSource = tensCounterTableViewDelegate
      barCounterUnit.dataSource = unitsCounterTableViewDelegate
      hundredsCounterTableViewDelegate!.animateCounter()
      tensCounterTableViewDelegate!.animateCounter()
      unitsCounterTableViewDelegate!.animateCounter()
    } else if barCount >= 10 {
      barCounterUnitWidthConstraint.constant = 70
      barCounterTensWidthConstraint.constant = 70
      barCounterHundredsWidthConstraint.constant = 0
      if tensCounterTableViewDelegate == nil {
        tensCounterTableViewDelegate = CounterTableViewDelegate ()
      }
      if unitsCounterTableViewDelegate == nil {
        unitsCounterTableViewDelegate = CounterTableViewDelegate ()
      }
      tensCounterTableViewDelegate?.configureWithTableView(barCounterTens, counterType: 1, counter: barCount)
      unitsCounterTableViewDelegate?.configureWithTableView(barCounterUnit, counterType: 0, counter: barCount)
      barCounterTens.dataSource = tensCounterTableViewDelegate
      barCounterUnit.dataSource = unitsCounterTableViewDelegate
      tensCounterTableViewDelegate!.animateCounter()
      unitsCounterTableViewDelegate!.animateCounter()
    } else {
      barCounterUnitWidthConstraint.constant = 70
      barCounterTensWidthConstraint.constant = 0
      barCounterHundredsWidthConstraint.constant = 0
      if unitsCounterTableViewDelegate == nil {
        unitsCounterTableViewDelegate = CounterTableViewDelegate ()
      }
      unitsCounterTableViewDelegate?.configureWithTableView(barCounterUnit, counterType: 0, counter: barCount)
      barCounterUnit.dataSource = unitsCounterTableViewDelegate
      unitsCounterTableViewDelegate!.animateCounter()
    }
  }
}
