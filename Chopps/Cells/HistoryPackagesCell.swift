//
//  HistoryPackagesCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/6/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol HistoryPackagesCellDelegate {
  func goToDetail(cell: HistoryPackagesCell)
}

class HistoryPackagesCell: UITableViewCell {

  @IBOutlet weak var counterLabel: UILabel!
  @IBOutlet weak var packagesLabel: UILabel!
  @IBOutlet weak var bottomLineImageView: UIImageView!
  
  weak var delegate: HistoryPackagesCellDelegate?
  
  func configureWithCount(count: Int, active: Bool) {
    counterLabel.text = String(count)
    packagesLabel.text = active ? "Pacotes ativos" : "Pacotes expirados"
    bottomLineImageView.hidden = !active
  }
  
  @IBAction func goToDetail(sender: UIButton) {
    delegate?.goToDetail(self)
  }
}
