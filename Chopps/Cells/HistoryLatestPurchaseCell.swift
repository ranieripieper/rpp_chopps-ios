//
//  HistoryLatestPurchaseCell.swift
//  Chopps
//
//  Created by Gilson Gil on 11/6/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HistoryLatestPurchaseCell: UITableViewCell {

  @IBOutlet weak var latestPurchaseDateLabel: UILabel!
  
  @IBOutlet weak var paymentMethodTypeLabel: UILabel!
  @IBOutlet weak var paymentMethodEndingLabel: UILabel!
  @IBOutlet weak var paymentMethodImageView: UIImageView!
  
  @IBOutlet weak var packageTypeImageView: UIImageView!
  @IBOutlet weak var packageTypeLabel: UILabel!
  @IBOutlet weak var packageOriginalPriceLabel: UILabel!
  @IBOutlet weak var packageSalePriceLabel: UILabel!
  @IBOutlet weak var drink1Label: UILabel!
  @IBOutlet weak var drink2Label: UILabel!
  @IBOutlet weak var drink3Label: UILabel!
  
  func configureWithPackage(package: Package, paymentMethod: PaymentMethod, date: String) {
    latestPurchaseDateLabel.text = date
    paymentMethodTypeLabel.text = "Cartão " + paymentMethod.flag!
    paymentMethodEndingLabel.text = paymentMethod.number
    paymentMethodImageView.image = UIImage(named: "icn_pay_cartao_\(paymentMethod.flag!.lowercaseString)")
    
    if package.type == .Chopp {
      packageTypeImageView.image = UIImage(named: "icn_detail_bar_chopp")
      packageTypeLabel.text = "Pacote de Chopp's"
    } else {
      packageTypeImageView.image = UIImage(named: "icn_detail_bar_cerveja")
      packageTypeLabel.text = "Pacote de Cervejas"
    }
    let fontDict = [NSFontAttributeName: UIFont(name: "GillSans", size: 18)!]
    let boldFontDict = [NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 20)!]
    if package.drinks.count > 0 {
      let remaining = package.drinks[0].amount - package.drinks[0].consumed
      let string = "\(remaining) \(package.drinks[0].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(remaining)))
      drink1Label.attributedText = attr
    } else {
      drink1Label.text = ""
    }
    if package.drinks.count > 1 {
      let remaining = package.drinks[1].amount - package.drinks[1].consumed
      let string = "\(remaining) \(package.drinks[1].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(remaining)))
      drink2Label.attributedText = attr
    } else {
      drink2Label.text = ""
    }
    if package.drinks.count > 2 {
      let remaining = package.drinks[2].amount - package.drinks[2].consumed
      let string = "\(remaining) \(package.drinks[2].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(remaining)))
      drink3Label.attributedText = attr
    } else {
      drink3Label.text = ""
    }
  }
}
