//
//  MyPackagesCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/2/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol MyPackagesCellDelegate {
  
  func myPackagesButtonTapped (myPackagesCell: MyPackagesCell)
}

class MyPackagesCell: UITableViewCell {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  weak var delegate: MyPackagesCellDelegate?
  
  var packages: [Package] = []
  var didAnimateFirstCell = false
  var dateFormatter: NSDateFormatter {
    let df = NSDateFormatter()
    df.dateFormat = "dd.MM.yy"
    return df
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.registerNib(UINib (nibName: PackagesCell.defaultIdentifier(), bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: PackagesCell.defaultIdentifier())
  }
  
  @IBAction func myPackagedButtonTapped () {
    delegate?.myPackagesButtonTapped(self)
  }
  
  func configureWithPackages (packages: [Package]) {
    self.packages = packages
    collectionView.reloadData()
  }
  
  func animateFirstCell () {
    didAnimateFirstCell = true
    if packages.count > 0 {
      let package = packages.first as Package!
      let cell = collectionView.cellForItemAtIndexPath(NSIndexPath (forItem: 0, inSection: 0)) as PackagesCell
      var amount = 0
      let consumed = package.drinks.reduce(0, combine: { (count, drink) -> Int in
        amount += drink.amount
        return count + drink.consumed
      })
      cell.animateCellWithProgress(CGFloat(consumed) / CGFloat(amount))
    }
  }
}

extension MyPackagesCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return packages.count
  }

  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(PackagesCell.defaultIdentifier(), forIndexPath: indexPath) as PackagesCell
    let package = packages[indexPath.row]
    var progress = 0.0 as CGFloat
    var amount = 0
    let consumed = package.drinks.reduce(0, combine: { (count, drink) -> Int in
      amount += drink.amount
      return count + drink.consumed
    })
    if didAnimateFirstCell || indexPath.row != 0 {
      progress = CGFloat(consumed) / CGFloat(amount)
    }
    cell.configureCellWithBarName(package.bar?.name ?? "", drink1Name: package.drinks.first?.brand, drink2Name: package.drinks.count > 1 ? package.drinks[1].brand : nil, drink3Name: package.drinks.count > 2 ? package.drinks[2].brand : nil, expirationDate: dateFormatter.stringFromDate(package.expirationDate ?? package.dateConsumption ?? NSDate(timeIntervalSince1970: 0)), progress: progress, animated: false)
    return cell
  }

  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsMake(0, 0, 0, 20)
  }
}

extension UICollectionViewCell { // Class Name
  
  class func defaultIdentifier () -> String {
    let tokens = split(NSStringFromClass(self.classForCoder()), { $0 == "." })
    return tokens.last!
  }
}
