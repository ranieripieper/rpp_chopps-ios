//
//  BarDetailPackagesCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/28/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol BarDetailPackageCellDelegate {
  func buyPackage(package: Package)
  func moreDetailsFromPackage(package: Package, cell: BarDetailPackageCell)
  func consumePackage(package: Package)
}

class BarDetailPackageCell: UITableViewCell {

  @IBOutlet weak var barPackagesView: BarPackagesView!
  @IBOutlet weak var originalPriceLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var expireLabel: UILabel!
  @IBOutlet weak var buyPackageButton: UIButton!
  @IBOutlet weak var moreDetailsButton: UIButton!
  @IBOutlet weak var buyButtonTopSpaceConstraint: NSLayoutConstraint!
  @IBOutlet weak var moreDetailButtonTopAlignmentConstraint: NSLayoutConstraint!
  @IBOutlet weak var moreDetailTrailingAlignmentConstraint: NSLayoutConstraint!
  
  weak var package: Package?
  
  weak var delegate: BarDetailPackageCellDelegate?
  
  func configureWithPackage(package: Package, packageBought: Bool) {
    barPackagesView.package = package
    self.package = package
    let packageType = package.type ?? .Chopp
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yy"
    expireLabel.text = "Válido até " + (package.expirationDate != nil ? dateFormatter.stringFromDate(package.expirationDate!) : dateFormatter.stringFromDate(package.dateConsumption ?? NSDate(timeIntervalSince1970: 0)))
    if packageBought {
      buyPackageButton.setTitle("TOMAR " + (packageType == .Chopp ? "CHOPP" : "CERVEJA"), forState: .Normal)
      buyButtonTopSpaceConstraint.constant = 207
      moreDetailButtonTopAlignmentConstraint.constant = 0
      moreDetailTrailingAlignmentConstraint.constant = 108
      buyPackageButton.tag = 2
      originalPriceLabel.text = ""
      priceLabel.text = ""
    } else {
      buyPackageButton.setTitle("COMPRAR PACOTE", forState: .Normal)
      buyButtonTopSpaceConstraint.constant = 177
      moreDetailButtonTopAlignmentConstraint.constant = 48
      moreDetailTrailingAlignmentConstraint.constant = 0
      buyPackageButton.tag = 1
      let attributedString = NSMutableAttributedString(string: package.originalPrice)
      attributedString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: NSMakeRange(0, countElements(package.originalPrice)))
      originalPriceLabel.attributedText = attributedString
      priceLabel.text = package.salePrice
    }
  }
  
  @IBAction func buyPackageButtonTapped(sender: UIButton) {
    if sender.tag == 1 {
      delegate?.buyPackage(package!)
    } else {
      delegate?.consumePackage(package!)
    }
  }
  
  @IBAction func moreDetailButtonTapped(sender: UIButton) {
    delegate?.moreDetailsFromPackage(package!, cell: self)
  }
}
