//
//  SearchPackageParametersCell.swift
//  Chopps
//
//  Created by Gilson Gil on 10/31/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@objc protocol SearchPackageParameterDelegate {
  func didSelectYellowBeer ()
  func didSelectDarkBeer ()
  func didChangeSliderTo (to: CGFloat)
}

class SearchPackageParametersCell: UITableViewCell {
  
  @IBOutlet weak var yellowBeerButton: UIButton!
  @IBOutlet weak var darkBeerButton: UIButton!
  @IBOutlet weak var selectedBeerImageView: UIImageView!
  @IBOutlet weak var currentPriceLabel: UILabel!
  @IBOutlet weak var slider: UISlider!
  @IBOutlet weak var priceValueLeftPaddingConstraint: NSLayoutConstraint!
  
  weak var delegate: SearchPackageParameterDelegate?
  let numberFormatter = NSNumberFormatter()
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    slider.setThumbImage(UIImage(named: "btn_search_seek"), forState: .Normal)
    slider.setThumbImage(UIImage(named: "btn_search_seek"), forState: .Highlighted)
    slider.setMinimumTrackImage(UIImage(named: "img_clear"), forState: .Normal)
    slider.setMaximumTrackImage(UIImage(named: "img_clear"), forState: .Normal)
    numberFormatter.numberStyle = .CurrencyStyle
  }

  @IBAction func selectYellowBeer (sender: UIButton) {
    sender.selected = true
    darkBeerButton.selected = false
    moveSelectedToYellowBeer()
    delegate?.didSelectYellowBeer()
  }
  
  @IBAction func selectDarkBeer (sender: UIButton) {
    sender.selected = true
    yellowBeerButton.selected = false
    moveSelectedToDarkBeer()
    delegate?.didSelectDarkBeer()
  }
  
  @IBAction func slidedDidChange (sender: UISlider) {
    let left = 47 + CGFloat(sender.value) * sender.bounds.width * 0.8
    priceValueLeftPaddingConstraint.constant = left
    currentPriceLabel.text = numberFormatter.stringFromNumber(30 + (199 - 30) * sender.value)
  }
  
  @IBAction func slidedEnded (sender: UISlider) {
    delegate?.didChangeSliderTo(CGFloat(sender.value))
  }
  
  func moveSelectedToYellowBeer () {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: nil, animations: { () -> Void in
      self.selectedBeerImageView.transform = CGAffineTransformIdentity
    }) { (finished) -> Void in
      self.selectedBeerImageView.hidden = false
    }
  }
  
  func moveSelectedToDarkBeer () {
    UIView.animateWithDuration(0.3, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: nil, animations: { () -> Void in
      self.selectedBeerImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 110.0, 0.0)
    }) { (finished) -> Void in
      self.selectedBeerImageView.hidden = false
    }
  }
}
