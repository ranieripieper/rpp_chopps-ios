//
//  SearchPackagesResultsCell.swift
//  Chopps
//
//  Created by Gilson Gil on 2/25/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit

class SearchPackagesResultsCell: UITableViewCell {
  @IBOutlet weak var packageTypeImageView: UIImageView!
  @IBOutlet weak var drink1Label: UILabel!
  @IBOutlet weak var drink2Label: UILabel!
  @IBOutlet weak var drink3Label: UILabel!
  @IBOutlet weak var originalPriceLabel: UILabel!
  @IBOutlet weak var salePriceLabel: UILabel!
  @IBOutlet weak var expirationDateLabel: UILabel!
  
  func configureWithPackage(package: Package) {
    packageTypeImageView.image = UIImage(named: (package.type == .Chopp ? "icn_detail_bar_chopp" : "icn_detail_bar_cerveja"))
    let fontDict = [NSFontAttributeName: UIFont(name: "GillSans", size: 14)!]
    let boldFontDict = [NSFontAttributeName: UIFont(name: "GillSans-Bold", size: 18)!]
    if package.drinks.first != nil {
      let string = "\(package.drinks.first!.amount) \(package.drinks.first!.brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(package.drinks.first!.amount)))
      drink1Label.attributedText = attr
    } else {
      drink1Label.text = ""
    }
    if package.drinks.count > 1 {
      let string = "\(package.drinks[1].amount) \(package.drinks[1].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(package.drinks[1].amount)))
      drink2Label.attributedText = attr
    } else {
      drink2Label.text = ""
    }
    if package.drinks.count > 2 {
      let string = "\(package.drinks[2].amount) \(package.drinks[2].brand)"
      let attr = NSMutableAttributedString(string: string, attributes: fontDict)
      attr.setAttributes(boldFontDict, range: (string as NSString).rangeOfString(String(package.drinks[2].amount)))
      drink3Label.attributedText = attr
    } else {
      drink3Label.text = ""
    }
    let attr = NSAttributedString(string: package.originalPrice, attributes: [NSStrikethroughStyleAttributeName: 1])
    originalPriceLabel.attributedText = attr
    salePriceLabel.text = package.salePrice
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd/MM/yy"
    expirationDateLabel.text = "Válido até " + (package.expirationDate != nil ? dateFormatter.stringFromDate(package.expirationDate!) : dateFormatter.stringFromDate(package.dateConsumption ?? NSDate(timeIntervalSince1970: 0)))
  }
}
