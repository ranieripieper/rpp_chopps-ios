//
//  NavigationControllerDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 10/21/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class NavigationControllerDelegate: NSObject {

  @IBOutlet weak var navigationController: UINavigationController!
  
  let animator = HomeAnimator()
  var interactionController: UIPercentDrivenInteractiveTransition?
  
  override func awakeFromNib() {
    let popRecognizer = UIPanGestureRecognizer (target: self, action: "popGestureRecognizer:")
    popRecognizer.cancelsTouchesInView = false
    navigationController.view.addGestureRecognizer(popRecognizer)
  }
  
  func popGestureRecognizer (popGestureRecognizer: UIPanGestureRecognizer) {
    if navigationController.viewControllers.last is BeerConsumedViewController {
      return
    }
    let view = self.navigationController.view
    if popGestureRecognizer.state == .Began {
      let location = popGestureRecognizer.locationInView(view)
      if location.x < view.bounds.width * 0.1 && navigationController.viewControllers.count > 1 {
        self.interactionController = UIPercentDrivenInteractiveTransition ()
        navigationController.popViewControllerAnimated(true)
      }
    } else if popGestureRecognizer.state == .Changed {
      let translation = popGestureRecognizer.translationInView(view)
      let d = fabs(translation.x / CGRectGetWidth(view.bounds))
      interactionController?.updateInteractiveTransition(d)
    } else if popGestureRecognizer.state == .Ended {
      if popGestureRecognizer.velocityInView(view).x > 0 {
        interactionController?.finishInteractiveTransition()
      } else {
        interactionController?.cancelInteractiveTransition()
      }
      interactionController = nil
    }
  }
}

extension NavigationControllerDelegate: UINavigationControllerDelegate {
  
  func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    if operation == .Pop {
      animator.presenting = false
    } else {
      animator.presenting = true
    }
    return animator
  }
  
  func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    return interactionController
  }
}
