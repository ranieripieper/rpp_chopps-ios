//
//  HomeAnimator.swift
//  Chopps
//
//  Created by Gilson Gil on 10/21/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

class HomeAnimator: NSObject, UIViewControllerAnimatedTransitioning {
  
  var presenting = false
  
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    if presenting {
      if let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? HomeViewController {
        if let searchBarViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? SearchBarViewController {
          presentFromHome(fromViewController, toVC: searchBarViewController, transitionContext: transitionContext)
        } else if let beerSelectionViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? BeerSelectionViewController {
          presentFromHome(fromViewController, toVC: beerSelectionViewController, transitionContext: transitionContext)
        } else if let searchPackageViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? SearchPackageViewController {
          presentFromHome(fromViewController, toVC: searchPackageViewController, transitionContext: transitionContext)
        } else if let settingsViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? SettingsViewController {
          presentFromHome(fromViewController, toVC: settingsViewController, transitionContext: transitionContext)
        } else if let myPackagesViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? MyPackagesViewController {
          presentFromHome(fromViewController, toVC: myPackagesViewController, transitionContext: transitionContext)
        } else if let historyViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? HistoryViewController {
          presentFromHome(fromViewController, toVC: historyViewController, transitionContext: transitionContext)
        } else if let barDetailViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? BarDetailViewController {
          presentFromHome(fromViewController, toVC: barDetailViewController, transitionContext: transitionContext)
        } else if let beerConsumedViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? BeerConsumedViewController {
          presentFromHome(fromViewController, toVC: beerConsumedViewController, transitionContext: transitionContext)
        }
      } else {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        presentDefaultFromViewController(fromViewController!, toVC: toViewController!, transitionContext: transitionContext)
      }
      
    } else {
      if let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey) as? HomeViewController {
        if let searchBarViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? SearchBarViewController {
          dismissToHome(toViewController, fromVC: searchBarViewController, transitionContext: transitionContext)
        } else if let beerSelectionViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? BeerSelectionViewController {
          dismissToHome(toViewController, fromVC: beerSelectionViewController, transitionContext: transitionContext)
        } else if let searchPackageViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? SearchPackageViewController {
          dismissToHome(toViewController, fromVC: searchPackageViewController, transitionContext: transitionContext)
        } else if let settingsViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? SettingsViewController {
          dismissToHome(toViewController, fromVC: settingsViewController, transitionContext: transitionContext)
        } else if let myPackagesViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? MyPackagesViewController {
          dismissToHome(toViewController, fromVC: myPackagesViewController, transitionContext: transitionContext)
        } else if let historyViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? HistoryViewController {
          dismissToHome(toViewController, fromVC: historyViewController, transitionContext: transitionContext)
        } else if let barDetailViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? BarDetailViewController {
          dismissToHome(toViewController, fromVC: barDetailViewController, transitionContext: transitionContext)
        } else if let beerConsumedViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey) as? BeerConsumedViewController {
          dismissToHome(toViewController, fromVC: beerConsumedViewController, transitionContext: transitionContext)
        }
      } else {
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
        dismissDefaultFromViewController(fromViewController!, toVC: toViewController!, transitionContext: transitionContext)
      }
    }
  }
  
  private func presentFromHome(homeViewController: HomeViewController, toVC: UIViewController, transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView()
    let barBg = UIView(frame: homeViewController.headerView.frame)
    let logoImageView1 = UIImageView(frame: homeViewController.view.convertRect(homeViewController.headerLogoImageView1.frame, fromView: homeViewController.headerLogoImageView1.superview))
    let logoImageView2 = UIImageView(frame: homeViewController.view.convertRect(homeViewController.headerLogoImageView2.frame, fromView: homeViewController.headerLogoImageView2.superview))
    let logoImageView3 = UIImageView(frame: homeViewController.view.convertRect(homeViewController.headerLogoImageView3.frame, fromView: homeViewController.headerLogoImageView3.superview))
    
    toVC.view.transform = CGAffineTransformMakeTranslation(containerView.bounds.width, 0)
    homeViewController.headerView.alpha = 0
    barBg.backgroundColor = homeViewController.headerView.backgroundColor
    logoImageView1.image = homeViewController.headerLogoImageView1.image
    logoImageView1.contentMode = .ScaleAspectFit
    logoImageView2.image = homeViewController.headerLogoImageView2.image
    logoImageView2.contentMode = .ScaleAspectFit
    logoImageView3.image = homeViewController.headerLogoImageView3.image
    logoImageView3.contentMode = .ScaleAspectFit
    
    containerView.addSubview(toVC.view)
    homeViewController.navigationController?.view.addSubview(barBg)
    homeViewController.navigationController?.view.addSubview(logoImageView1)
    homeViewController.navigationController?.view.addSubview(logoImageView2)
    homeViewController.navigationController?.view.addSubview(logoImageView3)
    
    UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
      homeViewController.view.transform = CGAffineTransformMakeTranslation(-containerView.bounds.width * 2 / 3, 0)
      toVC.view.transform = CGAffineTransformIdentity
      barBg.frame = CGRect (origin: CGPointZero, size: CGSize (width: homeViewController.view.bounds.width, height: 64))
      //0 4 94 36
      //84 17 15 13
      //100 7 57 26
//      logoImageView1.frame = CGRect (x: (containerView.bounds.width - 56) / 2, y: 20, width: 56, height: 44)
      logoImageView1.frame = CGRect(x: (containerView.bounds.width - 158) / 2.0, y: 20 + 4, width: 84, height: 36)
      logoImageView2.frame = CGRect(x: (containerView.bounds.width - 158) / 2.0 + 84, y: 20 + 17, width: 16, height: 13)
      logoImageView3.frame = CGRect(x: (containerView.bounds.width - 158) / 2.0 + 100, y: 20 + 7, width: 57, height: 26)
      }) { (finished) -> Void in
        homeViewController.view.transform = CGAffineTransformIdentity
        toVC.view.transform = CGAffineTransformIdentity
        barBg.removeFromSuperview()
        logoImageView1.removeFromSuperview()
        logoImageView2.removeFromSuperview()
        logoImageView3.removeFromSuperview()
        
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
    }
  }
  
  private func dismissToHome (homeViewController: HomeViewController, fromVC: UIViewController, transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView()
    let barBg = UIView(frame: CGRect(origin: CGPointZero, size: CGSize (width: fromVC.view.bounds.width, height: 64)))
    let logoImageView1 = UIImageView(frame: CGRect(x: (containerView.bounds.width - 158) / 2.0, y: 20 + 4, width: 84, height: 36))
    let logoImageView2 = UIImageView(frame: CGRect(x: (containerView.bounds.width - 158) / 2.0 + 84, y: 20 + 17, width: 16 , height: 13))
    let logoImageView3 = UIImageView(frame: CGRect(x: (containerView.bounds.width - 158) / 2.0 + 100, y: 20 + 7, width: 57, height: 26))
    
    homeViewController.view.transform = CGAffineTransformMakeTranslation(-containerView.bounds.width * 2 / 3, 0)
    barBg.backgroundColor = homeViewController.headerView.backgroundColor
    logoImageView1.image = homeViewController.headerLogoImageView1.image
    logoImageView1.contentMode = .ScaleAspectFit
    logoImageView2.image = homeViewController.headerLogoImageView2.image
    logoImageView2.contentMode = .ScaleAspectFit
    logoImageView3.image = homeViewController.headerLogoImageView3.image
    logoImageView3.contentMode = .ScaleAspectFit
    
    containerView.insertSubview(homeViewController.view, belowSubview: fromVC.view)
    containerView.addSubview(barBg)
    containerView.addSubview(logoImageView1)
    containerView.addSubview(logoImageView2)
    containerView.addSubview(logoImageView3)
    
    UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
      fromVC.view.transform = CGAffineTransformMakeTranslation(containerView.bounds.width, 0)
      homeViewController.view.transform = CGAffineTransformIdentity
      barBg.frame = homeViewController.headerView.frame
      logoImageView1.frame = homeViewController.view.convertRect(homeViewController.headerLogoImageView1.frame, fromView: homeViewController.headerLogoImageView1.superview)
      logoImageView2.frame = homeViewController.view.convertRect(homeViewController.headerLogoImageView2.frame, fromView: homeViewController.headerLogoImageView2.superview)
      logoImageView3.frame = homeViewController.view.convertRect(homeViewController.headerLogoImageView3.frame, fromView: homeViewController.headerLogoImageView3.superview)
      }) { (finished) -> Void in
        fromVC.view.transform = CGAffineTransformIdentity
        homeViewController.view.transform = CGAffineTransformIdentity
        barBg.removeFromSuperview()
        logoImageView1.removeFromSuperview()
        logoImageView2.removeFromSuperview()
        logoImageView3.removeFromSuperview()
        if transitionContext.transitionWasCancelled() {
          homeViewController.headerView.alpha = 0
        } else {
          homeViewController.headerView.alpha = 1
        }
        
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
    }
  }
  
  private func presentDefaultFromViewController (fromVC: UIViewController, toVC: UIViewController, transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView()
     
    toVC.view.transform = CGAffineTransformMakeTranslation(containerView.bounds.width, 0)
    containerView.addSubview(toVC.view)
    
    UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
      fromVC.view.transform = CGAffineTransformMakeTranslation(-containerView.bounds.width * 2 / 3, 0)
      toVC.view.transform = CGAffineTransformIdentity
      }) { (finished) -> Void in
        fromVC.view.transform = CGAffineTransformIdentity
        toVC.view.transform = CGAffineTransformIdentity
        
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
    }
  }
  
  private func dismissDefaultFromViewController (fromVC: UIViewController, toVC: UIViewController, transitionContext: UIViewControllerContextTransitioning) {
    let containerView = transitionContext.containerView()
    
    toVC.view.transform = CGAffineTransformMakeTranslation(-containerView.bounds.width * 2 / 3, 0)
    containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
    
    UIView.animateWithDuration(transitionDuration(transitionContext), animations: { () -> Void in
      fromVC.view.transform = CGAffineTransformMakeTranslation(containerView.bounds.width, 0)
      toVC.view.transform = CGAffineTransformIdentity
      }) { (finished) -> Void in
        fromVC.view.transform = CGAffineTransformIdentity
        toVC.view.transform = CGAffineTransformIdentity
        
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
    }
  }
}
