//
//  AppDelegate.swift
//  Chopps
//
//  Created by Gilson Gil on 9/25/14.
//  Copyright (c) 2014 doisdoissete. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var fbSession: FBSession?
  var locationManager = LocationManager()

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    loadApplication()
    setAppearance()
    FBSession.openActiveSessionWithReadPermissions(["public_profile"], allowLoginUI: false, completionHandler: { (session, state, error) -> Void in
      if error == nil {
        self.fbSession = session
      } else {
        self.fbSession = Facebook.getSession()
      }
    })
    return true
  }
  
  func application(application: UIApplication, openURL url: NSURL, sourceApplication: String, annotation: AnyObject?) -> Bool {
    let wasHandled = FBAppCall.handleOpenURL(url, sourceApplication: sourceApplication)
    return wasHandled
  }
  
  // MARK: Load Logic
  func loadApplication () {
    window = UIWindow (frame: UIScreen.mainScreen().bounds)
    window?.makeKeyAndVisible()
    
    let logado = Person.persistedUser() != nil
    if logado {
      loadMainApplicationFlow()
    } else {
      loadLoginFlow()
    }
  }

  func loadLoginFlow () {
    let storyboard = UIStoryboard (name: "Login", bundle: NSBundle.mainBundle())
    window?.rootViewController = storyboard.instantiateInitialViewController() as? UIViewController
  }
  
  func loadMainApplicationFlow(options: DictionaryType?) {
    let storyboard = UIStoryboard (name: "Main", bundle: NSBundle.mainBundle())
    let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController
    if let homeViewController = navigationController!.viewControllers.first as? HomeViewController {
      if options != nil {
        if let hasInitialInfo = options!["hasInitialInfo"] as? Bool {
          homeViewController.hasInitialInfo = hasInitialInfo
        }
        if let closestBar = options!["closestbar"] as? Bar {
          homeViewController.closestBar = closestBar
        }
      }
    }
    window?.rootViewController = navigationController
  }
  
  func loadMainApplicationFlow () {
    loadMainApplicationFlow(nil)
  }
  
  func setAppearance () {
    let appearanceNavigationBar = UINavigationBar.appearance()
    appearanceNavigationBar.backIndicatorImage = UIImage (named: "btn_back")
    appearanceNavigationBar.backIndicatorTransitionMaskImage = UIImage (named: "btn_back")
    appearanceNavigationBar.tintColor = UIColor.whiteColor()
  }
}
